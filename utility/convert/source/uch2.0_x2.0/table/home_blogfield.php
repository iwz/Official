<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_blogfield.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'blogfield';
$table_target = $db_target->tablepre.'home_blogfield';

// 每次转换多少数据
$limit = $setting['limit']['blog'] ? $setting['limit']['blog'] : 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT bf.blogid, bf.uid, bf.tag, bf.message, bf.postip, bf.related, bf.relatedtime, bf.target_ids, bf.hotuser, b.pic FROM $table_source bf LEFT JOIN ".$db_source->table_name('blog')." b ON b.blogid = bf.blogid WHERE bf.blogid>'$start' ORDER BY bf.blogid LIMIT $limit");
while ($blog = $db_source->fetch_array($query)) {
	
	if(!empty($blog['tag'])) {
		$tag = unserialize($blog['tag']);
		if(is_array($tag)) {
			foreach($tag as $k => $v) {
				$blog['tag'] = implode(' ', $tag);
			}
		}
	}

	//下次执行id
	$nextid = $blog['blogid'];

	//数据引号处理
	$blog  = daddslashes($blog, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($blog, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." blogid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>