<?php

/**
 * DiscuzX Convert
 *
 * $Id: common_myapp.php 18808 2010-12-06 08:18:37Z zhengqingpeng $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'myapp';
$table_target = $db_target->tablepre.'common_myapp';

// 每次转换多少数据
$limit = $setting['limit']['myapp'] ? $setting['limit']['myapp'] : 500;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT  * FROM $table_source WHERE appid>'$start' ORDER BY appid LIMIT $limit");
while ($app = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $app['appid'];

	//数据引号处理
	$app  = daddslashes($app, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($app, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." appid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}
?>