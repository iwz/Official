<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_friend.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'friend';

// 每次转换多少数据
$limit = $setting['limit']['friend'] ? $setting['limit']['friend'] : 10000;

// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
$start = empty($start) ? 0 : $start;

if($start == 0) {
	$db_target->query("TRUNCATE ".$db_target->tablepre.'home_friend_request');
}

//下次执行id
$nextid = $start + $limit;
$done = true;

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source ORDER BY uid LIMIT $start, $limit");
while ($rs = $db_source->fetch_array($query)) {

	$done = false;

	//数据引号处理
	$rs  = daddslashes($rs, 1);

	if($rs['status']) {
		$table_target = $db_target->tablepre.'home_friend';
		if(empty($rs['fusername'])) {
			$subquery = $db_source->query("SELECT username FROM ".$db_source->tablepre.'space'." WHERE uid='$rs[fuid]]'");
			$rs['fusername'] = $db_source->result($subquery, 0);
			$rs['fusername'] = addslashes($rs['fusername']);
		}
		$rs['note'] = '';
	} else {
		$_uid = $rs['uid'];
		$_fuid = $rs['fuid'];
		$rs['uid'] = $_fuid;
		$rs['fuid'] = $_uid;
		
		$subquery = $db_source->query("SELECT username FROM ".$db_source->tablepre.'space'." WHERE uid='$_uid'");
		$rs['fusername'] = $db_source->result($subquery, 0);
		$rs['fusername'] = addslashes($rs['fusername']);
		
		$table_target = $db_target->tablepre.'home_friend_request';
	}
	

	//将数组整理成sql数据格式
	$data = implode_field_value($rs, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("REPLACE INTO $table_target SET $data");

}

//判断是否需要跳转
if($done == false) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." start> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>