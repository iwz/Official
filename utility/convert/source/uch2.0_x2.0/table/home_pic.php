<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_pic.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'pic';
$table_target = $db_target->tablepre.'home_pic';

// 每次转换多少数据
$limit = $setting['limit']['pic'] ? $setting['limit']['pic'] : 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT picid,albumid,uid,username,dateline,postip,filename,title,type,size,filepath,thumb,remote,hot,click_6,click_7,click_8,click_9,click_10,magicframe".
						 " FROM $table_source WHERE picid>'$start' ORDER BY picid LIMIT $limit");
while ($pic = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $pic['picid'];

	//数据引号处理
	$pic  = daddslashes($pic, 1);

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET `picid`='".$pic[picid]."',`albumid`='".$pic[albumid]."',`uid`='".$pic[uid]."',`username`='".$pic[username]."',`dateline`='".$pic[dateline].
			"',`postip`='".$pic[pistip]."',`filename`='".$pic[filename]."',`title`='".$pic[title]."',`type`='".$pic[type]."',`size`='".$pic[size]."',`filepath`='".$pic[filepath]."', `thumb`='".$pic[thumb].
			"',`remote`='".$pic[remote]."',`hot`='".$pic[hot]."',`click1`='".$pic[click_9]."',`click2`='".$pic[click_8]."',`click3`='".$pic[click_7]."',`click4`='".$pic[click_6]."',`click5`='".$pic[click_10]."'
			");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." picid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>