<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_mailcron.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的主table
$table_source = $db_source->tablepre.'mailcron';
$table_target = $db_target->tablepre.'common_mailcron';

//转换的关联table
$table_source_rel = $db_source->tablepre.'mailqueue';
$table_target_rel = $db_target->tablepre.'common_mailqueue';

// 每次转换多少数据
$limit = 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');

// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
	$db_target->query("TRUNCATE $table_target_rel");
}

// 取得主表数据，并存储
$query = $db_source->query("SELECT  * FROM $table_source WHERE cid>'$start' ORDER BY cid LIMIT $limit");
$mcids = array();
while ($rs = $db_source->fetch_array($query)) {

	//主表ID
	$mcids[] = $rs['cid'];
	
	//下次执行id
	$nextid = $rs['cid'];
	
	//数据引号处理
	$rs  = daddslashes($rs, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($rs, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

if (!empty($mcids)) {
	//取得关联表数据，并存储
	$query = $db_source->query("SELECT  * FROM $table_source_rel WHERE cid IN (".dimplode($mcids).")");
	while ($rs = $db_source->fetch_array($query)) {

		//数据引号处理
		$rs  = daddslashes($rs, 1);

		//将数组整理成sql数据格式
		$data = implode_field_value($rs, ',', db_table_fields($db_target, $table_target_rel));

		//插入数据表
		$db_target->query("INSERT INTO $table_target_rel SET $data");
	}
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." mcid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>