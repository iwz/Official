<?php

/**
 * DiscuzX Convert
 *
 * $Id: attachments.php 15777 2010-08-26 04:00:58Z zhengqingpeng $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'attachments';
$table_target = $db_target->tablepre.'portal_attachment';

// 每次转换多少数据
$limit = 150;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');

// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT  * FROM $table_source WHERE aid>'$start' ORDER BY aid LIMIT $limit");
while ($rs = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $rs['aid'];

	$setarr = array();
	$setarr['uid'] = $rs['uid'];
	$setarr['dateline'] = $rs['dateline'];
	$setarr['filename'] = $rs['filename'];
	$setarr['filetype'] = $rs['attachtype'];
	$setarr['filesize'] = $rs['size'];
	$setarr['attachment'] = $rs['filepath'];
	$setarr['isimage'] = $rs['isimage'];
	//ss中有缩略图的附件标记为2
	$setarr['thumb'] = empty($rs['thumbpath']) ? '0' : '2';
	$setarr['remote'] = '0';
	$setarr['aid'] = $rs['itemid'];
	
	//数据引号处理
	$setarr  = daddslashes($setarr, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($setarr, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." id> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>