<?php

/**
 * DiscuzX Convert
 *
 * $Id: spaceitems.php 15777 2010-08-26 04:00:58Z zhengqingpeng $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table 文章的标题
$table_source = $db_source->tablepre.'spaceitems';
$table_target = $db_target->tablepre.'portal_article_title';
$table_target_count = $db_target->tablepre.'portal_article_count';
$table_target_content = $db_target->tablepre.'portal_article_content';

//文章的内容
$table_source_content = $db_source->tablepre.'spacenews';

// 每次转换多少数据
$limit = 300;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');

// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
	$db_target->query("TRUNCATE $table_target_count");
	$db_target->query("TRUNCATE $table_target_content");
}

// 取得标题数据，并存储
$query = $db_source->query("SELECT * FROM $table_source WHERE itemid>'$start' ORDER BY itemid LIMIT $limit");
while ($rs = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $rs['itemid'];
	$settitle = array();
	$rs['none'] = '';
	$settitle['aid'] = $rs['itemid'];
	$settitle['catid'] = $rs['catid'];
	$settitle['bid'] = $rs['none'];
	$settitle['uid'] = $rs['uid'];
	$settitle['username'] = $rs['username'];
	$settitle['title'] = $rs['subject'];
	$settitle['shorttitle'] = $rs['none'];
	$settitle['summary'] = $rs['none'];
	$settitle['pic'] = $rs['none'];
	$settitle['thumb'] = $rs['none'];
	$settitle['remote'] = $rs['none'];
	$settitle['id'] = $rs['none'];
	$settitle['idtype'] = $rs['none'];
	$settitle['allowcomment'] = $rs['allowreply'];
	$settitle['dateline'] = $rs['dateline'];

	//content中的数据
	$settitle['author'] = '';
	$settitle['from'] = '';
	$settitle['fromurl'] = '';
	$settitle['url'] = '';
	
	//处理分页内容
	$count = 0; //分页数
	$cquery = $db_source->query("SELECT * FROM $table_source_content WHERE itemid='$rs[itemid]'");
	while($crs = $db_source->fetch_array($cquery)) {
		//文章内容
		$setcontent = array();
		$setcontent['cid'] = $crs['nid'];
		$setcontent['aid'] = $crs['itemid'];
		$setcontent['content'] = $crs['message'];
		$setcontent['pageorder'] = $crs['pageorder'];
		$setcontent['dateline'] = $crs['dateline'];

		//数据引号处理
		$setcontent  = daddslashes($setcontent, 1);

		//将数组整理成sql数据格式
		$data = implode_field_value($setcontent, ',', db_table_fields($db_target, $table_target_content));

		//插入数据表
		$db_target->query("INSERT INTO $table_target_content SET $data");

		//移动到title表中的数据
		$settitle['author'] = $crs['newsauthor'] ? $crs['newsauthor'] : $settitle['author'];
		$settitle['from'] = $crs['newsfrom'] ? $crs['newsfrom'] : $settitle['from'];
		$settitle['fromurl'] = $crs['newsfromurl'] ? $crs['newsfromurl'] : $settitle['fromurl'];
		$settitle['url'] = $crs['newsurl'] ? $crs['newsurl'] : $settitle['url'];
		$count ++;
	}
	
	$settitle['contents'] = $count;
	
	//数据引号处理
	$settitle  = daddslashes($settitle, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($settitle, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");

	
	//文章的统计
	$setcount = array();
	$setcount['aid'] = $rs['itemid'];
	$setcount['viewnum'] = $rs['viewnum'];
	$setcount['commentnum'] = $rs['replynum'];
	$setcount['catid'] = $rs['catid'];
	$setcount['dateline'] = $rs['dateline'];

	//数据引号处理
	$setcount  = daddslashes($setcount, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($setcount, ',', db_table_fields($db_target, $table_target_count));

	//插入数据表
	$db_target->query("INSERT INTO $table_target_count SET $data");

}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." itemid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>