<?php

/**
 * DiscuzX Convert
 *
 * $Id: categories.php 15777 2010-08-26 04:00:58Z zhengqingpeng $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'categories';
$table_target = $db_target->tablepre.'portal_category';

$table_source_items =  $db_source->tablepre.'spaceitems';

// 每次转换多少数据
$limit = 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start') ? getgpc('start') : 0;

// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

$arr = $catids = $count_num = array();

// 取得数据
$query = $db_source->query("SELECT * FROM $table_source WHERE catid>'$start' ORDER BY catid LIMIT $limit");
while ($value = $db_source->fetch_array($query)) {
	$arr[] = $value;
	$catids[] = $value['catid'];
}

//处理文章总数
$query = $db_source->query("SELECT catid, COUNT(*) AS num FROM $table_source_items WHERE catid IN (".dimplode($catids).") GROUP BY catid");
while ($value = $db_source->fetch_array($query)) {
	$count_num[$value['catid']] = $value['num'];
}

//存储
foreach ($arr as $rs) {
	//下次执行id
	$nextid = $rs['aid'];

	$setarr = array();
	$setarr['catid'] = $rs['catid'];
	$setarr['upid'] = $rs['upid'];
	$setarr['catname'] = $rs['name'];
	$setarr['displayorder'] = $rs['displayorder'];
	$setarr['description'] = $rs['note'];
	
	//处理文章总数
	$setarr['articles'] = empty($count_num[$rs['catid']]) ? 0 : $count_num[$rs['catid']];
	
	//数据引号处理
	$setarr  = daddslashes($setarr, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($setarr, ',', db_table_fields($db_target, $table_target));
	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." catid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>