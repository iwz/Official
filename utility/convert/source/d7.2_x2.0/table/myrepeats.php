<?php

/**
 * DiscuzX Convert
 *
 * $Id: myrepeats.php 15815 2010-08-27 02:56:14Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'myrepeats';
$table_target = $db_target->tablepre.'myrepeats';

// 每次转换多少数据
$limit = 2000;
// 下次跳转id
$nextid = 0;

$query1 = $db_source->result($db_source->query("SHOW FIELDS FROM $table_source", 'SILENT'), 0);
$query2 = $db_target->result($db_target->query("SHOW FIELDS FROM $table_target", 'SILENT'), 0);
$pass = $query1 && $query2;

if($pass) {
	// 取得本次转换的起始id
	$start = getgpc('start');
	// 首次执行，清空目标数据表，避免重复转换
	if(empty($start)) {
		$start = 0;
		$db_target->query("TRUNCATE $table_target");
	}

	// 取得数据，并存储
	$query = $db_source->query("SELECT * FROM $table_source LIMIT $start, $limit");
	while ($row = $db_source->fetch_array($query)) {

		//下次执行id
		$nextid = 1;

		//数据引号处理
		$row  = daddslashes($row, 1);

		//将数组整理成sql数据格式
		$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));

		//插入数据表
		$db_target->query("INSERT INTO $table_target SET $data");
	}

	//判断是否需要跳转
	if($nextid) {
		//跳转到新的id
		showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=".($start+$limit));
	}
}

?>