<?php

/**
 * DiscuzX Convert
 *
 * $Id: activities.php 10469 2010-05-11 09:12:14Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'activities';
$table_source_aa = $db_source->tablepre.'activityapplies';
$table_target = $db_target->tablepre.'forum_activity';

// 每次转换多少数据
$limit = 2000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if(empty($start)) {
	$start = 0;
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source WHERE tid>'$start' ORDER BY tid LIMIT $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $row['tid'];

	//数据引号处理
	$row['applynumber'] = $db_source->result_first("SELECT COUNT(*) FROM $table_source_aa WHERE tid='$row[tid]' AND verified=1");
	$row = daddslashes($row, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." tid > $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>