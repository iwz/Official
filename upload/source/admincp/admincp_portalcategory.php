<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_portalcategory.php 32945 2013-03-26 05:01:12Z zhangguosheng $
 */

if(!defined('IN_DISCUZ') || !defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/portalcp');

cpheader();
$operation = in_array($operation, array('delete', 'move', 'perm', 'add', 'edit')) ? $operation : 'list';

// 文章分类
loadcache('portalcategory');
$portalcategory = $_G['cache']['portalcategory'];

if($operation == 'list') {// 管理分类

	//兼容处理
	//if(empty($portalcategory) && DB::result_first('SELECT COUNT(*) FROM '.DB::table('portal_category'))) {
	if(empty($portalcategory) && C::t('portal_category')->count()) {
		updatecache('portalcategory');
		loadcache('portalcategory', true);
		$portalcategory = $_G['cache']['portalcategory'];
	}
	if(!submitcheck('editsubmit')) {

		shownav('portal', 'portalcategory');
		showsubmenu('portalcategory',  array(
			array('list', 'portalcategory', 1)
		));

		$tdstyle = array('width="25"', 'width="60"', '', 'width="45"', 'width="55"', 'width="30"', 'width="30"', 'width="30"', 'width="185"', 'width="100"');
		showformheader('portalcategory');
		echo '<div style="height:30px;line-height:30px;"><a href="javascript:;" onclick="show_all()">'.cplang('show_all').'</a> | <a href="javascript:;" onclick="hide_all()">'.cplang('hide_all').'</a> <input type="text" id="srchforumipt" class="txt" /> <input type="submit" class="btn" value="'.cplang('search').'" onclick="return srchforum()" /></div>';
		showtableheader('', '', 'id="portalcategory_header" style="min-width:900px;*width:900px;"');
		showsubtitle(array('', '', 'portalcategory_name', 'portalcategory_articles', 'portalcategory_allowpublish', 'portalcategory_allowcomment', 'portalcategory_is_closed', 'setindex', 'operation', 'portalcategory_article_op'), 'header tbm', $tdstyle);
		showtablefooter();
		echo '<script type="text/javascript">floatbottom(\'portalcategory_header\');</script>';
		showtableheader('', '', 'style="min-width:900px;*width:900px;"');
		showsubtitle(array('', '', 'portalcategory_name', 'portalcategory_articles', 'portalcategory_allowpublish', 'portalcategory_allowcomment', 'portalcategory_is_closed', 'setindex', 'operation', 'portalcategory_article_op'), 'header', $tdstyle);
		foreach ($portalcategory as $key=>$value) {
			if($value['level'] == 0) {
				echo showcategoryrow($key, 0, '');
			}
		}
		echo '<tbody><tr><td>&nbsp;</td><td colspan="6"><div><a class="addtr" href="'.ADMINSCRIPT.'?action=portalcategory&operation=add&upid=0">'.cplang('portalcategory_addcategory').'</a></div></td><td colspan="3">&nbsp;</td></tr></tbody>';
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();

		$langs = array();
		$keys = array('portalcategory_addcategory', 'portalcategory_addsubcategory', 'portalcategory_addthirdcategory');
		foreach ($keys as $key) {
			$langs[$key] = cplang($key);
		}
		echo <<<SCRIPT
<script type="text/Javascript">
var rowtypedata = [
	[[1,'', ''], [4, '<div class="parentboard"><input type="text" class="txt" value="$lang[portalcategory_addcategory]" name="newname[{1}][]"/></div>']],
	[[1,'<input type="text" class="txt" name="neworder[{1}][]" value="0" />', 'td25'], [4, '<div class="board"><input type="text" class="txt" value="$lang[portalcategory_addsubcategory]" name="newname[{1}][]"/>  <input type="checkbox" name="newinheritance[{1}][]" value="1" checked>$lang[portalcategory_inheritance]</div>']],
	[[1,'<input type="text" class="txt" name="neworder[{1}][]" value="0" />', 'td25'], [4, '<div class="childboard"><input type="text" class="txt" value="$lang[portalcategory_addthirdcategory]" name="newname[{1}][]"/> <input type="checkbox" name="newinheritance[{1}][]" value="1" checked>$lang[portalcategory_inheritance]</div>']],
];
</script>
SCRIPT;

	} else {
		$cachearr = array('portalcategory');
		// 修改
		if($_POST['name']) {
			$openarr = $closearr = array();
			foreach($_POST['name'] as $key=>$value) {
				$sets = array();
				$value = trim($value);
				if($portalcategory[$key] && $portalcategory[$key]['catname'] != $value) {
					$sets['catname'] = $value;
				}
				if($portalcategory[$key] && $portalcategory[$key]['displayorder'] != $_POST['neworder'][$key]) {
					$sets['displayorder'] = $_POST['neworder'][$key];
				}
				if($sets) {
					//DB::query('UPDATE '.DB::table('portal_category')." SET ".implode(',',$sets)." WHERE catid = '$key'");
					C::t('portal_category')->update($key, $sets);
					//DB::update('common_diy_data',array('name'=>$value),array('targettplname'=>'portal/list_'.$key));
					C::t('common_diy_data')->update('portal/list_'.$key, getdiydirectory($portalcategory[$key]['primaltplname']), array('name'=>$value));
					C::t('common_diy_data')->update('portal/view_'.$key, getdiydirectory($portalcategory[$key]['articleprimaltplname']), array('name'=>$value));
					$cachearr[] = 'diytemplatename';
				}
			}
		}

		if($_GET['newsetindex']) {
			//DB::insert('common_setting', array('skey' => 'defaultindex', 'svalue' => $portalcategory[$_GET['newsetindex']]['caturl']), 0, 1);
			C::t('common_setting')->update('defaultindex', $portalcategory[$_GET['newsetindex']]['caturl']);
			$cachearr[] = 'setting';
		}
		// 更新缓存
		include_once libfile('function/cache');
		updatecache($cachearr);

		cpmsg('portalcategory_update_succeed', 'action=portalcategory', 'succeed');
	}

} elseif($operation == 'perm') {

	$catid = intval($_GET['catid']);
	if(!submitcheck('permsubmit')) {
		//取出文章分类
		//$category = DB::fetch_first('SELECT * FROM '.DB::table('portal_category')." WHERE catid='$catid'");
		$category = C::t('portal_category')->fetch($catid);
		shownav('portal', 'portalcategory');
		$upcat = $category['upid'] ? ' - <a href="'.ADMINSCRIPT.'?action=portalcategory&operation=perm&catid='.$category['upid'].'">'.$portalcategory[$category['upid']]['catname'].'</a> ' : '';
		showsubmenu('<a href="'.ADMINSCRIPT.'?action=portalcategory">'.cplang('portalcategory_perm_edit').'</a>'.$upcat.' - '.$category['catname']);
		showtips('portalcategory_article_perm_tips');
		showformheader("portalcategory&operation=perm&catid=$catid");

		showtableheader('', 'fixpadding');

		$inherited_checked = !$category['notinheritedarticle'] ? 'checked' : '';
		if($portalcategory[$catid]['level'])showsubtitle(array('','<input class="checkbox" type="checkbox" name="inherited" value="1" '.$inherited_checked.'/>'.cplang('portalcategory_inheritance'),'',''));
		showsubtitle(array('', 'username',
		'<input class="checkbox" type="checkbox" name="chkallpublish" onclick="checkAll(\'prefix\', this.form, \'publish\', \'chkallpublish\')" id="chkallpublish" /><label for="chkallpublish">'.cplang('portalcategory_perm_publish').'</label>',
		'<input class="checkbox" type="checkbox" name="chkallmanage" onclick="checkAll(\'prefix\', this.form, \'manage\', \'chkallmanage\')" id="chkallmanage" /><label for="chkallmanage">'.cplang('portalcategory_perm_manage').'</label>',
		'block_perm_inherited'
		));

		$line = '&minus;';
		//$query = DB::query("SELECT m.*, cp.* FROM ".DB::table('common_member')." m ,".DB::table('portal_category_permission')." cp WHERE cp.catid='$catid' AND cp.uid=m.uid");
		//while($value = DB::fetch($query)) {
		$permissions = C::t('portal_category_permission')->fetch_all_by_catid($catid);
		$members = C::t('common_member')->fetch_all(array_keys($permissions));
		foreach($permissions as $uid => $value) {
			$value = array_merge($value, $members[$uid]);
			if(!empty($value['inheritedcatid'])) {
				showtablerow('', array('class="td25"'), array(
					"",
					"$value[username]",
					$value['allowpublish'] ? '&radic;' : $line,
					$value['allowmanage'] ? '&radic;' : $line,
					'<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=perm&catid='.$value['inheritedcatid'].'">'.$portalcategory[$value['inheritedcatid']]['catname'].'</a>',
				));
			} else {
				showtablerow('', array('class="td25"'), array(
					"<input type=\"checkbox\" class=\"checkbox\" name=\"delete[$value[uid]]\" value=\"$value[uid]\" /><input type=\"hidden\" name=\"perm[$value[uid]]\" value=\"$value[catid]\" />
					<input type=\"hidden\" name=\"perm[$value[uid]][allowpublish]\" value=\"$value[allowpublish]\" />
					<input type=\"hidden\" name=\"perm[$value[uid]][allowmanage]\" value=\"$value[allowmanage]\" />",
					"$value[username]",
					"<input type=\"checkbox\" class=\"checkbox\" name=\"allowpublish[$value[uid]]\" value=\"1\" ".($value['allowpublish'] ? 'checked' : '').' />',
					"<input type=\"checkbox\" class=\"checkbox\" name=\"allowmanage[$value[uid]]\" value=\"1\" ".($value['allowmanage'] ? 'checked' : '').' />',
					$line,
				));
			}
		}
		showtablerow('', array('class="td25"'), array(
			cplang('add_new'),
			'<input type="text" class="txt" name="newuser" value="" size="20" />',
			'<input type="checkbox" class="checkbox" name="newpublish" value="1" />',
			'<input type="checkbox" class="checkbox" name="newmanage" value="1" />',
			'',
		));

		showsubmit('permsubmit', 'submit', 'del');
		showtablefooter();
		showformfooter();
	} else {

		//更新用户
		$users = array();
		if(is_array($_GET['perm'])) {
			foreach($_GET['perm'] as $uid => $value) {
				if(empty($_GET['delete']) || !in_array($uid, $_GET['delete'])) {
					$user = array();
					$user['allowpublish'] = $_GET['allowpublish'][$uid] ? 1 : 0;
					$user['allowmanage'] = $_GET['allowmanage'][$uid] ? 1 : 0;
					//过滤修改
					if($value['allowpublish'] != $user['allowpublish'] || $value['allowmanage'] != $user['allowmanage']) {
						$user['uid'] = intval($uid);
						$users[] = $user;
					}
				}
			}
		}
		//添加新用户
		if(!empty($_GET['newuser'])) {
			//$value = DB::fetch_first("SELECT uid FROM ".DB::table('common_member')." WHERE username='$_GET[newuser]'");
			$newuid = C::t('common_member')->fetch_uid_by_username($_GET['newuser']);
			if($newuid) {
				$user['uid'] = $newuid;
				$user['allowpublish'] = $_GET['newpublish'] ? 1 : 0;
				$user['allowmanage'] = $_GET['newmanage'] ? 1 : 0;
				$users[$user['uid']] = $user;
			} else {
				cpmsg_error($_GET['newuser'].cplang('portalcategory_has_no_allowauthorizedarticle'));
			}
		}

		require_once libfile('class/portalcategory');
		$categorypermsission = & portal_category::instance();
		if(!empty($users)) {
			$categorypermsission->add_users_perm($catid, $users);
		}

		//删除用户
		if(!empty($_GET['delete'])) {
			$categorypermsission->delete_users_perm($catid, $_GET['delete']);
		}

		//更新分类的继承权限
		$notinherited = !$_POST['inherited'] ? '1' : '0';
		if($notinherited != $portalcategory[$catid]['notinheritedarticle']) {
			if($notinherited) {
				$categorypermsission->delete_inherited_perm_by_catid($catid, $portalcategory[$catid]['upid']);
			} else {
				$categorypermsission->remake_inherited_perm($catid);
			}
			//DB::update('portal_category', array('notinheritedarticle'=>$notinherited), array('catid'=>$catid));
			C::t('portal_category')->update($catid, array('notinheritedarticle'=>$notinherited));
		}

		// 更新缓存
		include_once libfile('function/cache');
		updatecache('portalcategory');

		cpmsg('portalcategory_perm_update_succeed', "action=portalcategory&operation=perm&catid=$catid", 'succeed');
	}

} elseif($operation == 'delete') {// 删除分类

	$_GET['catid'] = max(0, intval($_GET['catid']));
	if(!$_GET['catid'] || !$portalcategory[$_GET['catid']]) {
		cpmsg('portalcategory_catgory_not_found', '', 'error');
	}
	$catechildren = $portalcategory[$_GET['catid']]['children'];
	// 更新缓存
	include_once libfile('function/cache');
	if(!submitcheck('deletesubmit')) {
		// 如果该分类下没有文章且没有子分类，直接删除；
		// 如果有，提示移动到其他分类；如果该分类有子分类，提示子分类删除或移动到上级分类
		//$article_count = DB::result_first('SELECT COUNT(*) FROM '.DB::table('portal_article_title')." WHERE catid = '$_GET[catid]'");
		$article_count = C::t('portal_article_title')->fetch_count_for_cat($_GET['catid']);
		if(!$article_count && empty($catechildren)) {// 直接删除

			//删除频道分类的目录
			if($portalcategory[$_GET['catid']]['foldername']) delportalcategoryfolder($_GET['catid']);

			deleteportalcategory($_GET['catid']);
			updatecache(array('portalcategory','diytemplatename'));
			cpmsg('portalcategory_delete_succeed', 'action=portalcategory', 'succeed');
		}

		shownav('portal', 'portalcategory');
		showsubmenu('portalcategory',  array(
			array('list', 'portalcategory', 0),
			array('delete', 'portalcategory&operation=delete&catid='.$_GET['catid'], 1)
		));

		showformheader('portalcategory&operation=delete&catid='.$_GET['catid']);
		showtableheader();
		if($portalcategory[$_GET[catid]]['children']) {
			showsetting('portalcategory_subcategory_moveto', '', '',
				'<input type="radio" name="subcat_op" value="trash" id="subcat_op_trash" checked="checked" />'.
				'<label for="subcat_op_trash" />'.cplang('portalcategory_subcategory_moveto_trash').'</label>'.
				'<input type="radio" name="subcat_op" value="parent" id="subcat_op_parent" checked="checked" />'.
				'<label for="subcat_op_parent" />'.cplang('portalcategory_subcategory_moveto_parent').'</label>'
			);
		}
		include_once libfile('function/portalcp');
		echo "<tr><td colspan=\"2\" class=\"td27\">".cplang('portalcategory_article').":</td></tr>
				<tr class=\"noborder\">
					<td class=\"vtop rowform\">
						<ul class=\"nofloat\" onmouseover=\"altStyle(this);\">
						<li class=\"checked\"><input class=\"radio\" type=\"radio\" name=\"article_op\" value=\"move\" checked />&nbsp;".cplang('portalcategory_article_moveto')."&nbsp;&nbsp;&nbsp;".category_showselect('portal', 'tocatid', false, $portalcategory[$_GET['catid']]['upid'])."</li>
						<li><input class=\"radio\" type=\"radio\" name=\"article_op\" value=\"delete\" />&nbsp;".cplang('portalcategory_article_delete')."</li>
						</ul></td>
					<td class=\"vtop tips2\"></td>
				</tr>";

		showsubmit('deletesubmit', 'portalcategory_delete');
		showtablefooter();
		showformfooter();

	} else {

		if($_POST['article_op'] == 'delete') {
			if(!$_GET['confirmed']) {
				cpmsg('portal_delete_confirm', "action=portalcategory&operation=delete&catid=$_GET[catid]", 'form', array(),
				'<input type="hidden" class="btn" id="deletesubmit" name="deletesubmit" value="1" /><input type="hidden" class="btn" id="subcat_op" name="subcat_op" value="'.$_POST[subcat_op].'" />
					<input type="hidden" class="btn" id="article_op" name="article_op" value="delete" /><input type="hidden" class="btn" id="tocatid" name="tocatid" value="'.$_POST[tocatid].'" />');
			}
		}

		if($_POST['article_op'] == 'move') {
			if($_POST['tocatid'] == $_GET['catid'] || empty($portalcategory[$_POST['tocatid']])) {// 无法移动分类
				cpmsg('portalcategory_move_category_failed', 'action=portalcategory', 'error');
			}
		}

		$delids = array($_GET['catid']);
		$updatecategoryfile = array();
		if($catechildren) {// 含有子分类
			if($_POST['subcat_op'] == 'parent') {// 移动到上级分类
				$upid = intval($portalcategory[$_GET['catid']]['upid']);
				//转移目录
				if(!empty($portalcategory[$upid]['foldername']) || ($portalcategory[$_GET['catid']]['level'] == '0' && $portalcategory[$_GET['catid']]['foldername'])) {
					$parentdir = DISCUZ_ROOT.'/'.getportalcategoryfulldir($upid);
					foreach($catechildren as $subcatid) {
						if($portalcategory[$subcatid]['foldername']) {
							$olddir = DISCUZ_ROOT.'/'.getportalcategoryfulldir($subcatid);
							rename($olddir, $parentdir.$portalcategory[$subcatid]['foldername']);
							$updatecategoryfile[] = $subcatid;
						}
					}
				}
				//DB::query('UPDATE '.DB::table('portal_category')." SET upid = '$upid' WHERE catid IN (".dimplode($catechildren).')');
				C::t('portal_category')->update($catechildren, array('upid' => $upid));
				//删除从$_GET['catid']继承的模块权限
				require_once libfile('class/blockpermission');
				require_once libfile('class/portalcategory');
				$tplpermission = & template_permission::instance();
				$tplpermission->delete_perm_by_inheritedtpl('portal/list_'.$_GET['catid']);
				//删除从$_GET['catid']继承的文章权限
				$categorypermission = & portal_category::instance();
				$categorypermission->delete_perm_by_inheritedcatid($_GET['catid']);

			} else {// 直接删除
				$delids = array_merge($delids, $catechildren);
				foreach ($catechildren as $id) {// 子子分类
					$value = $portalcategory[$id];
					if($value['children']) {
						$delids = array_merge($delids, $value['children']);
					}
				}
				if($_POST['article_op'] == 'move') {
					if(!$portalcategory[$_POST['tocatid']] || in_array($_POST['tocatid'], $delids)) {// 无法移动分类
						cpmsg('portalcategory_move_category_failed', 'action=portalcategory', 'error');
					}
				}
			}
		}

		if($delids) {// 删除分类后文章处理
			deleteportalcategory($delids);
			if($_POST['article_op'] == 'delete') {
				require_once libfile('function/delete');
				$aidarr = array();
				//$query = DB::query("SELECT aid FROM ".DB::table('portal_article_title')." WHERE catid IN (".dimplode($delids).")");
				$query = C::t('portal_article_title')->fetch_all_for_cat($delids);
				//while($value = DB::fetch($query)) {
				foreach($query as $value) {
					$aidarr[] = $value['aid'];
				}
				if($aidarr) {
					deletearticle($aidarr, '0');
				}
			} else {
				//DB::update('portal_article_title', array('catid'=>$_POST['tocatid']), 'catid IN ('.dimplode($delids).')');
				C::t('portal_article_title')->update_for_cat($delids, array('catid'=>$_POST['tocatid']));
				// 更新分类下文章数
				//$num = DB::result_first('SELECT COUNT(*) FROM '.DB::table('portal_article_title')." WHERE catid = '$_POST[tocatid]'");
				$num = C::t('portal_article_title')->fetch_count_for_cat($_POST['tocatid']);
				//DB::update('portal_category', array('articles'=>$num), array('catid'=>$_POST['tocatid']));
				C::t('portal_category')->update($_POST['tocatid'], array('articles'=>dintval($num)));
			}
		}

		//删除频道分类的目录
		if($portalcategory[$_GET['catid']]['foldername']) delportalcategoryfolder($_GET['catid']);
		updatecache(array('portalcategory','diytemplatename'));
		loadcache('portalcategory', true);
		//更新生成目录的index.php文件
		remakecategoryfile($updatecategoryfile);
		cpmsg('portalcategory_delete_succeed', 'action=portalcategory', 'succeed');
	}

} elseif($operation == 'move') {// 移动文章
	$_GET['catid'] = intval($_GET['catid']);
	if(!$_GET['catid'] || !$portalcategory[$_GET['catid']]) {
		cpmsg('portalcategory_catgory_not_found', '', 'error');
	}
	if(!submitcheck('movesubmit')) {
		//$article_count = DB::result_first('SELECT COUNT(*) FROM '.DB::table('portal_article_title')." WHERE catid = '$_GET[catid]'");
		$article_count = C::t('portal_article_title')->fetch_count_for_cat($_GET['catid']);
		if(!$article_count) {// 没有文章
			cpmsg('portalcategory_move_empty_error', 'action=portalcategory', 'succeed');
		}

		shownav('portal', 'portalcategory');
		showsubmenu('portalcategory',  array(
			array('list', 'portalcategory', 0),
			array('portalcategory_move', 'portalcategory&operation=move&catid='.$_GET['catid'], 1)
		));

		showformheader('portalcategory&operation=move&catid='.$_GET['catid']);
		showtableheader();
		include_once libfile('function/portalcp');
		showsetting('portalcategory_article_moveto', '', '', category_showselect('portal', 'tocatid', false, $portalcategory[$_GET['catid']]['upid']));
		showsubmit('movesubmit', 'portalcategory_move');
		showtablefooter();
		showformfooter();

	} else {

		if($_POST['tocatid'] == $_GET['catid'] || empty($portalcategory[$_POST['tocatid']])) {// 无法移动分类
			cpmsg('portalcategory_move_category_failed', 'action=portalcategory', 'error');
		}

		// 移动文章
		//DB::query('UPDATE '.DB::table('portal_article_title')." SET catid = '$_POST[tocatid]' WHERE catid ='$_GET[catid]'");
		C::t('portal_article_title')->update_for_cat($_GET['catid'], array('catid' => $_POST['tocatid']));
		// 更新分类下文章数
		//DB::update('portal_category', array('articles'=>0), array('catid'=>$_GET['catid']));
		C::t('portal_category')->update($_GET['catid'], array('articles'=>0));
		//$num = DB::result_first('SELECT COUNT(*) FROM '.DB::table('portal_article_title')." WHERE catid = '$_POST[tocatid]'");
		$num = C::t('portal_article_title')->fetch_count_for_cat($_POST['tocatid']);
		//DB::update('portal_category', array('articles'=>$num), array('catid'=>$_POST['tocatid']));
		C::t('portal_category')->update($_POST['tocatid'], array('articles'=>$num));
		updatecache('portalcategory');

		cpmsg('portalcategory_move_succeed', 'action=portalcategory', 'succeed');
	}
} elseif($operation == 'edit' || $operation == 'add') {// 编辑频道
	$_GET['catid'] = intval($_GET['catid']);
	$anchor = in_array($_GET['anchor'], array('basic', 'html')) ? $_GET['anchor'] : 'basic';

	if($_GET['catid'] && !$portalcategory[$_GET['catid']]) {
		cpmsg('portalcategory_catgory_not_found', '', 'error');
	}

	$cate = $_GET['catid'] ? $portalcategory[$_GET['catid']] : array();
	if($operation == 'add') {
		$_GET['upid'] = intval($_GET['upid']);
		if($_GET['upid']) {
			$cate['level'] = $portalcategory[$_GET['upid']] ? $portalcategory[$_GET['upid']]['level']+1 : 0;
			$cate['upid'] = intval($_GET['upid']);
		} else {
			$cate['level'] = 0;
			$cate['upid'] = 0;
		}
		$cate['displayorder'] = 0;
		$cate['closed'] = 1;
	}
	@include_once DISCUZ_ROOT.'./data/cache/cache_domain.php';
	$channeldomain = isset($rootdomain['channel']) && $rootdomain['channel'] ? $rootdomain['channel'] : array();

	if(!submitcheck('detailsubmit')) {
		shownav('portal', 'portalcategory');
		$url = 'portalcategory&operation='.$operation.($operation == 'add' ? '&upid='.$_GET['upid'] : '&catid='.$_GET['catid']);
		showsubmenuanchors(cplang('portalcategory_detail').($cate['catname'] ? ' - '.$cate['catname'] : ''), array(
			array('edit', 'basic', $anchor == 'basic'),
			//array('生成html', 'html', $anchor == 'html')
		));

		showtagheader('div', 'basic', $anchor == 'basic');
		showformheader($url);
		showtableheader();
		$catemsg = '';
		if($cate['username']) $catemsg .= $lang['portalcategory_username'].' '.$cate['username'];
		if($cate['dateline']) $catemsg .= ' '.$lang['portalcategory_dateline'].' '.dgmdate($cate['dateline'],'Y-m-d m:i:s');
		if($cate['upid']) $catemsg .= ' '.$lang['portalcategory_upname'].': <a href="'.ADMINSCRIPT.'?action=portalcategory&operation=edit&catid='.$cate['upid'].'">'.$portalcategory[$cate['upid']]['catname'].'</a>';
		if($catemsg) showtitle($catemsg);
		showsetting('portalcategory_catname', 'catname', html_entity_decode($cate['catname']), 'text');
		showsetting('display_order', 'displayorder', $cate['displayorder'], 'text');
		showsetting('portalcategory_foldername', 'foldername', $cate['foldername'], 'text');
		showsetting('portalcategory_url', 'url', $cate['url'], 'text');
		showsetting('portalcategory_perpage', 'perpage', $cate['perpage'] ? $cate['perpage'] : 15, 'text');
		showsetting('portalcategory_maxpages', 'maxpages', $cate['maxpages'] ? $cate['maxpages'] : 1000, 'text');

		//列表页模板
		showportalprimaltemplate($cate['primaltplname'], 'list');
		//列表页模板
		showportalprimaltemplate($cate['articleprimaltplname'], 'view');

		showsetting('portalcategory_allowpublish', 'allowpublish', $cate['disallowpublish'] ? 0 : 1, 'radio');
		showsetting('portalcategory_notshowarticlesummay', 'notshowarticlesummay', $cate['notshowarticlesummay'] ? 0 : 1, 'radio');
		showsetting('portalcategory_allowcomment', 'allowcomment', $cate['allowcomment'], 'radio');
		if($cate['level']) {
			showsetting('portalcategory_inheritancearticle', 'inheritancearticle', !$cate['notinheritedarticle'] ? '1' : '0', 'radio');
			showsetting('portalcategory_inheritanceblock', 'inheritanceblock', !$cate['notinheritedblock'] ? '1' : '0', 'radio');
		}
		showsetting('portalcategory_is_closed', 'closed', $cate['closed'] ? 0 : 1, 'radio');
		if($cate['level'] != 2) showsetting('portalcategory_shownav', 'shownav', $cate['shownav'], 'radio');
		$setindex = !empty($_G['setting']['defaultindex']) && $_G['setting']['defaultindex'] == $cate['caturl'] ? 1 : 0;
		showsetting('setindex', 'setindex', $setindex, 'radio');
		if($cate['level'] == 0) {
			if(!empty($_G['setting']['domain']['root']['channel'])) {
				showsetting('forums_edit_extend_domain', '', '', 'http://<input type="text" class="txt" name="domain" class="txt" value="'.$cate['domain'].'" style="width:100px; margin-right:0px;" >.'.$_G['setting']['domain']['root']['channel']);
			} else {
				showsetting('forums_edit_extend_domain', 'domain', '', 'text', 'disabled');
			}
		}
		showsetting('portalcategory_noantitheft', 'noantitheft', $cate['noantitheft'], 'radio');
		showtablefooter();
		showtips('setting_seo_portal_tips', 'tips', true, 'setseotips');
		showtableheader();
		showsetting('portalcategory_seotitle', 'seotitle', $cate['seotitle'], 'text');
		showsetting('portalcategory_keyword', 'keyword', $cate['keyword'], 'text');
		showsetting('portalcategory_summary', 'description', $cate['description'], 'textarea');
		showtablefooter();

		/*
		//生存HTML
		showtagfooter('div');
		showtagheader('div', 'html', $anchor == 'html');
		showtableheader();
		if($catemsg) showtitle($catemsg);

		showsetting('portalcategory_automakehtml', 'automakehtml', $cate['automakehtml'], 'radio');

		showsetting('portalcategory_makehtml', 'makehtml', $cate['makehtml'], 'radio', 0, 1);
		showsetting('portalcategory_htmlname', 'htmlname', $cate['htmlname'] ? $cate['htmlname'] : 'index', 'text');
		showtagfooter('tbody');

		showsetting('portalcategory_makearticlehtml', 'makearticlehtml', $cate['makearticlehtml'], 'radio', 0, 1);
		showsetting('portalcategory_articlehtmldir', 'articlehtmldir', $cate['articlehtmldir'], 'text');
		$dirformat = array('htmldirformat',
				array(array(0, dgmdate(TIMESTAMP, '/Ym/')),
					array(1, dgmdate(TIMESTAMP, '/Ym/d/')),
					array(2, dgmdate(TIMESTAMP, '/Y/m/')),
					array(3, dgmdate(TIMESTAMP, '/Y/m/d/')))
			);
		showsetting('portalcategory_htmldirformat', $dirformat, $cate['htmldirformat'], 'select');
		showtagfooter('tbody');


		showtablefooter();
		showtagfooter('div');
		*/
		showsubmit('detailsubmit');
		if($operation == 'add') showsetting('', '', '', '<input type="hidden" name="level" value="'.$cate['level'].'" />');
		showtablefooter();
		showformfooter();

	} else {
		require_once libfile('function/portalcp');
		$domain = $_GET['domain'] ? $_GET['domain'] : '';
		$_GET['closed'] = intval($_GET['closed']) ? 0 : 1;
		$_GET['catname'] = trim($_GET['catname']);
		$foldername = trim($_GET['foldername']);
		$oldsetindex = !empty($_G['setting']['defaultindex']) && $_G['setting']['defaultindex'] == $cate['caturl'] ? 1 : 0;
		$perpage = intval($_GET['perpage']);
		$maxpages = intval($_GET['maxpages']);
		$perpage = empty($perpage) ? 15 : $perpage;
		$maxpages = empty($maxpages) ? 1000 : $maxpages;

		//删除该域名记录
		if($_GET['catid'] && !empty($cate['domain'])) {
			require_once libfile('function/delete');
			deletedomain($_GET['catid'], 'channel');
		}
		if(!empty($domain)) {
			require_once libfile('function/domain');
			domaincheck($domain, $_G['setting']['domain']['root']['channel'], 1);
		}

		$updatecategoryfile = array();

		/*
		//文件名称做安全检查,为空时设置为index
		if($_GET['htmlname']) {
			$htmlname = basename($_GET['htmlname']) == $_GET['htmlname'] ? $_GET['htmlname'] : 'index';
		} else {
			$htmlname = 'index';
		}

		//目录安全检查，为空时设置为$cate['fullfoldername']
		//以网站的根目录
		if($_GET['articlehtmldir']) {
			$articlehtmldir = trim($_GET['articlehtmldir'], ' /');
			preg_match_all('/[^\w\d\_\\]/',$articlehtmldir,$re);
			if(!empty($re[0])) {
				cpmsg(cplang('portalcategory_articlehtmldir_invalid').','.cplang('return'), NULL, 'error');
			}
		} else {
			$articlehtmldir = $cate['fullfoldername'];
		}*/

		$editcat = array(
			'catname' => $_GET['catname'],
			'allowcomment'=>$_GET['allowcomment'],
			'url' => $_GET['url'],
			'closed' => $_GET['closed'],
			'seotitle' => $_GET['seotitle'],
			'keyword' => $_GET['keyword'],
			'description' => $_GET['description'],
			'displayorder' => intval($_GET['displayorder']),
			'notinheritedarticle' => $_GET['inheritancearticle'] ? '0' : '1',
			'notinheritedblock' => $_GET['inheritanceblock'] ? '0' : '1',
			'disallowpublish' => $_GET['allowpublish'] ? '0' : '1',
			'notshowarticlesummay' => $_GET['notshowarticlesummay'] ? '0' : '1',
			'perpage' => $perpage,
			'maxpages' => $maxpages,
			'noantitheft' => intval($_GET['noantitheft']),
			/*'automakehtml' => intval($_GET['automakehtml']),
			'makehtml' => intval($_GET['makehtml']),
			'htmlname' => $htmlname, //文件名称做安全检查,为空时设置为index
			'makearticlehtml' => intval($_GET['makearticlehtml']),
			'articlehtmldir' => $articlehtmldir, //目录安全检查，为空时设置为$cate['fullfoldername']
			'htmldirformat' => intval($_GET['htmldirformat']),*/
		);

		//目录操作
		$dir = '';
		if(!empty($foldername)) {
			$oldfoldername = empty($_GET['catid']) ? '' : $portalcategory[$_GET['catid']]['foldername'];
			preg_match_all('/[^\w\d\_]/',$foldername,$re);
			if(!empty($re[0])) {
				cpmsg(cplang('portalcategory_foldername_rename_error').','.cplang('return'), NULL, 'error');
			}
			$parentdir = getportalcategoryfulldir($cate['upid']);
			if($parentdir === false) cpmsg(cplang('portalcategory_parentfoldername_empty').','.cplang('return'), NULL, 'error');
			//重新生成index.php文件
			if($foldername == $oldfoldername) {
				$dir = $parentdir.$foldername;
			//指定目录已经存在
			} elseif(is_dir(DISCUZ_ROOT.'./'.$parentdir.$foldername)) {
				cpmsg(cplang('portalcategory_foldername_duplicate').','.cplang('return'), NULL, 'error');
			//重命名目录
			} elseif ($portalcategory[$_GET['catid']]['foldername']) {
				$r = rename(DISCUZ_ROOT.'./'.$parentdir.$portalcategory[$_GET['catid']]['foldername'], DISCUZ_ROOT.'./'.$parentdir.$foldername);
				if($r) {
					$updatecategoryfile[] = $_GET['catid'];
					$editcat['foldername'] = $foldername;
				} else {
					cpmsg(cplang('portalcategory_foldername_rename_error').','.cplang('return'), NULL, 'error');
				}
			//新建目录
			} elseif (empty($portalcategory[$_GET['catid']]['foldername'])) {
				$dir = $parentdir.$foldername;
				$editcat['foldername'] = $foldername;
			}
		} elseif(empty($foldername) && $portalcategory[$_GET['catid']]['foldername']) {
			delportalcategoryfolder($_GET['catid']);
			$editcat['foldername'] = '';
			//cpmsg(cplang('portalcategory_foldername_is_empty').','.cplang('return'), NULL, 'error');
		}
		$primaltplname = $viewprimaltplname = '';
		if(!empty($_GET['listprimaltplname'])) {
			$primaltplname = $_GET['listprimaltplname'];
//			DEBUG(ARRAY(dsign($primaltplname), $_GET['signs']['list'][dsign($primaltplname)], $primaltplname, $_GET), 1);
			if(!isset($_GET['signs']['list'][dsign($primaltplname)])) {
				cpmsg(cplang('diy_sign_invalid').','.cplang('return'), NULL, 'error');
			}
			$checktpl = checkprimaltpl($primaltplname);
			if($checktpl !== true) {
				cpmsg(cplang($checktpl).','.cplang('return'), NULL, 'error');
			}
		}

		if(empty($_GET['viewprimaltplname'])) {
			$_GET['viewprimaltplname'] = getparentviewprimaltplname($_GET['catid']);
		} else if(!isset($_GET['signs']['view'][dsign($_GET['viewprimaltplname'])])) {
				cpmsg(cplang('diy_sign_invalid').','.cplang('return'), NULL, 'error');
		}
		$viewprimaltplname = strpos($_GET['viewprimaltplname'], ':') === false ? $_G['cache']['style_default']['tpldir'].':portal/'.$_GET['viewprimaltplname'] : $_GET['viewprimaltplname'];
		$checktpl = checkprimaltpl($viewprimaltplname);
		if($checktpl !== true) {
			cpmsg(cplang($checktpl).','.cplang('return'), NULL, 'error');
		}

		$editcat['primaltplname'] = $primaltplname;
		$editcat['articleprimaltplname'] = $viewprimaltplname;

		if($_GET['catid']) {
			//编辑单个频道
			if($portalcategory[$_G['catid']]['level'] < 2) $editcat['shownav'] = intval($_GET['shownav']);
			if($domain && $portalcategory[$_G['catid']]['level'] == 0) {
				$editcat['domain'] = $domain;
			} else {
				$editcat['domain'] = '';
			}
		} else {
			if($portalcategory[$cate['upid']]) {
				//二级子频道可以设置是否显示在导航中
				if($portalcategory[$cate['upid']]['level'] == 0) $editcat['shownav'] = intval($_GET['shownav']);
			} else {
				//一级频道设置是否导航和域名
				$editcat['shownav'] = intval($_GET['shownav']);
				$editcat['domain'] = $domain;
			}
		}
		$cachearr = array('portalcategory');
		//编辑
		if($_GET['catid']) {
			//更新分类表
			//DB::update('portal_category', $editcat, array('catid'=>$cate['catid']));
			C::t('portal_category')->update($cate['catid'], $editcat);
			if($cate['catname'] != $_GET['catname']) {
				//更新DIY模板名
				//DB::update('common_diy_data',array('name'=>$_GET['catname']),array('targettplname'=>'portal/list_'.$cate['catid']));
				C::t('common_diy_data')->update('portal/list_'.$cate['catid'], getdiydirectory($cate['primaltplname']), array('name'=>$_GET['catname']));
				C::t('common_diy_data')->update('portal/view_'.$cate['catid'], getdiydirectory($cate['articleprimaltplname']), array('name'=>$_GET['catname']));
				$cachearr[] = 'diytemplatename';
			}
		//添加
		} else {
			$editcat['upid'] = $cate['upid'];
			$editcat['dateline'] = TIMESTAMP;
			$editcat['uid'] = $_G['uid'];
			$editcat['username'] = $_G['username'];
			//$_GET['catid'] = DB::insert('portal_category', $editcat, TRUE);
			$_GET['catid'] = C::t('portal_category')->insert($editcat, true);
			$cachearr[] = 'diytemplatename';
		}

		if(!empty($domain)) {
//			DB::insert('common_domain', array('domain' => $domain, 'domainroot' => addslashes($_G['setting']['domain']['root']['channel']), 'id' => $_GET['catid'], 'idtype' => 'channel'));
			C::t('common_domain')->insert(array('domain' => $domain, 'domainroot' => $_G['setting']['domain']['root']['channel'], 'id' => $_GET['catid'], 'idtype' => 'channel'));
			$cachearr[] = 'setting';
		}
		//重新选择列表页模板文件
		if($_GET['listprimaltplname'] && (empty($cate['primaltplname']) || $cate['primaltplname'] != $primaltplname)) {
			remakediytemplate($primaltplname, 'portal/list_'.$_GET['catid'], $_GET['catname'], getdiydirectory($cate['primaltplname']));
		}

		//如果新模板和原模板不一样则重新生成文章内容页模板文件。
		if($cate['articleprimaltplname'] != $viewprimaltplname) {
			remakediytemplate($viewprimaltplname, 'portal/view_'.$_GET['catid'], $_GET['catname'].'-'.cplang('portalcategory_viewpage'), getdiydirectory($cate['articleprimaltplname']));
		}

		include_once libfile('function/cache');
		updatecache('portalcategory');
		loadcache('portalcategory',true);
		$portalcategory = $_G['cache']['portalcategory'];

		//添加或编辑完成且更新缓存以后进行的一些操作
		require libfile('class/blockpermission');
		$tplpermsission = & template_permission::instance();
		$tplpre = 'portal/list_';

		require libfile('class/portalcategory');
		$categorypermsission = & portal_category::instance();

		//添加
		if($operation == 'add') {
			//更新权限
			if($cate['upid'] && $_GET['catid']) {
				if(!$editcat['notinheritedblock']) {
					$tplpermsission->remake_inherited_perm($tplpre.$_GET['catid'], $tplpre.$cate['upid']);
				}
				if(!$editcat['notinheritedarticle']) {
					$categorypermsission->remake_inherited_perm($_GET['catid']);
				}
			}
		//编辑
		} elseif($operation == 'edit') {
			//计算模块的权限
			if($editcat['notinheritedblock'] != $cate['notinheritedblock']) {
				$tplname = $tplpre.$cate['catid'];
				if($editcat['notinheritedblock']) {
					$tplpermsission->delete_inherited_perm_by_tplname($tplname, $tplpre.$cate['upid']);
				} else {
					if($portalcategory[$cate['catid']]['upid']) {
						$tplpermsission->remake_inherited_perm($tplname, $tplpre.$portalcategory[$cate['catid']]['upid']);
					}
				}
			}
			//计算文章的权限
			if($editcat['notinheritedarticle'] != $cate['notinheritedarticle']) {
				if($editcat['notinheritedarticle']) {
					$categorypermsission->delete_inherited_perm_by_catid($cate['catid'], $cate['upid']);
				} else {
					$categorypermsission->remake_inherited_perm($cate['catid']);
				}
			}
		}

		//重新生成目录的index.php文件
		if(!empty($updatecategoryfile)) {
			remakecategoryfile($updatecategoryfile);
		}

		//生成目录
		if($dir) {
			if(!makecategoryfile($dir, $_GET['catid'], $domain)) {
				cpmsg(cplang('portalcategory_filewrite_error').','.cplang('return'), NULL, 'error');
			}
			remakecategoryfile($portalcategory[$_GET['catid']]['children']);
		}

		//编辑或添加一级、二级频道时同步导航
		if(($_GET['catid'] && $cate['level'] < 2) || empty($_GET['upid']) || ($_GET['upid'] && $portalcategory[$_GET['upid']]['level'] == 0)) {
//			$nav = DB::fetch_first("SELECT * FROM ".DB::table('common_nav')." WHERE `type`='4' AND identifier='$_GET[catid]'");
			$nav = C::t('common_nav')->fetch_by_type_identifier(4, $_GET['catid']);
			//在导航中显示
			if($editcat['shownav']) {
				//没有在导航中，则添加新的
				if(empty($nav)) {
					$navparentid = 0;
					//二级频道处理
					if($_GET['catid'] && $cate['level'] > 0 || !empty($_GET['upid'])) {
						$identifier = !empty($cate['upid']) ? $cate['upid'] : ($_GET['upid'] ? $_GET['upid'] : 0);
//						$navparentid = DB::result_first('SELECT id FROM '.DB::table('common_nav')." WHERE `type`='4' AND identifier='$identifier'");
						$navparent = C::t('common_nav')->fetch_by_type_identifier(4, $identifier);
						$navparentid = $navparent['id'];
						//note
						if(empty($navparentid)) {
							cpmsg(cplang('portalcategory_parentcategory_no_shownav').','.cplang('return'), NULL, 'error');
						}
					}
					$setarr = array(
						'parentid' => $navparentid,
						'name' => $editcat['catname'],
						'url' => $portalcategory[$_GET['catid']]['caturl'],
						'type' => '4',
						'available' => '1',
						'identifier' => $_GET['catid'],
					);
					//编辑或添加一级频道
					if($_GET['catid'] && $cate['level'] == 0 || empty($_GET['upid']) && empty($_GET['catid'])) {
						$setarr['subtype'] = '1';
					}
//					$navid = DB::insert('common_nav', $setarr, true);
					$navid = C::t('common_nav')->insert($setarr, true);

					//note 处理一级频道的子频道
					if($_GET['catid'] && $cate['level'] == 0) {
						if(!empty($cate['children'])) {
							foreach($cate['children'] as $subcatid) {
								if($portalcategory[$subcatid]['shownav']) {
									$setarr = array(
										'parentid' => $navid,
										'name' => $portalcategory[$subcatid]['catname'],
										'url' => $portalcategory[$subcatid]['caturl'],
										'type' => '4',
										'available' => '1',
										'identifier' => $subcatid,
									);
//									DB::insert('common_nav', $setarr);
									C::t('common_nav')->insert($setarr);
								}
							}
						}
					}

				//已经在导航中存在，直接修改
				} else {
					$setarr = array('available'=>'1','url' => $portalcategory[$_GET['catid']]['caturl']);
//					DB::update('common_nav', $setarr, array('type' => '4','identifier' => $_GET['catid']));
					C::t('common_nav')->update_by_type_identifier(4, $_GET['catid'], $setarr);
					//批量更新子频道的导航路径
					if($portalcategory[$_GET['catid']]['level'] == 0 && $portalcategory[$_GET['catid']]['children']) {
						foreach($portalcategory[$_GET['catid']]['children'] as $subcatid) {
//							DB::update('common_nav', array('url' => $portalcategory[$subcatid]['caturl']), array('type' => '4','identifier' => $subcatid));
							C::t('common_nav')->update_by_type_identifier(4, $subcatid, array('url' => $portalcategory[$subcatid]['caturl']));
						}
					}
				}
				$cachearr[] = 'setting';
			//不在导航中显示
			} else {
				if(!empty($nav)) {
//					DB::delete('common_nav', array('id'=>$nav['id']));
					C::t('common_nav')->delete($nav['id']);
					if($portalcategory[$_GET['catid']]['level'] == 0 && !empty($portalcategory[$_GET['catid']]['children'])) {
//						DB::delete('common_nav', array('parentid'=>$nav['id']));
						C::t('common_nav')->delete_by_parentid($nav['id']);
						//DB::update('portal_category', array('shownav'=>'0'), ' catid IN ('.dimplode($portalcategory[$_GET['catid']]['children']).')');
						C::t('portal_category')->update($portalcategory[$_GET['catid']]['children'], array('shownav'=>'0'));
					}
					$cachearr[] = 'setting';
				}
			}
		}

		//设为首页
		if($_GET['setindex']) {
			//DB::insert('common_setting', array('skey' => 'defaultindex', 'svalue' => $portalcategory[$_GET['catid']]['caturl']), 0, 1);
			C::t('common_setting')->update('defaultindex', $portalcategory[$_GET['catid']]['caturl']);
			$cachearr[] = 'setting';
		} elseif($oldsetindex) {
			//DB::insert('common_setting', array('skey' => 'defaultindex', 'svalue' => ''), 0, 1);
			C::t('common_setting')->update('defaultindex', '');
			$cachearr[] = 'setting';
		}

		// 更新缓存
		updatecache(array_unique($cachearr));

		$url = $operation == 'add' ? 'action=portalcategory#cat'.$_GET['catid'] : 'action=portalcategory&operation=edit&catid='.$_GET['catid'];
		cpmsg('portalcategory_edit_succeed', $url, 'succeed');
	}
}

/**
 * 显示分类栏目
 *
 * @param unknown_type $key
 * @param unknown_type $level
 * @param unknown_type $last
 * @return unknown
 */
function showcategoryrow($key, $level = 0, $last = '') {
	global $_G;

	loadcache('portalcategory');
	$value = $_G['cache']['portalcategory'][$key];
	$return = '';

	include_once libfile('function/portalcp');
	$value['articles'] = category_get_num('portal', $key);
	$publish = '';
	if(empty($_G['cache']['portalcategory'][$key]['disallowpublish'])) {
		$publish = '&nbsp;<a href="portal.php?mod=portalcp&ac=article&catid='.$key.'" target="_blank">'.cplang('portalcategory_publish').'</a>';
	}
	if($level == 2) {
		$class = $last ? 'lastchildboard' : 'childboard';
		$return = '<tr class="hover" id="cat'.$value['catid'].'"><td>&nbsp;</td><td class="td25"><input type="text" class="txt" name="neworder['.$value['catid'].']" value="'.$value['displayorder'].'" /></td><td><div class="'.$class.'">'.
		'<input type="text" class="txt" name="name['.$value['catid'].']" value="'.$value['catname'].'" />'.
		'</div>'.
		//'</td><td>'.(empty($value['makearticlehtml']) ? cplang('no') : cplang('yes')).'</td>'.
		'</td><td>'.$value['articles'].'</td>'.
		'<td>'.(empty($value['disallowpublish']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(!empty($value['allowcomment']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(empty($value['closed']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td><input class="radio" type="radio" name="newsetindex" value="'.$value['catid'].'" '.($value['caturl'] == $_G['setting']['defaultindex'] ? 'checked="checked"':'').' /></td>'.
		'<td><a href="'.$value['caturl'].'" target="_blank">'.cplang('view').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=edit&catid='.$value['catid'].'">'.cplang('edit').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=move&catid='.$value['catid'].'">'.cplang('portalcategory_move').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=delete&catid='.$value['catid'].'">'.cplang('delete').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=diytemplate&operation=perm&targettplname=portal/list_'.$value['catid'].'&tpldirectory='.getdiydirectory($value['primaltplname']).'">'.cplang('portalcategory_blockperm').'</a></td>
		<td><a href="'.ADMINSCRIPT.'?action=article&operation=list&&catid='.$value['catid'].'">'.cplang('portalcategory_articlemanagement').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=perm&catid='.$value['catid'].'">'.cplang('portalcategory_articleperm').'</a>'.$publish.'</td></tr>';
	} elseif($level == 1) {
		$return = '<tr class="hover" id="cat'.$value['catid'].'"><td>&nbsp;</td><td class="td25"><input type="text" class="txt" name="neworder['.$value['catid'].']" value="'.$value['displayorder'].'" /></td><td><div class="board">'.
		'<input type="text" class="txt" name="name['.$value['catid'].']" value="'.$value['catname'].'" />'.
		'<a class="addchildboard" href="'.ADMINSCRIPT.'?action=portalcategory&operation=add&upid='.$value['catid'].'">'.cplang('portalcategory_addthirdcategory').'</a></div>'.
		//'</td><td>'.(empty($value['makearticlehtml']) ? cplang('no') : cplang('yes')).'</td>'.
		'</td><td>'.$value['articles'].'</td>'.
		'<td>'.(empty($value['disallowpublish']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(!empty($value['allowcomment']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(empty($value['closed']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td><input class="radio" type="radio" name="newsetindex" value="'.$value['catid'].'" '.($value['caturl'] == $_G['setting']['defaultindex'] ? 'checked="checked"':'').' /></td>'.
		'<td><a href="'.$value['caturl'].'" target="_blank">'.cplang('view').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=edit&catid='.$value['catid'].'">'.cplang('edit').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=move&catid='.$value['catid'].'">'.cplang('portalcategory_move').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=delete&catid='.$value['catid'].'">'.cplang('delete').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=diytemplate&operation=perm&targettplname=portal/list_'.$value['catid'].'&tpldirectory='.getdiydirectory($value['primaltplname']).'">'.cplang('portalcategory_blockperm').'</a></td>
		<td><a href="'.ADMINSCRIPT.'?action=article&operation=list&&catid='.$value['catid'].'">'.cplang('portalcategory_articlemanagement').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=perm&catid='.$value['catid'].'">'.cplang('portalcategory_articleperm').'</a>'.$publish.'</td></tr>';
		for($i=0,$L=count($value['children']); $i<$L; $i++) {
			$return .= showcategoryrow($value['children'][$i], 2, $i==$L-1);
		}
	} else {
		$childrennum = count($_G['cache']['portalcategory'][$key]['children']);
		$toggle = $childrennum > 25 ? ' style="display:none"' : '';
		$return = '<tbody><tr class="hover" id="cat'.$value['catid'].'"><td onclick="toggle_group(\'group_'.$value['catid'].'\')"><a id="a_group_'.$value['catid'].'" href="javascript:;">'.($toggle ? '[+]' : '[-]').'</a></td>'
		.'<td class="td25"><input type="text" class="txt" name="neworder['.$value['catid'].']" value="'.$value['displayorder'].'" /></td><td><div class="parentboard">'.
		'<input type="text" class="txt" name="name['.$value['catid'].']" value="'.$value['catname'].'" />'.
		'</div>'.
		//'</td><td>'.(empty($value['makearticlehtml']) ? cplang('no') : cplang('yes')).'</td>'.
		'</td><td>'.$value['articles'].'</td>'.
		'<td>'.(empty($value['disallowpublish']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(!empty($value['allowcomment']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td>'.(empty($value['closed']) ? cplang('yes') : cplang('no')).'</td>'.
		'<td><input class="radio" type="radio" name="newsetindex" value="'.$value['catid'].'" '.($value['caturl'] == $_G['setting']['defaultindex'] ? 'checked="checked"':'').' /></td>'.
		'<td><a href="'.$value['caturl'].'" target="_blank">'.cplang('view').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=edit&catid='.$value['catid'].'">'.cplang('edit').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=move&catid='.$value['catid'].'">'.cplang('portalcategory_move').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=delete&catid='.$value['catid'].'">'.cplang('delete').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=diytemplate&operation=perm&targettplname=portal/list_'.$value['catid'].'&tpldirectory='.getdiydirectory($value['primaltplname']).'">'.cplang('portalcategory_blockperm').'</a></td>
		<td><a href="'.ADMINSCRIPT.'?action=article&operation=list&&catid='.$value['catid'].'">'.cplang('portalcategory_articlemanagement').'</a>&nbsp;
		<a href="'.ADMINSCRIPT.'?action=portalcategory&operation=perm&catid='.$value['catid'].'">'.cplang('portalcategory_articleperm').'</a>'.$publish.'</td></tr></tbody>
		<tbody id="group_'.$value['catid'].'"'.$toggle.'>';
		for($i=0,$L=count($value['children']); $i<$L; $i++) {
			$return .= showcategoryrow($value['children'][$i], 1, '');
		}
		$return .= '</tdoby><tr><td>&nbsp;</td><td colspan="9"><div class="lastboard"><a class="addtr" href="'.ADMINSCRIPT.'?action=portalcategory&operation=add&upid='.$value['catid'].'">'.cplang('portalcategory_addsubcategory').'</a></td></div>';
	}
	return $return;
}

function deleteportalcategory($ids) {
	global $_G;

	if(empty($ids)) return false;
	//note 删除绑定域名
	if(!is_array($ids) && $_G['cache']['portalcategory'][$ids]['upid'] == 0) {
		@require_once libfile('function/delete');
		deletedomain(intval($ids), 'channel');
	}
	if(!is_array($ids)) $ids = array($ids);

	//删除模块权限
	require_once libfile('class/blockpermission');
	require_once libfile('class/portalcategory');
	$tplpermission = & template_permission::instance();
	$templates = array();
	foreach($ids as $id) {
		$templates[] = 'portal/list_'.$id;
		$templates[] = 'portal/view_'.$id;
	}
	$tplpermission->delete_allperm_by_tplname($templates);
	//处理频道文章权限
	$categorypermission = & portal_category::instance();
	$categorypermission->delete_allperm_by_catid($ids);

	//DB::delete('portal_category', "catid IN (".dimplode($ids).")");
	C::t('portal_category')->delete($ids);
//	DB::delete('common_nav', "`type`='4' AND identifier IN (".dimplode($ids).")");
	C::t('common_nav')->delete_by_type_identifier(4, $ids);

	$tpls = $defaultindex = array();
	foreach($ids as $id) {
//		deletecatdiyfiles($id);
		$defaultindex[] = $_G['cache']['portalcategory'][$id]['caturl'];
		$tpls[] = 'portal/list_'.$id;
		$tpls[] = 'portal/view_'.$id;
	}
	if(in_array($_G['setting']['defaultindex'], $defaultindex)) {
		//DB::insert('common_setting', array('skey' => 'defaultindex', 'svalue' => ''), 0, 1);
		C::t('common_setting')->update('defaultindex', '');
	}
	//$wheresql = "targettplname IN (".dimplode($tpls).")";
	//DB::delete('common_diy_data', $wheresql);
	C::t('common_diy_data')->delete($tpls, NULL);
	//DB::delete('common_template_block', $wheresql);
	C::t('common_template_block')->delete_by_targettplname($tpls);

}

//function deletecatdiyfiles($id){
//	global $_G;
//	$listdirectory = getdiydirectory($_G['cache']['portalcategory'][$id]['primaltplname']);
//	$articledirectory = getdiydirectory($_G['cache']['portalcategory'][$id]['articleprimaltplname']);
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$listdirectory.'/portal/list_'.$id.'.htm');
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$listdirectory.'/portal/list_'.$id.'.htm.bak');
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$listdirectory.'/portal/list_'.$id.'_diy_preview.htm');
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$articledirectory.'/portal/view_'.$id.'.htm');
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$articledirectory.'/portal/view_'.$id.'.htm.bak');
//	@unlink(DISCUZ_ROOT.'./data/diy/'.$articledirectory.'/portal/view_'.$id.'_diy_preview.htm');
//}

//生成目录中的index.php文件
function makecategoryfile($dir, $catid, $domain) {
	dmkdir(DISCUZ_ROOT.'./'.$dir, 0777, FALSE);
	$portalcategory = getglobal('cache/portalcategory');
	$prepath = str_repeat('../', $portalcategory[$catid]['level']+1);
	if($portalcategory[$catid]['level']) {
		$upid = $portalcategory[$catid]['upid'];
		while($portalcategory[$upid]['upid']) {
			$upid = $portalcategory[$upid]['upid'];
		}
		$domain = $portalcategory[$upid]['domain'];
	}

	$sub_dir = $dir;
	//去掉domain
//	if($domain) {
//		$folderarr = explode('/', $dir);
//		$sub_dir = implode('/', (array)array_slice((array)$folderarr, 1));
//	}
	if($sub_dir) {
		$sub_dir = substr($sub_dir, -1, 1) == '/' ? '/'.$sub_dir : '/'.$sub_dir.'/';
	}
	$code = "<?php
chdir('$prepath');
define('SUB_DIR', '$sub_dir');
\$_GET['mod'] = 'list';
\$_GET['catid'] = '$catid';
require_once './portal.php';
?>";
	$r = file_put_contents($dir.'/index.php', $code);
	return $r;
}
//得到完整目录（包含父级目录）结构
function getportalcategoryfulldir($catid) {
	if(empty($catid)) return '';
	$portalcategory = getglobal('cache/portalcategory');
	$curdir = $portalcategory[$catid]['foldername'];
	$curdir = $curdir ? $curdir : '';
	if($catid && empty($curdir)) return FALSE;
	$upid = $portalcategory[$catid]['upid'];
	while($upid) {
		$updir = $portalcategory[$upid]['foldername'];
		if(!empty($updir)) {
			$curdir = $updir.'/'.$curdir;
		} else {
			return FALSE;
		}
		$upid = $portalcategory[$upid]['upid'];
	}
	return $curdir ? $curdir.'/' : '';
}

function delportalcategoryfolder($catid) {
	if(empty($catid)) return FALSE;
	$updatearr = array();
	$portalcategory = getglobal('cache/portalcategory');
	$children = $portalcategory[$catid]['children'];
	if($children) {
		foreach($children as $subcatid) {
			if($portalcategory[$subcatid]['foldername']) {
				$arr = delportalcategorysubfolder($subcatid);
				$updatearr = array_merge($updatearr, $arr);
			}
		}
	}

	$dir = getportalcategoryfulldir($catid);
	if(!empty($dir)) {
		unlink(DISCUZ_ROOT.$dir.'index.html');
		unlink(DISCUZ_ROOT.$dir.'index.php');
		//TODO 删除其它文件的，确保可以正常删除文件夹
		rmdir(DISCUZ_ROOT.$dir);
		$updatearr[] = $catid;
	}
	if(dimplode($updatearr)) {
		//DB::update('portal_category',array('foldername'=>''), 'catid IN('.dimplode($updatearr).')');
		C::t('portal_category')->update($updatearr, array('foldername'=>''));
	}
}

function delportalcategorysubfolder($catid) {
	if(empty($catid)) return FALSE;
	$updatearr = array();
	$portalcategory = getglobal('cache/portalcategory');
	$children = $portalcategory[$catid]['children'];
	if($children) {
		foreach($children as $subcatid) {
			if($portalcategory[$subcatid]['foldername']) {
				$arr = delportalcategorysubfolder($subcatid);
				$updatearr = array_merge($updatearr, $arr);
			}
		}
	}

	$dir = getportalcategoryfulldir($catid);
	if(!empty($dir)) {
		unlink(DISCUZ_ROOT.$dir.'index.html');
		unlink(DISCUZ_ROOT.$dir.'index.php');
		rmdir(DISCUZ_ROOT.$dir);
		$updatearr[] = $catid;
	}
	return $updatearr;
}

//重新生成目录的index.php
function remakecategoryfile($categorys) {
	if(is_array($categorys)) {
		$portalcategory = getglobal('cache/portalcategory');
		foreach($categorys as $subcatid) {
			$dir = getportalcategoryfulldir($subcatid);
			makecategoryfile($dir, $subcatid, $portalcategory[$subcatid]['domain']);
			if($portalcategory[$subcatid]['children']) {
				remakecategoryfile($portalcategory[$subcatid]['children']);
			}
		}
	}
}

/**
 * 显示列表模板和文章模板select
 * @param <string> $pritplname 模板名称
 * @param <string> $type 类型：list,view
 */
function showportalprimaltemplate($pritplname, $type) {
	include_once libfile('function/portalcp');
	//可选择的模板文件
	$tpls = array('./template/default:portal/'.$type=>getprimaltplname('portal/'.$type.'.htm'));
	foreach($alltemplate = C::t('common_template')->range() as $template) {
		//if(($allfiles = scandir(DISCUZ_ROOT.$template['directory'].'/portal/'))) {
		if(($dir = dir(DISCUZ_ROOT.$template['directory'].'/portal/'))) {
			//foreach($allfiles as $file) {
			while(false !== ($file = $dir->read())) {
				$file = strtolower($file);
				if (fileext($file) == 'htm' && substr($file, 0, strlen($type)+1) == $type.'_') {
					$key = $template['directory'].':portal/'.str_replace('.htm','',$file);
					$tpls[$key] = getprimaltplname($template['directory'].':portal/'.$file);
				}
			}
		}
	}

	foreach($tpls as $key => $value) {
		echo "<input name=signs[$type][".dsign($key)."] value='1' type='hidden' />";
	}

	$pritplvalue = '';
	if(empty($pritplname)) {
		$pritplhide = '';
		$pritplvalue = ' style="display:none;"';
	} else {
		$pritplhide = ' style="display:none;"';
	}
	$catetplselect = '<span'.$pritplhide.'><select id="'.$type.'select" name="'.$type.'primaltplname">';
	$selectedvalue = '';
	if($type == 'view') {
		$catetplselect .= '<option value="">'.cplang('portalcategory_inheritupsetting').'</option>';
	}
	foreach($tpls as $k => $v){
		if($pritplname === $k) {
			$selectedvalue = $k;
			$selected = ' selected';
		} else {
			$selected = '';
		}
		$catetplselect .= '<option value="'.$k.'"'.$selected.'>'.$v.'</option>';
	}
	$pritplophide = !empty($pritplname) ? '' : ' style="display:none;"';
	$catetplselect .= '</select> <a href="javascript:;"'.$pritplophide.' onclick="$(\''.$type.'select\').value=\''.$selectedvalue.'\';$(\''.$type.'select\').parentNode.style.display=\'none\';$(\''.$type.'value\').style.display=\'\';">'.cplang('cancel').'</a></span>';

	if(empty($pritplname)) {
		showsetting('portalcategory_'.$type.'primaltplname', '', '', $catetplselect);
	} else {
		$tplname = getprimaltplname($pritplname.'.htm');
		$html = '<span id="'.$type.'value" '.$pritplvalue.'> '.$tplname.'<a href="javascript:;" onclick="$(\''.$type.'select\').parentNode.style.display=\'\';$(\''.$type.'value\').style.display=\'none\';"> '.cplang('modify').'</a></span>';
		showsetting('portalcategory_'.$type.'primaltplname', '', '', $catetplselect.$html);
	}
}

/**
 * 重新生成DIY记录和文件
 * @param <string> $primaltplname 原模板名
 * @param <string> $targettplname 目标模板名
 * @param <string> $diytplname diy页面标识
 * @return <bool> true
 */
function remakediytemplate($primaltplname, $targettplname, $diytplname, $olddirectory){
	global $_G;
	if(empty($targettplname)) return false;
	$tpldirectory = '';
	if(strpos($primaltplname, ':') !== false) {
		list($tpldirectory, $primaltplname) = explode(':', $primaltplname);
	}
	$tpldirectory = ($tpldirectory ? $tpldirectory : $_G['cache']['style_default']['tpldir']);
	//新页面的DIY数据
	$newdiydata = C::t('common_diy_data')->fetch($targettplname, $tpldirectory);
	if($newdiydata) {
		return false;
	}
	//$diydata = DB::fetch_first("SELECT diycontent FROM ".DB::table('common_diy_data')." WHERE targettplname='$targettplname'");
	$diydata = C::t('common_diy_data')->fetch($targettplname, $olddirectory);
	$diycontent = empty($diydata['diycontent']) ? '' : $diydata['diycontent'];
	if($diydata) {
		//DB::update('common_diy_data',array('primaltplname'=>$primaltplname),array('targettplname'=>$targettplname));
		C::t('common_diy_data')->update($targettplname, $olddirectory, array('primaltplname'=>$primaltplname, 'tpldirectory'=>$tpldirectory));//注意：此处不删除已经生成的DIY文件
	} else {
		$diycontent = '';
		if(in_array($primaltplname, array('portal/list', 'portal/view'))) {
			//$diydata = DB::fetch_first("SELECT diycontent FROM ".DB::table('common_diy_data')." WHERE targettplname='$primaltplname'");
			$diydata = C::t('common_diy_data')->fetch($targettplname, $olddirectory);
			$diycontent = empty($diydata['diycontent']) ? '' : $diydata['diycontent'];
		}
		$diyarr = array(
			'targettplname' => $targettplname,
			'tpldirectory' => $tpldirectory,
			'primaltplname' => $primaltplname,
			'diycontent' => $diycontent,
			'name' => $diytplname,
			'uid' => $_G['uid'],
			'username' => $_G['username'],
			'dateline' => TIMESTAMP,
			);
		//DB::insert('common_diy_data',$diyarr);
		C::t('common_diy_data')->insert($diyarr);
	}
	//没有DIY过，直接COPY一份文件
	if(empty($diycontent)) {
		//$file = ($_G['cache']['style_default']['tpldir'] ? $_G['cache']['style_default']['tpldir'] : './template/default').'/'.$primaltplname.'.htm';
		$file = $tpldirectory.'/'.$primaltplname.'.htm';
		if (!file_exists($file)) {
			$file = './template/default/'.$primaltplname.'.htm';
		}
		$content = @file_get_contents(DISCUZ_ROOT.$file);
		if(!$content) $content = '';
		$content = preg_replace("/\<\!\-\-\[name\](.+?)\[\/name\]\-\-\>/i", '', $content);
		file_put_contents(DISCUZ_ROOT.'./data/diy/'.$tpldirectory.'/'.$targettplname.'.htm', $content);
	} else {
		//重新生成DIY文件
		updatediytemplate($targettplname, $tpldirectory);
	}
	return true;
}

/**
 * 得到上级频道使用的文章模板，共找两级
 * @param int $catid 频道ID
 * @return string 模板名
 */
function getparentviewprimaltplname($catid) {
	global $_G;
	$tpl = 'view';
	if(empty($catid)) {
		return $tpl;
	}
	$cat = $_G['cache']['portalcategroy'][$catid];
	if(!empty($cat['upid']['articleprimaltplname'])) {
		$tpl = $cat['upid']['articleprimaltplname'];
	} else {
		$cat = $_G['cache']['portalcategroy'][$cat['upid']];
		if($cat && $cat['articleprimaltplname']) {
			$tpl = $cat['articleprimaltplname'];
		}
	}
	return $tpl;
}
?>