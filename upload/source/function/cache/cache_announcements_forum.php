<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_announcements_forum.php 26271 2011-12-07 09:15:53Z chenmengshu $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_announcements_forum() {
	$data = array();
	/*$query = DB::query("SELECT a.id, a.author, m.uid AS authorid, a.subject, a.message, a.type, a.starttime, a.endtime, a.displayorder FROM ".DB::table('forum_announcement')."
		a LEFT JOIN ".DB::table('common_member')." m ON m.username=a.author WHERE a.type!=2 AND a.groups = '' AND a.starttime<='".TIMESTAMP."' AND (endtime>='".TIMESTAMP."' OR endtime='0') ORDER BY a.displayorder, a.starttime DESC, a.id DESC LIMIT 1");*/
	
	$data = C::t('forum_announcement')->fetch_by_displayorder(TIMESTAMP);
	//if($data = DB::fetch($query)) {
	if($data) {
		$memberdata = C::t('common_member')->fetch_uid_by_username($data['author']);
		$data['authorid'] = $memberdata['uid'];
		$data['authorid'] = intval($data['authorid']);
		if(empty($data['type'])) {
			unset($data['message']);
		}
	} else {
		$data = array();
	}
	savecache('announcements_forum', $data);
}

?>