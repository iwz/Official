<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_portalcategory.php 31224 2012-07-27 03:54:18Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_portalcategory() {
	global $_G;

	/*$data = array();
	$query = DB::query("SELECT * FROM ".DB::table('portal_category')." ORDER BY displayorder,catid");
	while($value = DB::fetch($query)) {
		$value['catname'] = dhtmlspecialchars($value['catname']);
		$data[$value['catid']] = $value;
	}*/
	$data = C::t('portal_category')->range();
	//整理结构
	foreach($data as $key => $value) {
		$upid = $value['upid'];
		$data[$key]['level'] = 0;
		if($upid && isset($data[$upid])) {
			$data[$upid]['children'][] = $key;
			while($upid && isset($data[$upid])) {
				$data[$key]['level'] += 1;
				$upid = $data[$upid]['upid'];
			}
		}
	}
	//频道根域名
	$domain = $_G['setting']['domain'];
	$channelrootdomain = !empty($domain['root']) && !empty($domain['root']['channel']) ? $domain['root']['channel'] : '';
	//门户域名
	$portaldomain = '';
	if(!empty($domain['app']['portal'])) {
		$portaldomain = 'http://'.$domain['app']['portal'].$_G['siteroot'];
	} elseif(!empty($domain['app']['default'])) {
		$portaldomain = 'http://'.$domain['app']['default'].$_G['siteroot'];
	} else {
		$portaldomain = $_G['siteurl'];
	}
	foreach($data as $key => &$value){
		$url = $topid = '';
		$foldername = $value['foldername'];
		//子频道
		if($value['level']) {
			$topid = $key;
			$foldername = '';
			while($data[$topid]['upid']) {
				if($data[$topid]['foldername'] && $data[$key]['foldername']) {
					$foldername = $data[$topid]['foldername'].'/'.$foldername;
				}
				$topid = $data[$topid]['upid'];
			}
			if($foldername) $foldername = $data[$topid]['foldername'].'/'.$foldername;
		//顶级频道
		} else {
			$topid = $key;
		}
		$value['topid'] = $topid;

		//有域名和目录，将返回域名形式
		if($channelrootdomain && $data[$topid]['domain']){
			$url = 'http://'.$data[$topid]['domain'].'.'.$channelrootdomain.'/';
			if($foldername) {
				//服务器端绑定域名指定的目录时删除顶级目录
				//$folderarr = explode('/', $foldername);
				//$foldername = implode('/', (array)array_slice((array)$folderarr, 1));

				if(!empty($value['upid'])) {
					$url .= $foldername;
				}
			} else {
				$url = $portaldomain.'portal.php?mod=list&catid='.$key;
			}
		//有目录，返回目录形式
		} elseif ($foldername) {
			$url = $portaldomain.$foldername;
			if(substr($url, -1, 1) != '/') $url.= '/';
		//否则返回动态地址
		} else {
			$url = $portaldomain.'portal.php?mod=list&catid='.$key;
		}
		$value['caturl'] = $url;

		//完整的目录
		$value['fullfoldername'] = trim($foldername, '/');

		//更新导航入库
		if($value['shownav']) {
//			$rs = DB::update('common_nav', array('url' => addslashes($url), 'name' =>addslashes($value['catname'])), array('type' => '4','identifier' => $key));
			$rs = C::t('common_nav')->update_by_type_identifier(4, $key, array('url' => addslashes($url), 'name' =>$value['catname']));
		}
	}

	savecache('portalcategory', $data);

	//更新导航缓存
	if(!function_exists('get_cachedata_mainnav')) {
		include_once libfile('cache/setting','function');
	}
	$data = $_G['setting'];
	list($data['navs'], $data['subnavs'], $data['menunavs'], $data['navmns'], $data['navmn'], $data['navdms']) = get_cachedata_mainnav();
	savecache('setting', $data);
}

?>