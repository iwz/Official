<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_post.php 35198 2015-02-04 03:44:54Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function getattach($pid, $posttime = 0, $aids = '') {
	global $_G;

	require_once libfile('function/attachment');
	$attachs = $imgattachs = array();
	$aids = $aids ? explode('|', $aids) : array();
	if($aids) {
		$aidsnew = array();
		foreach($aids as $aid) {
			if($aid) {
				$aidsnew[] = intval($aid);
			}
		}
		$aids = "aid IN (".dimplode($aidsnew).") AND";
	} else {
		$aids = '';
	}
	$sqladd1 = $posttime > 0 ? "AND af.dateline>'$posttime'" : '';
	if(!empty($_G['fid']) && $_G['forum']['attachextensions']) {
		$allowext = str_replace(' ', '', strtolower($_G['forum']['attachextensions']));
		$allowext = explode(',', $allowext);
	} else {
		$allowext = '';
	}
//	$query = DB::query("SELECT a.*, af.*
//		FROM ".DB::table('forum_attachment')." a
//		INNER JOIN ".DB::table('forum_attachment_unused')." af USING(aid)
//		WHERE $aids (af.uid='$_G[uid]' AND a.tid='0' $sqladd1) ORDER BY a.aid DESC");
//	while($attach = DB::fetch($query)) {
	foreach(C::t('forum_attachment')->fetch_all_unused_attachment($_G['uid'], empty($aidsnew) ? null : $aidsnew, $posttime > 0 ? $posttime : null) as $attach) {
		$attach['filenametitle'] = $attach['filename'];
		$attach['ext'] = fileext($attach['filename']);
		if($allowext && !in_array($attach['ext'], $allowext)) {
			continue;
		}
		getattach_row($attach, $attachs, $imgattachs);
	}
	if($pid > 0) {
//		$query = DB::query("SELECT a.*, af.*
//			FROM ".DB::table('forum_attachment')." a
//			LEFT JOIN ".DB::table(getattachtablebytid($_G['tid']))." af USING(aid)
//			WHERE a.pid='$pid' ORDER BY a.aid DESC");
//		while($attach = DB::fetch($query)) {
		$attachmentns = C::t('forum_attachment_n')->fetch_all_by_id('tid:'.$_G['tid'], 'pid', $pid);
		foreach(C::t('forum_attachment')->fetch_all_by_id('pid', $pid, 'aid') as $attach) {
			$attach = array_merge($attach, $attachmentns[$attach['aid']]);
			$attach['filenametitle'] = $attach['filename'];
			$attach['ext'] = fileext($attach['filename']);
			if($allowext && !in_array($attach['ext'], $allowext)) {
				continue;
			}
			getattach_row($attach, $attachs, $imgattachs);
		}
	}
	return array('attachs' => $attachs, 'imgattachs' => $imgattachs);
}

function getattach_row($attach, &$attachs, &$imgattachs) {
	global $_G;
	$attach['filename'] = cutstr($attach['filename'], $_G['setting']['allowattachurl'] ? 25 : 30);
	$attach['attachsize'] = sizecount($attach['filesize']);
	$attach['dateline'] = dgmdate($attach['dateline']);
	$attach['filetype'] = attachtype($attach['ext']."\t".$attach['filetype']);
	if($attach['isimage'] < 1) {
		if($attach['isimage']) {
			$attach['url'] = $attach['remote'] ? $_G['setting']['ftp']['attachurl'] : $_G['setting']['attachurl'];
			$attach['width'] = $attach['width'] > 300 ? 300 : $attach['width'];
		}
		if($attach['pid']) {
			$attachs['used'][] = $attach;
		} else {
			$attachs['unused'][] = $attach;
		}
	} else {
		$attach['url'] = ($attach['remote'] ? $_G['setting']['ftp']['attachurl'] : $_G['setting']['attachurl']).'/forum';
		$attach['width'] = $attach['width'] > 300 ? 300 : $attach['width'];
		if($attach['pid']) {
			$imgattachs['used'][] = $attach;
		} else {
			$imgattachs['unused'][] = $attach;
		}
	}
}

function parseattachmedia($attach) {
	$attachurl = 'attach://'.$attach['aid'].'.'.$attach['ext'];
	switch(strtolower($attach['ext'])) {
		case 'mp3':
		case 'wma':
		case 'ra':
		case 'ram':
		case 'wav':
		case 'mid':
			return '[audio]'.$attachurl.'[/audio]';
		case 'wmv':
		case 'rm':
		case 'rmvb':
		case 'avi':
		case 'asf':
		case 'mpg':
		case 'mpeg':
		case 'mov':
		case 'flv':
		case 'swf':
			return '[media='.$attach['ext'].',400,300]'.$attachurl.'[/media]';
		default:
			return;
	}
}

function ftpupload($aids, $uid = 0) {
	global $_G;
	$uid = $uid ? $uid : $_G['uid'];

	if(!$aids || !$_G['setting']['ftp']['on']) {
		return;
	}
	$attachtables = $pics = array();
//	$query = DB::query("SELECT aid, tableid FROM ".DB::table('forum_attachment')." WHERE aid IN (".dimplode($aids).") AND uid='$uid'");
//	while($attach = DB::fetch($query)) {
	foreach(C::t('forum_attachment')->fetch_all($aids) as $attach) {
		if($uid != $attach['uid'] && !$_G['forum']['ismoderator']) {
			continue;
		}
		$attachtables[$attach['tableid']][] = $attach['aid'];
	}
	foreach($attachtables as $attachtable => $aids) {
//		$attachtable = 'forum_attachment_'.$attachtable;
//		$query = DB::query("SELECT aid, thumb, attachment, filename, filesize, picid FROM ".DB::table($attachtable)." WHERE aid IN (".dimplode($aids).") AND remote='0'");
//		while($attach = DB::fetch($query)) {
		$remoteaids = array();
		foreach(C::t('forum_attachment_n')->fetch_all($attachtable, $aids, 0) as $attach) {
			$attach['ext'] = fileext($attach['filename']);
			if(((!$_G['setting']['ftp']['allowedexts'] && !$_G['setting']['ftp']['disallowedexts']) || ($_G['setting']['ftp']['allowedexts'] && in_array($attach['ext'], $_G['setting']['ftp']['allowedexts'])) || ($_G['setting']['ftp']['disallowedexts'] && !in_array($attach['ext'], $_G['setting']['ftp']['disallowedexts']) && (!$_G['setting']['ftp']['allowedexts'] || $_G['setting']['ftp']['allowedexts'] && in_array($attach['ext'], $_G['setting']['ftp']['allowedexts'])) )) && (!$_G['setting']['ftp']['minsize'] || $attach['filesize'] >= $_G['setting']['ftp']['minsize'] * 1024)) {
				if(ftpcmd('upload', 'forum/'.$attach['attachment']) && (!$attach['thumb'] || ftpcmd('upload', 'forum/'.getimgthumbname($attach['attachment'])))) {
					dunlink($attach);
					$remoteaids[$attach['aid']] = $attach['aid'];
					if($attach['picid']) {
						$pics[] = $attach['picid'];
					}
				}
			}
		}

		if($remoteaids) {
//			DB::update($attachtable, array('remote' => 1), "aid IN (".dimplode($aids).")");
			C::t('forum_attachment_n')->update($attachtable, $remoteaids, array('remote' => 1));
		}
	}
	if($pics) {
		//DB::update('home_pic', array('remote' => 3), "picid IN (".dimplode($pics).")");
		C::t('home_pic')->update($pics, array('remote' => 3));
	}
}

function updateattach($modnewthreads, $tid, $pid, $attachnew, $attachupdate = array(), $uid = 0) {
	global $_G;
	$thread = C::t('forum_thread')->fetch($tid);
	$uid = $uid ? $uid : $_G['uid'];
	if($attachnew) {
		$newaids = array_keys($attachnew);
		$newattach = $newattachfile = $albumattach = array();
//		$query = DB::query("SELECT * FROM ".DB::table('forum_attachment_unused')." WHERE aid IN (".dimplode($newaids).")$uidadd");
//		while($attach = DB::fetch($query)) {
		foreach(C::t('forum_attachment_unused')->fetch_all($newaids) as $attach) {
			if($attach['uid'] != $uid && !$_G['forum']['ismoderator']) {
				continue;
			}
			$attach['uid'] = $uid;
			$newattach[$attach['aid']] = daddslashes($attach);
			if($attach['isimage']) {
				$newattachfile[$attach['aid']] = $attach['attachment'];
			}
		}
		if($_G['setting']['watermarkstatus'] && empty($_G['forum']['disablewatermark']) || !$_G['setting']['thumbdisabledmobile']) {
			require_once libfile('class/image');
			$image = new image;
		}
		if(!empty($_GET['albumaid'])) {
			array_unshift($_GET['albumaid'], '');
			$_GET['albumaid'] = array_unique($_GET['albumaid']);
			unset($_GET['albumaid'][0]);
			foreach($_GET['albumaid'] as $aid) {
				if(isset($newattach[$aid])) {
					$albumattach[$aid] = $newattach[$aid];
				}
			}
		}
		foreach($attachnew as $aid => $attach) {
			$update = array();
			$update['readperm'] = $_G['group']['allowsetattachperm'] ? $attach['readperm'] : 0;
			$update['price'] = $_G['group']['maxprice'] ? (intval($attach['price']) <= $_G['group']['maxprice'] ? intval($attach['price']) : $_G['group']['maxprice']) : 0;
			$update['tid'] = $tid;
			$update['pid'] = $pid;
			$update['uid'] = $uid;
			$update['description'] = censor(cutstr(dhtmlspecialchars($attach['description']), 100));
			//DB::update(getattachtablebytid($tid), $update, "aid='$aid'");
			C::t('forum_attachment_n')->update('tid:'.$tid, $aid, $update);
			if(!$newattach[$aid]) {
				continue;
			}
			$update = array_merge($update, $newattach[$aid]);
			if(!empty($newattachfile[$aid])) {
				if($_G['setting']['thumbstatus'] && $_G['forum']['disablethumb']) {
					$update['thumb'] = 0;
					@unlink($_G['setting']['attachdir'].'/forum/'.getimgthumbname($newattachfile[$aid]));
					if(!empty($albumattach[$aid])) {
						$albumattach[$aid]['thumb'] = 0;
					}
				} elseif(!$_G['setting']['thumbdisabledmobile']) {
					$_daid = sprintf("%09d", $aid);
					$dir1 = substr($_daid, 0, 3);
					$dir2 = substr($_daid, 3, 2);
					$dir3 = substr($_daid, 5, 2);
					$dw = 320;
					$dh = 320;
					$thumbfile = 'image/'.$dir1.'/'.$dir2.'/'.$dir3.'/'.substr($_daid, -2).'_'.$dw.'_'.$dh.'.jpg';
					$image->Thumb($_G['setting']['attachdir'].'/forum/'.$newattachfile[$aid], $thumbfile, $dw, $dh, 'fixwr');
					$dw = 720;
					$dh = 720;
					$thumbfile = 'image/'.$dir1.'/'.$dir2.'/'.$dir3.'/'.substr($_daid, -2).'_'.$dw.'_'.$dh.'.jpg';
					$image->Thumb($_G['setting']['attachdir'].'/forum/'.$newattachfile[$aid], $thumbfile, $dw, $dh, 'fixwr');
				}
				if($_G['setting']['watermarkstatus'] && empty($_G['forum']['disablewatermark'])) {
					$image->Watermark($_G['setting']['attachdir'].'/forum/'.$newattachfile[$aid], '', 'forum');
					$update['filesize'] = $image->imginfo['size'];
				}
			}
			if(!empty($_GET['albumaid']) && isset($albumattach[$aid])) {
				$newalbum = 0;
				if(!$_GET['uploadalbum']) {
					require_once libfile('function/spacecp');
					$_GET['uploadalbum'] = album_creat(array('albumname' => $_GET['newalbum']));
					$newalbum = 1;
				}
				$picdata = array(
					'albumid' => $_GET['uploadalbum'],
					'uid' => $uid,
					'username' => $_G['username'],
					'dateline' => $albumattach[$aid]['dateline'],
					'postip' => $_G['clientip'],
					'filename' => censor($albumattach[$aid]['filename']),
					'title' => censor(cutstr(dhtmlspecialchars($attach['description']), 100)),
					'type' => fileext($albumattach[$aid]['attachment']),
					'size' => $albumattach[$aid]['filesize'],
					'filepath' => $albumattach[$aid]['attachment'],
					'thumb' => $albumattach[$aid]['thumb'],
					'remote' => $albumattach[$aid]['remote'] + 2,
				);

				//$update['picid'] = DB::insert('home_pic', $picdata, 1);
				$update['picid'] = C::t('home_pic')->insert($picdata, 1);

				if($newalbum) {
					require_once libfile('function/home');
					require_once libfile('function/spacecp');
					album_update_pic($_GET['uploadalbum']);
				}
			}
			//DB::insert(getattachtablebytid($tid), $update, false, true);
			C::t('forum_attachment_n')->insert('tid:'.$tid, $update, false, true);
//			DB::update('forum_attachment', array('tid' => $tid, 'pid' => $pid, 'tableid' => getattachtableid($tid)), "aid='$aid'");
			C::t('forum_attachment')->update($aid, array('tid' => $tid, 'pid' => $pid, 'tableid' => getattachtableid($tid)));
//			DB::delete('forum_attachment_unused', "aid='$aid'");
			C::t('forum_attachment_unused')->delete($aid);
		}

		if(!empty($_GET['albumaid'])) {
			$albumdata = array(
				//'picnum' => DB::result_first("SELECT COUNT(*) FROM ".DB::table('home_pic')." WHERE albumid='$_GET[uploadalbum]'"),
				'picnum' => C::t('home_pic')->check_albumpic($_GET['uploadalbum']),
				'updatetime' => $_G['timestamp'],
			);
			//DB::update('home_album', $albumdata, "albumid='$_GET[uploadalbum]'");
			C::t('home_album')->update($_GET['uploadalbum'], $albumdata);
			require_once libfile('function/home');
			require_once libfile('function/spacecp');
			album_update_pic($_GET['uploadalbum']);
		}
		if($newattach) {
			ftpupload($newaids, $uid);
		}
	}

	if(!$modnewthreads && $newattach && $uid == $_G['uid']) {
		updatecreditbyaction('postattach', $uid, array(), '', count($newattach), 1, $_G['fid']);
	}

	if($attachupdate) {
//		$query = DB::query("SELECT pid, aid, attachment, thumb, remote FROM ".DB::table(getattachtablebytid($tid))." WHERE aid IN (".dimplode(array_keys($attachupdate)).")");
//		while($attach = DB::fetch($query)) {
		$attachs = C::t('forum_attachment_n')->fetch_all_by_id('tid:'.$tid, 'aid', array_keys($attachupdate));
		foreach($attachs as $attach) {
			if(array_key_exists($attach['aid'], $attachupdate) && $attachupdate[$attach['aid']]) {
				dunlink($attach);
			}
		}
//		$uaids = dimplode($attachupdate);
//		$query = DB::query("SELECT aid, width, filename, filesize, attachment, isimage, thumb, remote FROM ".DB::table('forum_attachment_unused')." WHERE aid IN ($uaids)$uidadd");
//		DB::query("DELETE FROM ".DB::table('forum_attachment_unused')." WHERE aid IN ($uaids)$uidadd");
		$unusedattachs = C::t('forum_attachment_unused')->fetch_all($attachupdate);
		$attachupdate = array_flip($attachupdate);
		//while($attach = DB::fetch($query)) {
		$unusedaids = array();
		foreach($unusedattachs as $attach) {
			if($attach['uid'] != $uid && !$_G['forum']['ismoderator']) {
				continue;
			}
			$unusedaids[] = $attach['aid'];
			$update = $attach;
			$update['dateline'] = TIMESTAMP;
			$update['remote'] = 0;
			unset($update['aid']);
			if($attach['isimage'] && $_G['setting']['watermarkstatus'] && empty($_G['forum']['disablewatermark'])) {
				$image->Watermark($_G['setting']['attachdir'].'/forum/'.$attach['attachment'], '', 'forum');
				$update['filesize'] = $image->imginfo['size'];
			}
			//DB::update(getattachtablebytid($tid), $update, "aid='".$attachupdate[$attach['aid']]."'");
			C::t('forum_attachment_n')->update('tid:'.$tid, $attachupdate[$attach['aid']], $update);
			@unlink($_G['setting']['attachdir'].'image/'.$attach['aid'].'_100_100.jpg');
//			DB::delete('forum_attachment_exif', array('aid' => $attachupdate[$attach['aid']]));
			C::t('forum_attachment_exif')->delete($attachupdate[$attach['aid']]);
//			DB::update('forum_attachment_exif', array('aid' => $attachupdate[$attach['aid']]), array('aid' => $attach['aid']));
			C::t('forum_attachment_exif')->update($attach['aid'], array('aid' => $attachupdate[$attach['aid']]));
			ftpupload(array($attachupdate[$attach['aid']]), $uid);
		}
		if($unusedaids) {
			C::t('forum_attachment_unused')->delete($unusedaids);
		}
	}

//	$attachcount = DB::result_first("SELECT COUNT(*) FROM ".DB::table(getattachtablebytid($tid))." WHERE tid='$tid'".($pid > 0 ? " AND pid='$pid'" : ''));
//	$attachment = $attachcount ? (DB::result_first("SELECT COUNT(*) FROM ".DB::table(getattachtablebytid($tid))." WHERE tid='$tid'".($pid > 0 ? " AND pid='$pid'" : '')." AND isimage != 0") ? 2 : 1) : 0;
	$attachcount = C::t('forum_attachment_n')->count_by_id('tid:'.$tid, $pid ? 'pid' : 'tid', $pid ? $pid : $tid);
	$attachment = 0;
	if($attachcount) {
		if(C::t('forum_attachment_n')->count_image_by_id('tid:'.$tid, $pid ? 'pid' : 'tid', $pid ? $pid : $tid)) {
			$attachment = 2;
		} else {
			$attachment = 1;
		}
	} else {
		$attachment = 0;
	}
//	DB::query("UPDATE ".DB::table('forum_thread')." SET attachment='$attachment' WHERE tid='$tid'", 'UNBUFFERED');
	C::t('forum_thread')->update($tid, array('attachment'=>$attachment));
	C::t('forum_post')->update('tid:'.$tid, $pid, array('attachment' => $attachment), true);

	if(!$attachment) {
		//DB::delete('forum_threadimage', "tid='$tid'");
		C::t('forum_threadimage')->delete_by_tid($tid);
	}
	//$posttable = getposttablebytid($tid);
	//DB::query("UPDATE ".DB::table($posttable)." SET attachment='$attachment' WHERE pid='$pid'", 'UNBUFFERED');
	$_G['forum_attachexist'] = $attachment;
}

/*debug
* 检查灌水
* @return 是否灌水
*/
function checkflood() {
	global $_G;
	if(!$_G['group']['disablepostctrl'] && $_G['uid']) {
		//新的灌水防御机制，采用进程锁
		if($_G['setting']['floodctrl'] && discuz_process::islocked("post_lock_".$_G['uid'], $_G['setting']['floodctrl'])) {
			return true;
		}
		return false;

		//老的灌水预防
		//$isflood = $_G['setting']['floodctrl'] && (TIMESTAMP - $_G['setting']['floodctrl'] <= getuserprofile('lastpost'));

		//if(empty($isflood)) {
		//	return FALSE;
		//} else {
		//	return TRUE;
		//}
	}
	return FALSE;
}

function checkmaxperhour($type) {
	global $_G;
	$morenumperhour = false;
	if(!$_G['group']['disablepostctrl'] && $_G['uid']) {
		if($_G['group']['max'.($type == 'pid' ? 'posts' : 'threads').'perhour']) {
			//$timestamp = $_G['timestamp']-3600;
			//$userposts = DB::result_first('SELECT COUNT(*) FROM '.DB::table('common_member_action_log')." WHERE dateline>$timestamp AND (`action`='".getuseraction('tid')."' OR `action`='".getuseraction('pid')."') AND uid='$_G[uid]'");
			$usernum = C::t('common_member_action_log')->count_per_hour($_G['uid'], $type);
			$var = $type === 'tid' ? 'maxthreadsperhour' : 'maxpostsperhour';
			$isflood = $usernum && ($usernum >= $_G['group'][$var]);
			if($isflood) {
				$morenumperhour = true;
			}
		}
	}
	return $morenumperhour;
}

/*debug
* 检查post内容
* @return 返回内容限制消息
*/
function checkpost($subject, $message, $special = 0) {
	global $_G;
	if(dstrlen($subject) > 80) {
		//note showmessage('post_subject_toolong');
		return 'post_subject_toolong';
	}
	if(!$_G['group']['disablepostctrl'] && !$special) {
		if($_G['setting']['maxpostsize'] && strlen($message) > $_G['setting']['maxpostsize']) {
			//note showmessage('post_message_toolong');
			return 'post_message_toolong';
		} elseif($_G['setting']['minpostsize']) {
			$minpostsize = !IN_MOBILE || !$_G['setting']['minpostsize_mobile'] ? $_G['setting']['minpostsize'] : $_G['setting']['minpostsize_mobile'];
			if(strlen(preg_replace("/\[quote\].+?\[\/quote\]/is", '', $message)) < $minpostsize || strlen(preg_replace("/\[postbg\].+?\[\/postbg\]/is", '', $message)) < $minpostsize) {
				//note showmessage('post_message_tooshort');
				return 'post_message_tooshort';
			}
		}
	}
	return FALSE;
}

/*debug
* 检查bbcode
* @param $message 消息
* @param bbcodeoff 是否关闭
* @return 返回bbcode状态
*/
function checkbbcodes($message, $bbcodeoff) {
	return !$bbcodeoff && (!strpos($message, '[/') && !strpos($message, '[hr]')) ? -1 : $bbcodeoff;
}

/*debug
* 检查smilies
* @param $message 消息
* @param $smileyoff 是否关闭
* @return 返回smileyoff状态
*/
function checksmilies($message, $smileyoff) {
	global $_G;

	if($smileyoff) {
		return 1;
	} else {
		if(!empty($_G['cache']['smileycodes']) && is_array($_G['cache']['smileycodes'])) {
			//$message = dstripslashes($message);
			foreach($_G['cache']['smileycodes'] as $id => $code) {
				if(strpos($message, $code) !== FALSE) {
					return 0;
				}
			}
		}
		return -1;
	}
}

/*debug
* 更新发帖积分
* @param $operator 操作
* @param $uidarry 影响到的用户列表
* @param $creditsarray 积分列表
*/
function updatepostcredits($operator, $uidarray, $action, $fid = 0) {
	global $_G;
	$val = $operator == '+' ? 1 : -1;
	$extsql = array();
	if(empty($uidarray)) {
		return false;
	}
	$uidarray = (array)$uidarray;
	//整理倍率
	$uidarr = array();
	foreach($uidarray as $uid) {
		$uidarr[$uid] = !isset($uidarr[$uid]) ? 1 : $uidarr[$uid]+1;
	}
	foreach($uidarr as $uid => $coef) {
		$opnum = $val*$coef;
		if($action == 'reply') {
			$extsql = array('posts' => $opnum);
		} elseif($action == 'post') {
			$extsql = array('threads' => $opnum, 'posts' => $opnum);
		}
		if($uid == $_G['uid']) {
			updatecreditbyaction($action, $uid, $extsql, '', $opnum, 1, $fid);
		} elseif(empty($uid)) {
			continue;
		} else {
			batchupdatecredit($action, $uid, $extsql, $opnum, $fid);
		}
	}
	if($operator == '+' && ($action == 'reply' || $action == 'post')) {
		//$uids = implode(',', $uidarray);
		//DB::query("UPDATE ".DB::table('common_member_status')." SET lastpost='".TIMESTAMP."' WHERE uid IN ('$uids')", 'UNBUFFERED');
		C::t('common_member_status')->update(array_keys($uidarr), array('lastpost' => TIMESTAMP), 'UNBUFFERED');
	}
}

/*debug
* 更新附件积分
* @param $operator 操作
* @param $uidarry 影响到的用户列表 array(uid => 附件数)
* @param $creditsarray 积分列表
*/
function updateattachcredits($operator, $uidarray) {
	global $_G;
	foreach($uidarray as $uid => $attachs) {
		updatecreditbyaction('postattach', $uid, array(), '', $operator == '-' ? -$attachs : $attachs, 1, $_G['fid']);
	}
}

/*debug
* 更新论坛统计数字
* @param $fid
*/
function updateforumcount($fid) {

	//TODO 以下代码中forum_forum是可以不参于查询的
// 	extract(DB::fetch_first("SELECT COUNT(*) AS threadcount, SUM(t.replies)+COUNT(*) AS replycount
// 		FROM ".DB::table('forum_thread')." t, ".DB::table('forum_forum')." f
// 		WHERE f.fid='$fid' AND t.fid=f.fid AND t.displayorder>='0'"));
	//会释放出$posts、$threads两个变量
	extract(C::t('forum_thread')->count_posts_by_fid($fid));

//	$thread = DB::fetch_first("SELECT tid, subject, author, lastpost, lastposter, closed FROM ".DB::table('forum_thread')."
//		WHERE fid='$fid' AND displayorder='0' ORDER BY lastpost DESC LIMIT 1");
	$thread = C::t('forum_thread')->fetch_by_fid_displayorder($fid, 0, '=');

	$thread['subject'] = addslashes($thread['subject']);
	$thread['lastposter'] = $thread['author'] ? addslashes($thread['lastposter']) : lang('forum/misc', 'anonymous');
	$tid = $thread['closed'] > 1 ? $thread['closed'] : $thread['tid'];
	//DB::query("UPDATE ".DB::table('forum_forum')." SET posts='$replycount', threads='$threadcount', lastpost='$tid\t$thread[subject]\t$thread[lastpost]\t$thread[lastposter]' WHERE fid='$fid'", 'UNBUFFERED');
	$setarr = array('posts' => $posts, 'threads' => $threads, 'lastpost' => "$tid\t$thread[subject]\t$thread[lastpost]\t$thread[lastposter]");
	C::t('forum_forum')->update($fid, $setarr);
}

/*debug
* 更新主题统计数字
* @param $tid 操作者
* @param $updateattach 是否更新附件
*/
function updatethreadcount($tid, $updateattach = 0) {
	//$posttable = getposttablebytid($tid);
	//$replycount = DB::result_first("SELECT COUNT(*) FROM ".DB::table($posttable)." WHERE tid='$tid' AND invisible='0'") - 1;
	$replycount = C::t('forum_post')->count_visiblepost_by_tid($tid) - 1;
	//$lastpost = DB::fetch_first("SELECT author, anonymous, dateline FROM ".DB::table($posttable)." WHERE tid='$tid' AND invisible='0' ORDER BY dateline DESC LIMIT 1");
	$lastpost = C::t('forum_post')->fetch_visiblepost_by_tid('tid:'.$tid, $tid, 0, 1);

	$lastpost['author'] = $lastpost['anonymous'] ? lang('forum/misc', 'anonymous') : addslashes($lastpost['author']);
	$lastpost['dateline'] = !empty($lastpost['dateline']) ? $lastpost['dateline'] : TIMESTAMP;

	$data = array('replies'=>$replycount, 'lastposter'=>$lastpost['author'], 'lastpost'=>$lastpost['dateline']);
	if($updateattach) {
		//$attach = DB::result_first("SELECT attachment FROM ".DB::table($posttable)." WHERE tid='$tid' AND invisible='0' AND attachment>0 LIMIT 1");
		$attach = C::t('forum_post')->fetch_attachment_by_tid($tid);
//		$attachadd = ', attachment=\''.($attach ? 1 : 0).'\'';
		$data['attachment'] = $attach ? 1 : 0;
	}
//	DB::query("UPDATE ".DB::table('forum_thread')." SET replies='$replycount', lastposter='$lastpost[author]', lastpost='$lastpost[dateline]' $attachadd WHERE tid='$tid'", 'UNBUFFERED');
	C::t('forum_thread')->update($tid, $data);
}

/*debug
* 更新管理日志
* @param $tids 主题数组
* @param $action 动作
* @param $expiration 限制
* @param $iscron 是否计划任务
* @param $reason 操作理由
*/
function updatemodlog($tids, $action, $expiration = 0, $iscron = 0, $reason = '', $stamp = 0) {
	global $_G;

	$uid = empty($iscron) ? $_G['uid'] : 0;
	$username = empty($iscron) ? $_G['member']['username'] : 0;
	$expiration = empty($expiration) ? 0 : intval($expiration);

	$data = $comma = '';
	$stampadd = $stampaddvalue = '';
	if($stamp) {
		$stampadd = ', stamp';
		$stampaddvalue = ", '$stamp'";
	}
	foreach(explode(',', str_replace(array('\'', ' '), array('', ''), $tids)) as $tid) {
		if($tid) {
// 			$data .= "{$comma} ('$tid', '$uid', '$username', '$_G[timestamp]', '$action', '$expiration', '1', '$reason'$stampaddvalue)";
// 			$comma = ',';

			$data = array(
					'tid' => $tid,
					'uid' => $uid,
					'username' => $username,
					'dateline' => $_G['timestamp'],
					'action' => $action,
					'expiration' => $expiration,
					'status' => 1,
					'reason' => $reason
				);
			if($stamp) {
				$data['stamp'] = $stamp;
			}
			C::t('forum_threadmod')->insert($data);
		}
	}

// 	!empty($data) && DB::query("INSERT INTO ".DB::table('forum_threadmod')." (tid, uid, username, dateline, action, expiration, status, reason$stampadd) VALUES $data", 'UNBUFFERED');

}

/*debug
* 判断用户是否使用了opera浏览器
* @return 使用了opera返回信息,否则返回假
*/
function isopera() {
	$useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
	if(strpos($useragent, 'opera') !== false) {
		preg_match('/opera(\/| )([0-9\.]+)/', $useragent, $regs);
		return $regs[2];
	}
	return FALSE;
}

/**
 * Module: HTML_CACHE
 * 清除主题缓存，使之重新生成
 *
 * @param int $tids 可以为多个，用,为开。
 * @return TRUE/FALSE
 */
function deletethreadcaches($tids) {
	global $_G;
	if(!$_G['setting']['cachethreadon']) {
		return FALSE;
	}
	require_once libfile('function/forumlist');
	if(!empty($tids)) {
		foreach(explode(',', $tids) as $tid) {
			$fileinfo = getcacheinfo($tid);
			@unlink($fileinfo['filename']);
		}
	}
	return TRUE;
}


/**
* 上传文件的函数
* @param $file - 要上传的文件
* @return 文件是否为上传的文件。
*/
function disuploadedfile($file) {
	return function_exists('is_uploaded_file') && (is_uploaded_file($file) || is_uploaded_file(str_replace('\\\\', '\\', $file)));
}

function postfeed($feed) {
	global $_G;
	if($feed) {
		require_once libfile('function/feed');
		feed_add($feed['icon'], $feed['title_template'], $feed['title_data'], $feed['body_template'], $feed['body_data'], '', $feed['images'], $feed['image_links'], '', '', '', 0, $feed['id'], $feed['idtype']);
	}
}

function messagesafeclear($message) {
	if(strpos($message, '[/password]') !== FALSE) {
		$message = '';
	}
	if(strpos($message, '[/postbg]') !== FALSE) {
		$message = preg_replace("/\s?\[postbg\]\s*([^\[\<\r\n;'\"\?\(\)]+?)\s*\[\/postbg\]\s?/is", '', $message);
	}
	if(strpos($message, '[/begin]') !== FALSE) {
		$message = preg_replace("/\[begin(=\s*([^\[\<\r\n]*?)\s*,(\d*),(\d*),(\d*),(\d*))?\]\s*([^\[\<\r\n]+?)\s*\[\/begin\]/is", '', $message);
	}
	if(strpos($message, '[page]') !== FALSE) {
		$message = preg_replace("/\s?\[page\]\s?/is", '', $message);
	}
	if(strpos($message, '[/index]') !== FALSE) {
		$message = preg_replace("/\s?\[index\](.+?)\[\/index\]\s?/is", '', $message);
	}
	if(strpos($message, '[/begin]') !== FALSE) {
		$message = preg_replace("/\[begin(=\s*([^\[\<\r\n]*?)\s*,(\d*),(\d*),(\d*),(\d*))?\]\s*([^\[\<\r\n]+?)\s*\[\/begin\]/is", '', $message);
	}
	if(strpos($message, '[/groupid]') !== FALSE) {
		$message = preg_replace("/\[groupid=\d+\].*\[\/groupid\]/i", '', $message);
	}
	$language = lang('forum/misc');
	$message = preg_replace(array($language['post_edithtml_regexp'],$language['post_editnobbcode_regexp'],$language['post_edit_regexp']), '', $message);
	return $message;
}

function messagecutstr($str, $length = 0, $dot = ' ...') {
	global $_G;
	$str = messagesafeclear($str);
	$sppos = strpos($str, chr(0).chr(0).chr(0));
	if($sppos !== false) {
		$str = substr($str, 0, $sppos);
	}
	$language = lang('forum/misc');
	loadcache(array('bbcodes_display', 'bbcodes', 'smileycodes', 'smilies', 'smileytypes', 'domainwhitelist'));
	$bbcodes = 'b|i|u|p|color|size|font|align|list|indent|float';
	$bbcodesclear = 'email|code|free|table|tr|td|img|swf|flash|attach|media|audio|groupid|payto'.($_G['cache']['bbcodes_display'][$_G['groupid']] ? '|'.implode('|', array_keys($_G['cache']['bbcodes_display'][$_G['groupid']])) : '');
	$str = strip_tags(preg_replace(array(
			"/\[hide=?\d*\](.*?)\[\/hide\]/is",
			"/\[quote](.*?)\[\/quote]/si",
			$language['post_edit_regexp'],
			"/\[url=?.*?\](.+?)\[\/url\]/si",
			"/\[($bbcodesclear)=?.*?\].+?\[\/\\1\]/si",
			"/\[($bbcodes)=?.*?\]/i",
			"/\[\/($bbcodes)\]/i",
			"/\\\\u/i"
		), array(
			"[b]$language[post_hidden][/b]",
			'',
			'',
			'\\1',
			'',
			'',
			'',
		        '%u'
		), $str));
	if($length) {
		$str = cutstr($str, $length, $dot);
	}
	$str = preg_replace($_G['cache']['smilies']['searcharray'], '', $str);
	if($_G['setting']['plugins']['func'][HOOKTYPE]['discuzcode']) {
		$_G['discuzcodemessage'] = & $str;
		$param = func_get_args();
		hookscript('discuzcode', 'global', 'funcs', array('param' => $param, 'caller' => 'messagecutstr'), 'discuzcode');
	}
	return trim($str);
}

/**
 * 保存帖子显示位置信息
 *
 * @param int $tid
 * @param int $pid
 * @param bollean $returnpostion
 * @return int
function savepostposition($tid, $pid) {
	//$res = DB::query("INSERT INTO ".DB::table('forum_postposition')." SET tid='$tid', pid='$pid'");
	return C::t('forum_postposition')->insert(array('tid'=>$tid, 'pid'=>$pid));
// 	if(!$returnposition) {
// 		return $res;
// 	} else {
// 		return DB::insert_id();
// 	}
}
 */

/**
 * 设置帖子封面
 *
 * @param int $pid
 * @param int $tid
 * @param int $aid 用指定图片附件设置封面
 * @param int $countimg 代表只更新图片数量不更新封面
 * @param int $imgurl 指定网络图片的地址，$aid优先
 */
function setthreadcover($pid, $tid = 0, $aid = 0, $countimg = 0, $imgurl = '') {
	global $_G;
	$cover = 0;
	if(empty($_G['uid']) || !intval($_G['setting']['forumpicstyle']['thumbheight']) || !intval($_G['setting']['forumpicstyle']['thumbwidth'])) {
		return false;
	}

	if(($pid || $aid) && empty($countimg)) {
		if(empty($imgurl)) {
			if($aid) {
//				$attachtable = getattachtablebyaid($aid);
//				$wheresql = "aid='$aid' AND isimage IN ('1', '-1')";
				$attachtable = 'aid:'.$aid;
				$attach = C::t('forum_attachment_n')->fetch('aid:'.$aid, $aid, array(1, -1));
			} else {
//				$attachtable = getattachtablebypid($pid);
//				$wheresql = "pid='$pid' AND isimage IN ('1', '-1') ORDER BY width DESC LIMIT 1";
				$attachtable = 'pid:'.$pid;
				$attach = C::t('forum_attachment_n')->fetch_max_image('pid:'.$pid, 'pid', $pid);
			}
//			$query = DB::query("SELECT * FROM ".DB::table($attachtable)." WHERE $wheresql");
			if(!$attach) {
				return false;
			}
			if(empty($_G['forum']['ismoderator']) && $_G['uid'] != $attach['uid']) {
				return false;
			}
			$pid = empty($pid) ? $attach['pid'] : $pid;
			$tid = empty($tid) ? $attach['tid'] : $tid;
			$picsource = ($attach['remote'] ? $_G['setting']['ftp']['attachurl'] : $_G['setting']['attachurl']).'forum/'.$attach['attachment'];
		} else {
			//$attachtable = getattachtablebypid($pid);
			$attachtable = 'pid:'.$pid;
			$picsource = $imgurl;
		}

		//生成缩略图
		$basedir = !$_G['setting']['attachdir'] ? (DISCUZ_ROOT.'./data/attachment/') : $_G['setting']['attachdir'];
		$coverdir = 'threadcover/'.substr(md5($tid), 0, 2).'/'.substr(md5($tid), 2, 2).'/';
		dmkdir($basedir.'./forum/'.$coverdir);

		require_once libfile('class/image');
		$image = new image();
		if($image->Thumb($picsource, 'forum/'.$coverdir.$tid.'.jpg', $_G['setting']['forumpicstyle']['thumbwidth'], $_G['setting']['forumpicstyle']['thumbheight'], 2)) {
			$remote = '';
			//远程上传
			if(getglobal('setting/ftp/on')) {
				if(ftpcmd('upload', 'forum/'.$coverdir.$tid.'.jpg')) {
					$remote = '-';
				}
			}
			//$cover = DB::result_first("SELECT COUNT(*) FROM ".DB::table($attachtable)." WHERE pid='$pid' AND isimage IN ('1', '-1')");
			$cover = C::t('forum_attachment_n')->count_image_by_id($attachtable, 'pid', $pid);
			if($imgurl && empty($cover)) {
				$cover = 1;
			}
			$cover = $remote.$cover;
		} else {
			return false;
		}
	}
	if($countimg) {
		if(empty($cover)) {
//			$oldcover = DB::result_first("SELECT cover FROM ".DB::table('forum_thread')." WHERE tid='$tid'");
			$thread = C::t('forum_thread')->fetch($tid);
			$oldcover = $thread['cover'];

			//$cover = DB::result_first("SELECT COUNT(*) FROM ".DB::table(getattachtablebytid($tid))." WHERE pid='$pid' AND isimage IN ('1', '-1')");
			$cover = C::t('forum_attachment_n')->count_image_by_id('tid:'.$tid, 'pid', $pid);
			if($cover) {
				$cover = $oldcover < 0 ? '-'.$cover : $cover;
			}
		}
	}
	if($cover) {
		//DB::update('forum_thread', array('cover' => $cover), array('tid'=>$tid));
		C::t('forum_thread')->update($tid, array('cover' => $cover));
		return true;
	}
}

?>