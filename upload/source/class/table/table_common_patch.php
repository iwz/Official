<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_patch.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_patch extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_patch';
		$this->_pk    = 'serial';

		parent::__construct();
	}

	/**
	 * 返回所有安全补丁
	 * @return array
	 */
	public function fetch_all() {
		return DB::fetch_all("SELECT * FROM ".DB::table($this->_table));
	}

	/**
	 * 返回最大漏洞编号
	 * @return string
	 */
	public function fetch_max_serial() {
		return DB::result_first("SELECT serial FROM ".DB::table($this->_table)." ORDER BY serial DESC LIMIT 1");
	}

	/**
	 * 通过漏洞编号条件更新补丁状态
	 * @param int $status 更新的状态
	 * @param string $serial 编号条件
	 * @param string $condition 链接符号 = or <= ...
	 * @return bool
	 */
	public function update_status_by_serial($status, $serial, $condition = '') {
		return DB::query("UPDATE ".DB::table($this->_table)." SET ".DB::field('status', $status)." WHERE ".DB::field('serial', $serial, $condition));
	}

	/**
	 * 返回需要修复的漏洞补丁
	 * @param string|array $serials 指定的漏洞编号
	 * @return array
	 */
	public function fetch_needfix_patch($serials) {
		return DB::fetch_all("SELECT * FROM ".DB::table($this->_table)." WHERE ".DB::field('serial', $serials)." AND status<=0");
	}

	/**
	 * 通过状态获取安全补丁
	 * @param int $status 状态
	 * @return array
	 */
	public function fetch_patch_by_status($status) {
		return DB::fetch_all("SELECT * FROM ".DB::table($this->_table)." WHERE ".DB::field('status', $status));
	}
}

?>