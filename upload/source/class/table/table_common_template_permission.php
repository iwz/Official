<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_template_permission.php 27830 2012-02-15 07:39:23Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_template_permission extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_template_permission';
		$this->_pk    = '';

		parent::__construct();
	}

	/**
	 * 依据模块ID， 返回一组数据
	 * @param array $bid 主键值的数组
	 * @return array
	 */
	public function fetch_all_by_targettplname($targettplname) {
		return DB::fetch_all('SELECT * FROM %t WHERE targettplname=%s ORDER BY inheritedtplname', array($this->_table, $targettplname), 'uid');
	}

	/**
	 * 获取指定会员的数据
	 * @param array $uids 指定会员
	 * @param bool $flag 是否是承继的权限，true为所有，false为非承继权限
	 * @param string $sort 排序
	 * @param int $start 开始条数
	 * @param int $limit 数据量
	 * @return array
	 */
	public function fetch_all_by_uid($uids, $flag = true, $sort = 'ASC', $start = 0, $limit = 0) {
		$wherearr = array();
		$sort = $sort === 'ASC' ? 'ASC' : 'DESC';
		if(($uids = dintval($uids, true))) {
			$wherearr[] = DB::field('uid', $uids);
		}
		if(!$flag) {
			$wherearr[] = 'inheritedtplname = \'\'';
		}
		$where = $wherearr ? ' WHERE '.implode(' AND ', $wherearr) : '';
		return DB::fetch_all('SELECT * FROM '.DB::table($this->_table).$where.' ORDER BY uid '.$sort.', inheritedtplname'.DB::limit($start, $limit), NULL, 'targettplname');
	}

	/**
	 * 权限统计数据
	 * @param array $uids 指定会员
	 * @param bool $flag 是否是承继的权限，true为所有，false为非承继权限
	 */
	public function count_by_uids($uids, $flag) {
		$wherearr = array();
		if(($uids = dintval($uids, true))) {
			$wherearr[] = DB::field('uid', $uids);
		}
		if(!$flag) {
			$wherearr[] = 'inheritedtplname = \'\'';
		}
		$where = $wherearr ? ' WHERE '.implode(' AND ', $wherearr) : '';
		return DB::result_first('SELECT COUNT(*) FROM '.DB::table($this->_table).$where);
	}

	/**
	 * 删除权限列表数据
	 * @param string|array $targettplname 频道ID
	 * @param int|array $uids 会员ID
	 * @param string|bool $inheritedtplname 页面标识,有字符串值表示页面标识,''表示所有非继承,TRUE所有继承权限,FALSE表示不限
	 * @return type
	 */
	public function delete_by_targettplname_uid_inheritedtplname($targettplname = false, $uids = false, $inheritedtplname = false) {
		$wherearr = array();
		if($targettplname) {
			$wherearr[] = DB::field('targettplname', $targettplname);
		}
		if(($uids = dintval($uids, true))) {
			$wherearr[] = DB::field('uid', $uids);
		}
		if($inheritedtplname === true) {
			$wherearr[] = "inheritedtplname!=''";
		} elseif($inheritedtplname !== false && is_string($inheritedtplname)) {
			$wherearr[] = DB::field('inheritedtplname', $inheritedtplname);
		}
		return $wherearr ? DB::delete($this->_table, implode(' AND ', $wherearr)) : false;
	}


	/**
	 * 批量添加多人和多页面的权限
	 * @param array $users 用户权限数组，一个用户一条记录
	 * @param array $templates 页面名称
	 * @param string $uptplname 继承上级页面的名称，为空时则$templates数组的第一个值为其它值的上级页面
	 */
	public function insert_batch($users, $templates, $uptplname = '') {
		$blockperms = array();
		if(!empty($users) && !empty($templates)){
			if(!is_array($templates)) {
				$templates = array($templates);
			}
			foreach($users as $user) {
				$inheritedtplname = $uptplname ? $uptplname : '';
				//数组的第一个值是手工添加的
				foreach ($templates as $tpl) {
					if($tpl) {
						$blockperms[] = "('$tpl','$user[uid]','$user[allowmanage]','$user[allowrecommend]','$user[needverify]','$inheritedtplname')";
						$inheritedtplname = empty($inheritedtplname) ? $tpl : $inheritedtplname;
					}
				}
			}
			//将用户和所有模块关联批量添加到模块权限表中，只添加替换继承的数据
			if($blockperms) {
				DB::query('REPLACE INTO '.DB::table($this->_table).' (targettplname,uid,allowmanage,allowrecommend,needverify,inheritedtplname) VALUES '.implode(',', $blockperms));
			}
		}
	}
}

?>