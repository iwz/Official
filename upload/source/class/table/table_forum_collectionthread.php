<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_forum_collectionthread.php 34219 2013-11-14 08:09:32Z jeffjzhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_collectionthread extends discuz_table 
{
	public function __construct() {

		$this->_table = 'forum_collectionthread';
		$this->_pk    = '';

		parent::__construct();
	}
	
	/**
	 * 读取指定淘帖专辑的数据
	 * @param $ctid 专辑ID
	 * @param $distinct 是否要唯一的tid
	 * @return array 淘帖数据数组
	 */
	public function fetch_all_by_ctid($ctid, $start = 0, $limit = 0, $distinct = 0) {
		if(!$ctid) {
			return null;
		}
		if($distinct == 1) {
			$sql = " GROUP BY tid";
		}
		return DB::fetch_all('SELECT * FROM %t WHERE '.DB::field('ctid', $ctid).$sql.' ORDER BY dateline DESC '.DB::limit($start, $limit), array($this->_table), 'tid');
	}
	
	public function fetch_by_ctid_dateline($ctid) {
		$data = $this->fetch_all_by_ctid($ctid, 0, 1);
		return $data ? current($data) : null;
	}
	
	/**
	 * 读取指定主题的专辑数据
	 * @param array $tids 主题ID
	 * @return array 淘帖数据数组
	 */
	public function fetch_all_by_tids($tids) {
		if(!$tids) {
			return null;
		}
		return DB::fetch_all('SELECT * FROM %t WHERE '.DB::field('tid', $tids), array($this->_table), 'ctid');
	}
	
	/**
	 * 读取单条淘帖的数据
	 * @param int $ctid 专辑ID
	 * @param int $tid 主题ID
	 * @return array 淘帖数据数组
	 */
	public function fetch_by_ctid_tid($ctid, $tid) {
		return DB::fetch_first('SELECT * FROM %t WHERE ctid=%d AND tid=%d', array($this->_table, $ctid, $tid));
	}
	
	/**
	 * 读取多条淘帖的数据
	 * @param int $ctid 专辑ID
	 * @param int $tids 主题IDs
	 * @return array 淘帖数据数组
	 */
	public function fetch_all_by_ctid_tid($ctid, $tids) {
		if(!$ctid || !$tids) {
			return null;
		}
		return DB::fetch_all('SELECT * FROM %t WHERE ctid=%d AND tid IN(%n)', array($this->_table, $ctid, $tids), 'tid');
	}
	
	/**
	 * 删除专辑淘帖的数据
	 * @param int $ctid 专辑ID
	 */
	public function delete_by_ctid($ctid) {
		if(!$ctid) {
			return false;
		}
		return DB::delete($this->_table, DB::field('ctid', $ctid));
	}
	
	/**
	 * 删除专辑中指定淘帖的数据
	 * @param int $ctid 专辑ID
	 * @param $tid 主题ID (可同时多条)
	 */
	public function delete_by_ctid_tid($ctid, $tid) {
		if(!$ctid && !$tid) {
			return false;
		}
		
		$condition = array();
		
		if($ctid) {
			$condition[] = DB::field('ctid', $ctid);
		}
		
		if($tid) {
			$condition[] = DB::field('tid', $tid);
		}
		
		return DB::delete($this->_table, implode(' AND ', $condition), 0, false);
	}
}

?>
