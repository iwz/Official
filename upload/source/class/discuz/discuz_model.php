<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

abstract class discuz_model extends discuz_base
{

	/**
	 * 结果数据
	 * @var array
	 */
	public $data;

	/**
	 * 扩展方法
	 * @var array
	 */
	public $methods = array();

	/**
	 * showmessage方法句柄
	 * @var array
	 */
	public $showmessage = 'showmessage';

	/**
	 * @var discuz_application
	 */
	public $app;

	/**
	 * 用户数据
	 * @var array
	 */
	public $member;

	/**
	 * 用户组
	 * @var array
	 */
	public $group;

	/**
	 * setting数据
	 * @var array
	 */
	public $setting;

	/**
	 * 方法的参数集合
	 * @var array
	 */
	public $param = array();

	/**
	 * 构造函数
	 */
	public function __construct() {
		$this->app = C::app();
		$this->setting = &$this->app->var['setting'];
		$this->group = &$this->app->var['group'];
		$this->member = &$this->app->var['member'];
		parent::__construct();
	}

	/**
	 * 获取当前的 APP config的某个键值
	 * <code>
	 * EXP: $this->config('memory');
	 * RET: array('prefix' => ...)
	 *
	 * EXP: $this->config('cookie/cookiepre');
	 * RET: (string) adfs_
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function config($name) {
		return getglobal('config/'.$name);
	}

	/**
	 * 获取当前 APP 全局setting 的某个键值
	 *
	 * <code>
	 * EXP: $this->setting('bbname');
	 * RET: (string) Discuz Board
	 *
	 * EXP: $this->setting('ec_credit/rank');
	 * RET: array(1=>4, 2=>11 ...)
	 *
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function setting($name = null, $val = null) {
		if(isset($val)) {
			return $this->setvar($this->setting, $name, $val);
		}
		return $this->getvar($this->setting, $name);
	}

	/**
	 * table loader 获取某个 table 对象， 作用同 C::t($name)
	 * @param type $name table名
	 * @return discuz_table
	 */
	public function table($name) {
		return C::t($name);
	}

	/**
	 * 获取/设置Setting, 获取动自加载某个系统缓存，同时返回键值
	 * <code>
	 * EXP: $this->cache('grouptype');
	 * RET: arrary('first'=> ...)
	 *
	 * EXP: $this->cache('onlinelist/legend');
	 * RET: string(....)
	 *
	 * </code>
	 * @param type $name 缓存名称， 支持 缓存名称/缓存键值 这种形式
	 * @return mixed
	 */
	public function cache($name, $val = null) {
		if(isset($val)) {
			savecache($name, $val);
			$this->app->var['cache'][$name] = $val;
			return true;
		} else {
			if (!isset($this->app->var['cache'][$name])) {
				loadcache($name);
			}
			if($this->app->var['cache'][$name] === null) {
				return null;
			} else {
				return getglobal('cache/'.$name);
			}
		}
	}

	/**
	 * 获取/设置当前 用户 的某个键值
	 *
	 * <code>
	 * EXP: $this->member('uid');
	 * RET: (string) 1
	 *
	 * EXP: $this->member('username');
	 * RET: admin
	 *
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function member($name = null, $val = null){
		if(isset($val)) {
			return $this->setvar($this->member, $name, $val);
		} else {
			return $this->getvar($this->member, $name);
		}
	}

	/**
	 * 获取/设置当前 用户组 的某个键值
	 *
	 * <code>
	 * EXP: $this->group('id');
	 * RET: (string) 1
	 *
	 * EXP: $this->group('groupname');
	 * RET: 管理员
	 *
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function group($name = null, $val = null){
		if(isset($val)) {
			return $this->setvar($this->group, $name, $val);
		} else {
			return $this->getvar($this->group, $name);
		}
	}

	/**
	 * 获取/设置当前 参数数据 的某个值
	 *
	 * <code>
	 * EXP: $this->param('key1');
	 * RET: (Mixed) val1
	 *
	 * EXP: $this->param('key1', 2); //设置key1的值为2
	 * RET: true
	 *
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function param($name = null, $val = null){
		if(isset($val)) {
			return $this->setvar($this->param, $name, $val);
		}
		return $this->getvar($this->param, $name);
	}

	/**
	* 设置某变量的值
	* @param <string> $var 变量名
	* @param <string> $key 键
	* @param <string> $value 值
	* @return true
	*
	* @example
	* $this->setvar($this->member,, 'uid', 1); // $this->member['uid'] = 1;
	* setvar($this->member,, 'config/test/abc', 2); //$this->member['config']['test']['abc'] = 2;
	*
	*/
	public function setvar(&$var, $key, $value) {
		if(isset($key)) {
			$key = explode('/', $key);
			$p = &$var;
			foreach ($key as $k) {
				if(!isset($p[$k]) || !is_array($p[$k])) {
					$p[$k] = array();
				}
				$p = &$p[$k];
			}
			$p = $value;
		} else {
			$var = $value;
		}
		return true;
	}

	/**
	* 获取指定变量 $var 当中的某个数值
	* @example
	* $v = $this->getvar($this->member, 'uid'); // $v = $this->member['uid']
	* $v = $this->getvar($this->member, 'test/hello/ok');  // $v = $this->member['test']['hello']['ok']
	*
	* @param string $var
	* @param string $key
	*
	* @return mixed
	*/
	public function getvar(&$var, $key = null) {
		if(isset($key)) {
			$key = explode('/', $key);
			foreach ($key as $k) {
				if (!isset($var[$k])) {
					return null;
				}
				$var = &$var[$k];
			}
		}
		return $var;
	}


	public function showmessage() {
		if(!empty($this->showmessage) && is_callable($this->showmessage)) {
			$p = func_get_args();
			if(is_string($this->showmessage)) {
				$fn = $this->showmessage;
				switch (func_num_args()) {
					case 0:	return $fn();break;
					case 1:	return $fn($p[0]);break;
					case 2:	return $fn($p[0], $p[1]);break;
					case 3:	return $fn($p[0], $p[1], $p[2]);exit;break;
					case 4:	return $fn($p[0], $p[1], $p[2], $p[3]);break;
					case 5:	return $fn($p[0], $p[1], $p[2], $p[3], $p[4]);break;
					default: return call_user_func_array($this->showmessage, $p);break;
				}
			} else {
				return call_user_func_array($this->showmessage, $p);
			}
		} else {
			return func_get_args();
		}
	}

	/**
	 * 注册方法执行前的功能函数
	 * @param string $name 方法名
	 * @param mixed $fn 函数名|静态方法：array('class', 'method')|实例方法：array('class'=>'class', 'method'=>'method');
	 * @example
	 * $obj->attach_before_method('newthread', 'var_dump'); //var_dump($parameters);
	 * $obj->attach_before_method('newthread', array('foo', 'var_dump')); //foo::var_dump($parameters);
	 * $obj->attach_before_method('newthread', array('class'=>'foo', 'method'=>'var_dump')); //$_obj = new foo(); $_obj->var_dump($parameters);
	 */
	public function attach_before_method($name, $fn) {
		$this->methods[$name][0][] = $fn;
	}

	/**
	 * 注册方法执行后的功能函数
	 * @param string $name 方法名
	 * @param mixed $fn 函数名|静态方法：array('class', 'method')|实例方法：array('class'=>'class', 'method'=>'method');
	 * @example
	 * $obj->attach_after_method('newthread', 'var_dump'); //var_dump($parameters);
	 * $obj->attach_after_method('newthread', array('foo', 'var_dump')); //foo::var_dump($parameters);
	 * $obj->attach_after_method('newthread', array('class'=>'foo', 'method'=>'var_dump')); //$_obj = new foo(); $_obj->var_dump($parameters);
	 */
	public function attach_after_method($name, $fn) {
		$this->methods[$name][1][] = $fn;
	}

	/**
	 * 批量注册方法执行前的功能函数
	 * @param string $name 方法名
	 * @param array $methods 功能函数数组
	 * @example
	 * $obj->attach_before_methods('newthread', array('var_dump', array('foo', 'var_dump'), array('class'=>'foo', 'method'=>'var_dump')));
	 */
	public function attach_before_methods($name, $methods){
		if(!empty($methods)) {
			foreach($methods as $method) {
				$this->methods[$name][0][] = $method;
			}
		}
	}

	/**
	 * 批量注册方法执行后的功能函数
	 * @param string $name 方法名
	 * @param array $methods 功能函数数组
	 * @example
	 * $obj->attach_after_methods('newthread', array('var_dump', array('foo', 'var_dump'), array('class'=>'foo', 'method'=>'var_dump')));
	 */
	public function attach_after_methods($name, $methods){
		if(!empty($methods)) {
			foreach($methods as $method) {
				$this->methods[$name][1][] = $method;
			}
		}
	}

	/**
	 * 参数过滤和初始化，主方法体开始处执行
	 * @param array $parameters 参数数组
	 * @example
		protected function _init_parameters($parameters){
			$varname = array(
				//公共数据
				'member', 'group', 'forum', 'modnewthreads',
				//threa数据
				'subject', 'displayorder', 'save', 'ordertype', 'hiddenreplies',
				'allownoticeauthor', 'readperm', 'price', 'typeid', 'sortid',
				'publishdate', 'digest', 'moderated', 'tstatus', 'isgroup',
				'replycredit', 'closed', 'special', 'tags',
				//post数据
				'message','clientip', 'invisible', 'isanonymous', 'usesig',
				'htmlon', 'bbcodeoff', 'smileyoff', 'parseurloff', 'pstatus'
			);
			foreach($varname as $name) {
				if(!isset($this->params[$name]) && isset($parameters[$name])) {
					$this->params[$name] = $parameters[$name];
				}
			}
		}
	 */
	abstract protected function _init_parameters($parameters);

}
?>
