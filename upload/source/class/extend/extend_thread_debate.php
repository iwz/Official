<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: extend_thread_debate.php 30673 2012-06-11 07:51:54Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class extend_thread_debate extends extend_thread_base {

	public $affirmpoint;
	public $negapoint;
	public $endtime;
	public $stand;

	public function before_newthread($parameters) {
		if(empty($_GET['affirmpoint']) || empty($_GET['negapoint'])) {
			showmessage('debate_position_nofound');
		} elseif(!empty($_GET['endtime']) && (!($this->endtime = @strtotime($_GET['endtime'])) || $this->endtime < TIMESTAMP)) {
			showmessage('debate_endtime_invalid');
		} elseif(!empty($_GET['umpire'])) {
			//if(!DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_member')." WHERE username='$_GET[umpire]'")) {
			if(!C::t('common_member')->fetch_uid_by_username($_GET['umpire'])) {
				$_GET['umpire'] = dhtmlspecialchars($_GET['umpire']);
				showmessage('debate_umpire_invalid', '', array('umpire' => $_GET['umpire']));
			}
		}
		$this->affirmpoint = censor(dhtmlspecialchars($_GET['affirmpoint']));
		$this->negapoint = censor(dhtmlspecialchars($_GET['negapoint']));
		$this->stand = intval($_GET['stand']);
		$this->param['extramessage'] = "\t".$_GET['affirmpoint']."\t".$_GET['negapoint'];
	}

	public function after_newthread() {
		if($this->group['allowpostdebate']) {//note 辩论入库

//		DB::query("INSERT INTO ".DB::table('forum_debate')." (tid, uid, starttime, endtime, affirmdebaters, negadebaters, affirmvotes, negavotes, umpire, winner, bestdebater, affirmpoint, negapoint, umpirepoint)
//			VALUES ('$tid', '$_G[uid]', '$publishdate', '$endtime', '0', '0', '0', '0', '$_GET[umpire]', '', '', '$affirmpoint', '$negapoint', '')");
		C::t('forum_debate')->insert(array(
			'tid' => $this->tid,
			'uid' => $this->member['uid'],
			'starttime' => $this->param['publishdate'],
			'endtime' => $this->endtime,
			'affirmdebaters' => 0,
			'negadebaters' => 0,
			'affirmvotes' => 0,
			'negavotes' => 0,
			'umpire' => $_GET['umpire'],
			'winner' => '',
			'bestdebater' => '',
			'affirmpoint' => $this->affirmpoint,
			'negapoint' => $this->negapoint,
			'umpirepoint' => ''
		));

	}
	}

	public function before_feed() {

		$message = !$this->param['price'] && !$this->param['readperm'] ? $this->param['message'] : '';
		$this->feed['icon'] = 'debate';
		$this->feed['title_template'] = 'feed_thread_debate_title';
		$this->feed['body_template'] = 'feed_thread_debate_message';
		$this->feed['body_data'] = array(
			'subject' => "<a href=\"forum.php?mod=viewthread&tid={$this->tid}\">{$this->param['subject']}</a>",
			'message' => messagecutstr($message, 150),
			'affirmpoint'=> messagecutstr($this->affirmpoint, 150),
			'negapoint'=> messagecutstr($this->negapoint, 150)
		);
	}

	public function after_newreply() {
		global $firststand, $stand;
		if($this->param['special'] == 5) {
	//		if(!DB::num_rows($standquery)) {
			if(!$firststand) {
				//debug 如果没有参与过辩论，更新辩论人数
	//			if($stand == 1) {
	//				DB::query("UPDATE ".DB::table('forum_debate')." SET affirmdebaters=affirmdebaters+1 WHERE tid='$_G[tid]'");
	//			} elseif($stand == 2) {
	//				DB::query("UPDATE ".DB::table('forum_debate')." SET negadebaters=negadebaters+1 WHERE tid='$_G[tid]'");
	//			}
				C::t('forum_debate')->update_debaters($this->thread['tid'], $stand);
			} else {
				$stand = $firststand;
			}
	//		if($stand == 1) {
	//			DB::query("UPDATE ".DB::table('forum_debate')." SET affirmreplies=affirmreplies+1 WHERE tid='$_G[tid]'");
	//		} elseif($stand == 2) {
	//			DB::query("UPDATE ".DB::table('forum_debate')." SET negareplies=negareplies+1 WHERE tid='$_G[tid]'");
	//		}
			C::t('forum_debate')->update_replies($this->thread['tid'], $stand);
	//		DB::query("INSERT INTO ".DB::table('forum_debatepost')." (tid, pid, uid, dateline, stand, voters, voterids) VALUES ('$_G[tid]', '$pid', '$_G[uid]', '$_G[timestamp]', '$stand', '0', '')");
			C::t('forum_debatepost')->insert(array(
			    'tid' => $this->thread['tid'],
			    'pid' => $this->pid,
			    'uid' => $this->member['uid'],
			    'dateline' => getglobal('timestamp'),
			    'stand' => $stand,
			    'voters' => 0,
			    'voterids' => '',
			));
		}
	}

	public function before_replyfeed() {
		global $stand;
		if($this->forum['allowfeed'] && !$this->param['isanonymous']) {
			if($this->param['special'] == 5 && $this->thread['authorid'] != $this->member['uid']) {
				$this->feed['icon'] = 'debate';
				if($stand == 1) {
					$this->feed['title_template'] = 'feed_thread_debatevote_title_1';
				} elseif($stand == 2) {
					$this->feed['title_template'] = 'feed_thread_debatevote_title_2';
				} else {
					$this->feed['title_template'] = 'feed_thread_debatevote_title_3';
				}
				$this->feed['title_data'] = array(
					'subject' => "<a href=\"forum.php?mod=viewthread&tid=".$this->thread['tid']."\">".$this->thread['subject']."</a>",
					'author' => "<a href=\"home.php?mod=space&uid=".$this->thread['authorid']."\">".$this->thread['author']."</a>"
				);
			}
		}
	}

	public function before_editpost($parameters) {
		//$this->param['orig'] = $this->post;
		$isfirstpost = $this->post['first'] ? 1 : 0;
		if($isfirstpost) {
			if($this->thread['special'] == 5 && $this->group['allowpostdebate']) {

				if(empty($_GET['affirmpoint']) || empty($_GET['negapoint'])) {
					showmessage('debate_position_nofound');
				} elseif(!empty($_GET['endtime']) && (!($endtime = @strtotime($_GET['endtime'])) || $endtime < TIMESTAMP)) {
					showmessage('debate_endtime_invalid');
				} elseif(!empty($_GET['umpire'])) {
					//if(!DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_member')." WHERE username='$_GET[umpire]'")) {
					if(!C::t('common_member')->fetch_uid_by_username($_GET['umpire'])) {
						$_GET['umpire'] = dhtmlspecialchars($_GET['umpire']);
						showmessage('debate_umpire_invalid');
					}
				}
				$affirmpoint = censor(dhtmlspecialchars($_GET['affirmpoint']));
				$negapoint = censor(dhtmlspecialchars($_GET['negapoint']));
	//				DB::query("UPDATE ".DB::table('forum_debate')." SET affirmpoint='$affirmpoint', negapoint='$negapoint', endtime='$endtime', umpire='$_GET[umpire]' WHERE tid='$_G[tid]'");
				C::t('forum_debate')->update($this->thread['tid'], array('affirmpoint' => $affirmpoint, 'negapoint' => $negapoint, 'endtime' => $endtime, 'umpire' => $_GET['umpire']));

			}
		}
	}
}

?>