<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: block_xml.php 28663 2012-03-07 05:50:37Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class block_xml extends discuz_block {

	var $blockdata = array();

	function block_xml($xmlid = null) {
		if(!empty($xmlid)) {
			//$blockxml = DB::fetch_first("SELECT * FROM ".DB::table('common_block_xml')." WHERE id='$xmlid'");
			if(!($blockxml = C::t('common_block_xml')->fetch($xmlid))) {
				return;
			}
			$this->blockdata = $blockxml;
			$this->blockdata['data'] = (array)dunserialize($blockxml['data']);
			//$this->blockdata['setting'] = (array)unserialize($blockxml['setting']);
		} else {
			//$query = DB::query('SELECT * FROM '.DB::table('common_block_xml'));
			//while($value = DB::fetch($query)) {
			foreach(C::t('common_block_xml')->range() as $value) {
				$one = $value;
				$one['data'] = (array)dunserialize($value['data']);
				$this->blockdata[] = $one;
			}
		}
	}

	/**
	 * 必须！本调用类显示的数据来源名称
	 * 返回本数据调用类的显示名称（显示在创建模块时选择“数据来源”的下拉列表里）
	 * @return <type>
	 */
	function name() {
		return dhtmlspecialchars($this->blockdata['data']['name']);
	}

	/**
	 * 必须！示例小分类名
	 * 返回一个数组： 第一个值为本数据类所在的模块分类；第二个值为模块分类显示的名称（显示在 DIY 模块面板）
	 * @return <type>
	 */
	function blockclass() {
		return dhtmlspecialchars($this->blockdata['data']['blockclass']);
	}

	/**
	 * 必须！
	 * 返回数据类中可供“模块样式”使用的字段。
	 * 格式见示例：
	 * name 为该字段的显示名称
	 * formtype 决定编辑单条数据时该字段的显示方式： 类型有： text, textarea, date, title, summary, pic； 详见 portalcp_block.htm 模板（搜 $field[formtype] ）
	 * datatype 决定该字段的数据展示，类型有： string, int, date, title, summary, pic； 详见 function_block.php 中 block_template 函数
	 * @return <type>
	 */
	function fields() {
		return dhtmlspecialchars($this->blockdata['data']['fields']);
	}

	/**
	 * 必须！
	 * 返回使用本数据类调用数据时的设置项
	 * 格式见示例：
	 * title 为显示的名称
	 * type 为表单类型， 有： text, password, number, textarea, radio, select, mselect, mradio, mcheckbox, calendar； 详见 function_block.php 中 block_makeform() 函数
	 * @return <type>
	 */
	function getsetting() {
		return dhtmlspecialchars($this->blockdata['data']['getsetting']);
	}

	/**
	 * 必须！
	 * 处理设置参数，返回数据
	 * 返回数据有两种：
	 * 一种是返回 html，放到模块 summary 字段，直接显示； 返回格式为： array('html'=>'返回内容', 'data'=>null)
	 * 一种是返回 data，通过模块样式渲染后展示，返回的数据应该包含 fields() 函数中指定的所有字段； 返回格式为： array('html'=>'', 'data'=>array(array('title'=>'value1'), array('title'=>'value2')))
	 * 特别的：
	 * parameter 参数包含 getsetting() 提交后的内容； 并附加了字段：
	 * items ，为用户指定显示的模块数据条数；
	 * bannedids ，为用户选择屏蔽某数据时记录在模块中的该数据 id。 应该在获取数据时屏蔽该数据；
	 *
	 * 如果返回的数据给 data， 那么应该包含 fields() 函数指定的所有字段。并附加以下字段：
	 * id 标志该数据的 id，如果用户屏蔽某数据时，会将该数据的 id 添加到 parameter[bannedids] 里
	 * idtype 标志该数据的 idtype
	 *
	 * @param <type> $style 模块样式（见 common_block_style 表）。 可以根据模块样式中用到的字段来选择性的获取/不获取某些数据
	 * @param <type> $parameter 用户对 getsetting() 给出的表单提交后的内容。
	 * @return <type>
	 */
	function getdata($style, $parameter) {
		$parameter = $this->cookparameter($parameter);
		$array = array();
		foreach($parameter as $key => $value) {
			if(is_array($value)) {
				$parameter[$key] = implode(',', $value);
			}
		}
		$parameter['clientid'] = $this->blockdata['clientid'];
		$parameter['op'] = 'getdata';
		$parameter['charset'] = CHARSET;
		$parameter['version'] = $this->blockdata['version'];
		$xmlurl = $this->blockdata['url'];
		$parse = parse_url($xmlurl);
		if(!empty($parse['host'])) {
			define('IN_ADMINCP', true);
			require_once libfile('function/importdata');
			$importtxt = @dfsockopen($xmlurl, 0, create_sign_url($parameter, $this->blockdata['key'], $this->blockdata['signtype']));
		} else {
			$ctx = stream_context_create(array('http' => array('timeout' => 20)));//设置20秒的超时时间
			$importtxt = @file_get_contents($xmlurl, false, $ctx);
		}
		if($importtxt) {
			require libfile('class/xml');
			$array = xml2array($importtxt);
		}
		$idtype = 'xml_'.$this->blockdata['id'];
		foreach($array['data'] as $key=>$value) {
			$value['idtype'] = $idtype;
			$array['data'][$key] = $value;
		}
		if(empty($array['data'])) $array['data'] = null;
		return $array;
	}

}

?>