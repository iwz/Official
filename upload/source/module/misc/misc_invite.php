<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: misc_invite.php 33107 2013-04-26 03:43:21Z andyzheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/friend');

$_GET['action'] = dhtmlspecialchars(preg_replace("/[^\[A-Za-z0-9_\]]/", '', $_GET['action']));
$friendgrouplist = friend_group_list();
if($_GET['action'] == 'group') {
	$id = intval($_GET['id']);
	//$isgroupuser = DB::result_first("SELECT uid FROM ".DB::table('forum_groupuser')." WHERE fid='$id' AND uid='{$_G['uid']}'");
	$groupuserinfo = C::t('forum_groupuser')->fetch_userinfo($_G['uid'], $id);
	if(empty($groupuserinfo['uid'])) {
		showmessage('group_invite_failed');
	}
	//$grouplevel = DB::result_first("SELECT level FROM ".DB::table('forum_forum')." WHERE fid='$id'");
	$foruminfo = C::t('forum_forum')->fetch($id);
	$grouplevel = $foruminfo['level'];
	loadcache('grouplevels');
	$grouplevel = $_G['grouplevels'][$grouplevel];
	$membermaximum = $grouplevel['specialswitch']['membermaximum'];
	if(!empty($membermaximum)) {
		//$curnum = DB::result_first("SELECT count(*) FROM ".DB::table('forum_groupuser')." WHERE fid='$id'");
		$curnum = C::t('forum_groupuser')->fetch_count_by_fid($id, -1);
		if($curnum >= $membermaximum) {
			showmessage('group_member_maximum', '', array('membermaximum' => $membermaximum));
		}
	}

	//$groupname = DB::result_first("SELECT name FROM ".DB::table('forum_forum')." WHERE fid='$id'");
	$groupname = $foruminfo['name'];
	$invitename = lang('group/misc', 'group_join', array('groupname' => $groupname));
	if(!submitcheck('invitesubmit')) {
		$friends = friend_list($_G['uid'], 100);//note 取100个好友，筛选已经邀请或已经是群组成员的好友。
		if(!empty($friends)) {
			$frienduids = array_keys($friends);
			$inviteduids = array();
			//$query = DB::query("SELECT inviteuid FROM ".DB::table('forum_groupinvite')." WHERE fid='$id' AND inviteuid IN (".dimplode($frienduids).") AND uid='$_G[uid]'");
			$query = C::t('forum_groupinvite')->fetch_all_inviteuid($id, $frienduids, $_G['uid']);
			//while($inviteuser = DB::fetch($query)) {
			foreach($query as $inviteuser) {
				$inviteduids[$inviteuser['inviteuid']] = $inviteuser['inviteuid'];
			}
			//$query = DB::query("SELECT uid FROM ".DB::table('forum_groupuser')." WHERE fid='$id' AND uid IN (".dimplode($frienduids).")");
			$query = C::t('forum_groupuser')->fetch_all_userinfo($frienduids, $id);
			//while($inviteuser = DB::fetch($query)) {
			foreach($query as $inviteuser) {
				$inviteduids[$inviteuser['uid']] = $inviteuser['uid'];
			}
		}
		$inviteduids = !empty($inviteduids) ? implode(',', $inviteduids) : '';
	} else {
		$uids = $_GET['uids'];
		if($uids) {
			if(count($uids) > 20) {
				showmessage('group_choose_friends_max');
			}
			//$query = DB::query("SELECT uid, username FROM ".DB::table('common_member')." WHERE uid IN (".dimplode($uids).")");
			//while($user = DB::fetch($query)) {
			foreach(C::t('common_member')->fetch_all($uids, false, 0) as $uid => $user) {
				//DB::query("REPLACE INTO ".DB::table('forum_groupinvite')." (fid, uid, inviteuid, dateline) VALUES ('$id', '$_G[uid]', '$uid', '".TIMESTAMP."')");
				C::t('forum_groupinvite')->insert(array('fid' => $id, 'uid' => $_G['uid'], 'inviteuid' => $uid, 'dateline' => TIMESTAMP), true, true);
				//$already = DB::affected_rows();
				$already = C::t('forum_groupinvite')->affected_rows();
				if($already == 1) {
					notification_add($uid, 'group', 'group_member_invite', array('groupname' => $groupname, 'fid' => $id, 'url' =>'forum.php?mod=group&action=join&fid='.$id, 'from_id' => $id, 'from_idtype' => 'invite_group'), 1);
				}
			}
			showmessage('group_invite_succeed', "forum.php?mod=group&fid=$id");
		} else {
			showmessage('group_invite_choose_member', "forum.php?mod=group&fid=$id");
		}
	}
} elseif($_GET['action'] == 'thread') {
	$inviteduids = array();
	$id = intval($_GET['id']);
//	$thread = DB::fetch_first("SELECT authorid,special,subject FROM ".DB::table('forum_thread')." WHERE tid='$id'");
	$thread = C::t('forum_thread')->fetch($id);
	$at = 0;
	$maxselect = 20;
	if(empty($_GET['activity'])) {
		$at = 1;
		$maxselect = 0;
		if($_G['group']['allowat']) {
			$atnum = 0;
//			$query = DB::query("SELECT * FROM ".DB::table('home_notification')." WHERE authorid='$_G[uid]' AND type='at' AND from_id='$id'");
//			while($row = DB::fetch($query)) {
			foreach(C::t('home_notification')->fetch_all_by_authorid_fromid($_G['uid'], $id, 'at') as $row) {
				$atnum ++;
				$inviteduids[$row[uid]] = $row['uid'];
			}
			$maxselect = $_G['group']['allowat'] - $atnum;
		} else {
			showmessage('noperm_at_user');
		}
		if($maxselect <= 0) {
			showmessage('thread_at_usernum_limit');
		}
		$invitename =  lang('forum/misc', 'at_invite');
	} else {
		$invitename =  lang('forum/misc', 'join_activity');
	}

	/*
	switch($thread['special']) {
		case 0:$invitename = lang('forum/misc', 'join_topic');break;
		case 1:$invitename = lang('forum/misc', 'join_poll');break;
		case 2:$invitename = lang('forum/misc', 'buy_trade');break;
		case 3:$invitename = lang('forum/misc', 'join_reward');break;
		case 4:$invitename = lang('forum/misc', 'join_activity');break;
		case 5:$invitename = lang('forum/misc', 'join_debate');break;
	}
	 * */
	if(!submitcheck('invitesubmit')) {
		$inviteduids = !empty($inviteduids) ? implode(',', $inviteduids) : '';
	} else {
		$uids = $_GET['uids'];
		if($uids) {
			if(count($uids) > $maxselect) {
				showmessage('group_choose_friends_max');
			}
			//$posttable = getposttablebytid($id);
			//$post = DB::fetch_first("SELECT pid, message FROM ".DB::table($posttable)." WHERE tid='$id' AND first=1");
			$post = C::t('forum_post')->fetch_threadpost_by_tid_invisible($id);
			require_once libfile('function/post');
			$post['message'] = messagecutstr($post['message'], 150);
			//$query = DB::query("SELECT uid, username FROM ".DB::table('common_member')." WHERE uid IN (".dimplode($uids).")");
			//while($user = DB::fetch($query)) {
			foreach(C::t('common_member')->fetch_all($uids, false, 0) as $uid => $user) {
				if($at) {
					notification_add($uid, 'at', 'at_message', array('from_id' => $id, 'from_idtype' => 'at', 'buyerid' => $_G['uid'], 'buyer' => $_G['username'], 'tid' => $id, 'subject' => $thread['subject'], 'pid' => $post['pid'], 'message' => $post['message']));
				} else {
					notification_add($uid, 'thread', 'thread_invite', array('subject' => $thread['subject'], 'invitename' => $invitename, 'tid' => $id, 'from_id' => $id, 'from_idtype' => 'invite_thread'));
				}
			}
			showmessage(($at ? 'at_succeed' : 'group_invite_succeed'), "forum.php?mod=viewthread&tid=$id");
		} else {
			showmessage(($at ? 'at_choose_member' : 'group_invite_choose_member'), "forum.php?mod=viewthread&tid=$id");
		}
	}
} elseif($_GET['action'] == 'blog') {
	$id = intval($_GET['id']);
	//$blog = DB::fetch_first("SELECT * FROM ".DB::table('home_blog')." WHERE blogid='$id'");
	$blog = C::t('home_blog')->fetch($id);

	if(!submitcheck('invitesubmit')) {
		$inviteduids = '';
	} else {
		$uids = $_GET['uids'];
		if($uids) {
			if(count($uids) > 20) {
				showmessage('group_choose_friends_max');
			}
			//$query = DB::query("SELECT uid, username FROM ".DB::table('common_member')." WHERE uid IN (".dimplode($uids).")");
			//while($user = DB::fetch($query)) {
			foreach(C::t('common_member')->fetch_all($uids, false, 0) as $uid => $user) {
				notification_add($uid, 'blog', 'blog_invite', array('subject' => $blog['subject'], 'uid' => $blog['uid'], 'blogid' => $id, 'from_id' => $id, 'from_idtype' => 'invite_blog'));
			}
			showmessage('group_invite_succeed', "home.php?mod=space&uid=".$blog['uid']."&do=blog&id=$id");
		} else {
			showmessage('group_invite_choose_member', "home.php?mod=space&uid=".$blog['uid']."&do=blog&id=$id");
		}
	}
} elseif($_GET['action'] == 'article') {
	$id = intval($_GET['id']);
	//$article = DB::fetch_first("SELECT * FROM ".DB::table('portal_article_title')." WHERE aid='$id'");
	$article = C::t('portal_article_title')->fetch($id);

	if(!submitcheck('invitesubmit')) {
		$inviteduids = '';
	} else {
		require_once libfile('function/portal');
		$article_url = fetch_article_url($article);
		$uids = $_GET['uids'];
		if($uids) {
			if(count($uids) > 20) {
				showmessage('group_choose_friends_max');
			}
			//$query = DB::query("SELECT uid, username FROM ".DB::table('common_member')." WHERE uid IN (".dimplode($uids).")");
			//while($user = DB::fetch($query)) {
			foreach(C::t('common_member')->fetch_all($uids, false, 0) as $uid => $user) {
				notification_add($uid, 'article', 'article_invite', array('subject' => $article['title'], 'url' => $article_url, 'from_id' => $id, 'from_idtype' => 'invite_article'));
			}
			showmessage('group_invite_succeed', $article_url);
		} else {
			showmessage('group_invite_choose_member', $article_url);
		}
	}
}
/*疑似废弃代码
function uc_avatar($uid, $size = '', $returnsrc = FALSE) {
	global $_G;
	return avatar($uid, $size, $returnsrc, FALSE, $_G['setting']['avatarmethod'], $_G['setting']['ucenterurl']);
}*/

include template('common/invite');
?>