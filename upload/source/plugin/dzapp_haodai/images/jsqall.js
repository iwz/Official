
//车贷计算输入验证函数
var cd_a = 20; //购车价格初始值
var cd_b2 = 36; //贷款期限初始值
var cd_c = 5.4; //年利率初始值
var car_price = true;
var car_nll = true;
var pop_list = true;
//公积金贷款计算器函数
var gjj_a = 20; //设置默认贷款金额
var gjj_b2 = 36; //设置默认贷款期限
var gjj_c = 5.4; //设置默认年利率
var gjj_d = 10000; //设置默认平米单价
var gjj_e = 200; //设置默认面积
var gjj_b6 = 1; //设置默认购房性质
var gjj_price = true;
var gjj_nll = true;
var gjj_list = true;
var gjj_pmdj = true;
var gjj_mj = true;
//组合贷款计算器函数
var zhdk_b2 = 36; //设置默认贷款期限
var zhdk_a = 20; //设置默认-公积金贷款金额
var zhdk_b = 20; //设置默-公积金贷款利率 
var zhdk_c = 20; //设置默认-商业贷款金额
var zhdk_d = 20; //设置默认-商业贷款利率
var zhdk_gjjprice = true; //公积金贷款金额-为真
var jq = jQuery.noConflict();
var zhdk_gjjll = true; //公积金贷款利率-为真 
var zhdk_sdprice = true; //商业贷款金额-为真
var zhdk_sdll = true; //商业贷款利率-为真
//二手房贷款计算器函数
var efd_mj = true;
var efd_zj = true;
var esfd_a = 0; //设置默认贷款期限
var esfd_b = 0; //设置默认贷款期限
jq(document).ready(function() {
    fdj();
    //车贷计算函数
    cdyz1(); //车贷 -- 购车价格 验证函数
    cdyz2(); //车贷 -- 贷款期限 函数
    cdyz3(); //车贷 -- 年利率 验证函数
    cdjs(); //车贷 -- 计算函数
    //公积金贷款计算函数
    jq('#jsstyle_0').attr('checked', true)
    gjjyz1(); //公积金 -- 购车价格 验证函数
    gjjyz2(); //公积金 -- 贷款期限 函数
    gjjyz3(); //公积金 -- 年利率 验证函数
    gjjyz4(); //公积金 -- 平米单价 函数
    gjjyz5(); //公积金 -- 面积 验证函数
    gjjyz6(); //公积金 -- 购房性质 验证函数
    gjjjs(); //公积金 -- 计算函数
    //组合贷款计算函数
    zhdkyz1(); //组合贷款 -- 贷款期限 函数
    zhdkyz2(); //组合贷款 -- 公积金贷款金额 函数
    zhdkyz3(); //组合贷款 -- 公积金贷款利率 函数
    zhdkyz4(); //组合贷款 -- 商业贷款金额 函数
    zhdkyz5(); //组合贷款 -- 商业贷款利率 函数
    zhdkjs(); //公积金 -- 计算函数 
    //二手房贷款计算函数
    jq('#jsfwxz_0').attr('checked', true)
    jq('#jshas5_0').attr('checked', true)
    esfdyz1(); //组合贷款 -- 贷款期限 函数
    esfdyz2(); //组合贷款 -- 公积金贷款金额 函数
    esfdjs(); //公积金 -- 计算函数 
});

//公积金计算器--计算方式选择
function fdj() {
    //var fdjc = jq('div.jsb div').eq(0);
    var fdjt01 = jq('#jsstyle_0');
    var fdjt02 = jq('#jsstyle_1');
    var fdjt01_con = fdjt01.parent().parent().parent().children('.fdj_01');
    var fdjt02_con = fdjt02.parent().parent().parent().children('.fdj_02');
    fdjt01.click(function() {
        fdjt01_con.removeClass('none');
        fdjt02_con.addClass('none');

        jq('.js_total').text('0');  //贷款总额
        jq('.js_time').text('0');  //还款时间
        jq('#js_Lmy').text('0');  //每月还款
        jq('#js_Lzlx').text('0'); //总支付利息  每月等额还款
        jq('#js_Lbxhj').text('0'); //本息合计  每月等额还款
        //逐月递减还款
        jq('#js_Rsy').text('0'); //首月还款
        jq('#js_Rdj').text('0'); //每月递减 计算
        jq('#js_Rzlx').text('0'); //总利息
        jq('#js_Rbxhj').text('0'); //本息合计
    });
    fdjt02.click(function() {
        fdjt01_con.addClass('none');
        fdjt02_con.removeClass('none');

        jq('.js_total').text('0');  //贷款总额
        jq('.js_time').text('0');  //还款时间
        jq('#js_Lmy').text('0');  //每月还款
        jq('#js_Lzlx').text('0'); //总支付利息  每月等额还款
        jq('#js_Lbxhj').text('0'); //本息合计  每月等额还款
        //逐月递减还款
        jq('#js_Rsy').text('0'); //首月还款
        jq('#js_Rdj').text('0'); //每月递减 计算
        jq('#js_Rzlx').text('0'); //总利息
        jq('#js_Rbxhj').text('0'); //本息合计
    });
}

//车贷计算函数 ----------start------------//
//购车价格
function cdyz1(cd_a) {
    var cipt = jq('#car_price') //购车价格 ipt
    var cd_a = cipt.val();
    //购车价格验证 
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义购车价格 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        var error_ts = jq('.car_price_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            car_price = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                car_price = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                car_price = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return cd_a;
}
//贷款期限
function cdyz2() {
    var ctime = jq('#cd_pop_list') //贷款期限下拉按钮
    var clist = jq('#cd_list_options'); //贷款期限下拉框
    //贷款期限 按钮下拉函数
    ctime.click(function() {
        var cd_yon = clist.hasClass('none');
        if (cd_yon == true) {
            clist.removeClass('none');
        } else {
            clist.addClass('none'); //把所有此类下拉框隐藏
        }
    });
}
//贷款时间下拉点击选择
function cdmonth(bb) {
    var bthis = jq(bb);
    //var cd_val = bthis.html(); //提取当前点击的val
    var cd_reval = bthis.attr('reval'); //提取当前点击reval的相关值
    var cdshow = jq('#car_dktime'); //当前最顶部显示的列表项
    var cdli = jq('#cd_list_options li a');
    var cdul = jq('#cd_list_options');
    var cdshow_reval = cdshow.attr('reval');
    cdshow_reval = cd_reval;
    cd_b2 = cdshow_reval;
    //bb = cd_reval;
    cdshow.text(bthis.text());
    cdli.removeClass('mo');
    bthis.addClass('mo');
    cdul.addClass('none');
    //alert(cdshow_reval);
    return cd_b2;
}
//年利率
function cdyz3(cd_c) {
    var cnll = jq('#car_nll');  //年利率 ipt
    var error_ts = jq('.car_nll_error'); //定义提示错误 
    var cd_c = cnll.val();
    //年利率验证
    cnll.blur(function() {
        var Cnll_val = cnll.val(); //定义年利率 val
        var Tnll_val = jq.trim(Cnll_val); //定义年利率 去空格后的val
        if (Tnll_val == '') {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            car_nll = false;
        } else if (Tnll_val.length > 0) {
            if (!isNaN(Tnll_val)) {
                cnll.addClass('success_ts');
                error_ts.addClass('none');
                car_nll = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个年利率的数字');
                cnll.addClass('error_ts');
                car_nll = false;
            }
        }
    });
    cnll.focus(function() {
        cnll.removeClass('success_ts');
        cnll.removeClass('error_ts');
    });
    return cd_c;
}
//车贷计算公式
function cdjs(cd_a, cd_c) {
    var cbtn = jq('#cd_jsbtn');  //计算 btn
    cbtn.click(function() {
//        alert(car_price + ',' + car_nll + ',' + pop_list);
        if (car_price == true && car_nll == true && pop_list == true) {
            var cd_sf = jq('#topSf'); //定义首付 
            var cd_yg = jq('#topYg'); //定义月供  
            var cd_time = jq('#topM'); //定义时间 
            var cd_price = jq('#topPrice'); //定义购车价格 
            var cd_all = jq('#topTotal'); //定义总计花费    
            var cd_more = jq('#topMore'); //定义多花费  

            var r1 = cdyz1(cd_a); //购车价格  将字符转成数字格式-输入值
            var r2 = cd_b2; //贷款期限  将字符转成数字格式-默认值
            var r3 = cdyz3(cd_c); //年利率  将字符转成数字格式

            //计算开始
            cd_sf = Math.round((r1 * (0.3)) * 10000) / 10000;  //首付计算
            cd_price = Math.round(r1 * 10000) / 10000;//购车价格    
            cd_time = r2;   //点击下拉对应相应的月份数 方法二 时间计算
            //月供计算
            var cd_bj = cd_price - cd_sf; //本金 = 购车价格 - 首付
            var yll = (r3 * 0.01) / 12;  //月利率 = 年利率/12*（贷款月份数） 
            var t = Math.pow((1 + yll), cd_time);
            //每月还款(即月供)
            cd_yg = Math.round((cd_bj * yll * (t / (t - 1))) * 10000) / 10000;
            //总计花费
            cd_all = Math.round((cd_sf + cd_yg * cd_time) * 10000) / 10000;
            //多花费
            cd_more = Math.round((cd_all - cd_price) * 10000) / 10000;
            //排列出来
            jq('#topSf').text(cd_sf);
            jq('#topYg').text(cd_yg * 10000);
            jq('#topM').text(cd_time);
            jq('#topPrice').text(cd_price);
            jq('#topTotal').text(cd_all);
            jq('#topMore').text(cd_more);

        } else {
//            cdyz1(); //车贷 -- 验证函数1
//            cdyz2(); //车贷 -- 验证函数2
//            cdyz3(); //车贷 -- 验证函数3
        }
        ;

    });
}

//车贷计算函数 ----------over------------//

//公积金贷款计算器 ----------start------------//
//贷款金额
function gjjyz1(gjj_a) {
    var cipt = jq('#gjj_price') //购车价格 ipt
    var gjj_a = cipt.val();
    //购车价格验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义购车价格 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            gjj_price = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                gjj_price = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                gjj_price = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return gjj_a;
}
//贷款期限
function gjjyz2() {
    var ctime = jq('#gjj_pop_list') //贷款期限下拉按钮
    var clist = jq('#gjj_list_options'); //贷款期限下拉框
    //贷款期限 按钮下拉函数
    ctime.click(function() {
        var js_yon = clist.hasClass('none');
        if (js_yon == true) {
            clist.removeClass('none');
        } else {
            clist.addClass('none'); //把所有此类下拉框隐藏
        }
    });
}
//贷款期限--下拉点击选择
function gjjmonth(gjjbb) {
    var bthis = jq(gjjbb);
    //var cd_val = bthis.html(); //提取当前点击的val
    var js_reval = bthis.attr('reval'); //提取当前点击reval的相关值
    var jsshow = jq('#gjj_dktime'); //当前最顶部显示的列表项
    var jsli = jq('#gjj_list_options li a');
    var jsul = jq('#gjj_list_options');
    var jsshow_reval = jsshow.attr('reval');
    jsshow_reval = js_reval;
    gjj_b2 = jsshow_reval;
    //bb = cd_reval;
    jsshow.text(bthis.text());
    jsli.removeClass('mo');
    bthis.addClass('mo');
    jsul.addClass('none');
    //alert(cdshow_reval);
    return gjj_b2;
}
//年利率
function gjjyz3(gjj_c) {
    var cnll = jq('#gjj_nll');  //年利率 ipt
    //var error_ts = jq('.js_nll_error'); //定义提示错误 
    var gjj_c = cnll.val();
    //年利率验证
    cnll.blur(function() {
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        var Cnll_val = cnll.val(); //定义年利率 val
        var Tnll_val = jq.trim(Cnll_val); //定义年利率 去空格后的val
        if (Tnll_val == '') {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            gjj_nll = false;
        } else if (Tnll_val.length > 0) {
            if (!isNaN(Tnll_val)) {
                cnll.addClass('success_ts');
                error_ts.addClass('none');
                gjj_nll = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个年利率的数字');
                cnll.addClass('error_ts');
                gjj_nll = false;
            }
        }
    });
    cnll.focus(function() {
        cnll.removeClass('success_ts');
        cnll.removeClass('error_ts');
    });
    return gjj_c;
}
//平米单价
function gjjyz4(gjj_d) {
    var cipt = jq('#gjj_pmdj') //购车价格 ipt
    var gjj_d = cipt.val();
    //购车价格验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义购车价格 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            gjj_pmdj = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                gjj_pmdj = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                gjj_pmdj = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return gjj_d;
}
//面积
function gjjyz5(gjj_e) {
    var cipt = jq('#gjj_mj') //购车价格 ipt
    var gjj_e = cipt.val();
    //购车价格验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义购车价格 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            gjj_mj = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                gjj_mj = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                gjj_mj = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return gjj_e;
}
//购房性质
function gjjyz6() {
    var ctime = jq('#gjj_nature_list') //贷款期限下拉按钮
    var clist = jq('#gjj_nature_options'); //贷款期限下拉框
    //贷款期限 按钮下拉函数
    ctime.click(function() {
        var js_yon = clist.hasClass('none');
        if (js_yon == true) {
            clist.removeClass('none');
        } else {
            clist.addClass('none'); //把所有此类下拉框隐藏
        }
    });
}
//购房性质--下拉点击选择
function gjjnature(gjjb6) {
    var bthis = jq(gjjb6);
    //var cd_val = bthis.html(); //提取当前点击的val
    var js_reval = bthis.attr('reval'); //提取当前点击reval的相关值
    var jsshow = jq('#gjj_nature_show'); //当前最顶部显示的列表项
    var jsli = jq('#gjj_nature_options li a');
    var jsul = jq('#gjj_nature_options');
    var jsshow_reval = jsshow.attr('reval');
    jsshow_reval = js_reval;
    gjj_b6 = jsshow_reval;
    //bb = cd_reval;
    jsshow.text(bthis.text());
    jsli.removeClass('mo');
    bthis.addClass('mo');
    jsul.addClass('none');
    //alert(gjj_b6);
    return gjj_b6;
}
//公积金计算公式
function gjjjs(gjj_a, gjj_c, gjj_d, gjj_e) {
    var gjjtn = jq('#gjj_jsbtn');  //计算 btn
    gjjtn.click(function() {
        var js_total = jq('.js_total'); //贷款总额 
        var js_time = jq('.js_time'); //还款月数
        var js_Lmy = jq('#js_Lmy'); //每月还款  每月等额还款
        var js_Rsy = jq('#js_Rsy'); //首月还款  逐月递减还款
        var js_Rdj = jq('#js_Rdj'); //每月递减  逐月递减还款
        var js_Lzlx = jq('#js_Lzlx'); //总支付利息  每月等额还款
        var js_Rzlx = jq('#js_Rzlx'); //总支付利息  逐月递减还款
        var js_Lbxhj = jq('#js_Lbxhj'); //本息合计  每月等额还款
        var js_Rbxhj = jq('#js_Rbxhj'); //本息合计  逐月递减还款

        var r1 = gjjyz1(gjj_a); //贷款金额  将字符转成数字格式-输入值
        var r2 = gjj_b2; //贷款期限  将字符转成数字格式-默认值
        var r3 = gjjyz3(gjj_c); //年利率  将字符转成数字格式
        var r4 = gjjyz4(gjj_d); //平米单价  将字符转成数字格式-输入值
        var r5 = gjjyz5(gjj_e) * 0.0001; //面积  将字符转成数字格式-输入值
        var r6 = gjj_b6; //购房性质  将字符转成数字格式-输入值
        if (jq('#jsstyle_0').attr('checked') == 'checked') {
            if (gjj_price == true && gjj_nll == true && gjj_list == true) {
                //按贷款额度计算
                js_total = Math.round(r1 * 10000) / 10000;  //计算-贷款总额
                js_time = Math.round(r2 * 10000) / 10000;  //计算-还款时间
                //计算-还款月数
                var yll = Math.round(((r3 * 0.01) / 12) * 10000) / 10000;  //月利率 = 年利率/12*（贷款月份数）
                var t = Math.pow((1 + yll), js_time);
                js_Lmy = Math.round((js_total * yll * (t / (t - 1))) * 10000) / 10000; //计算-每月还款
                //js_Lmy = Math.round((js_total * yll * (t / (t - 1)))*10000)/10000
                var js_Lzlx_00 = (js_time * js_Lmy) - js_total;  //计算-总支付利息  每月等额还款
                js_Lzlx = Math.round(js_Lzlx_00 * 10000) / 10000;
                //js_Lzlx = (js_time * js_Lmy) - js_total;  //计算-总支付利息  每月等额还款
                js_Lbxhj = js_Lzlx + Math.round(js_total);  //计算-本息合计  每月等额还款
                //等本计算
                var js_mbj = js_total / js_time;  //每月本金 计算
                var js_Rsy_00 = js_mbj + (js_total * yll); //首月还款 计算
                js_Rsy = Math.round(js_Rsy_00 * 10000) / 10000;
                var js_Rdj_00 = (js_mbj + (js_total - js_mbj) * yll) - (js_mbj + (js_total - js_mbj * 2) * yll); //每月递减 计算
                js_Rdj = Math.round(js_Rdj_00 * 10000) / 10000;
                var js_Rzlx_00 = js_total * yll * (js_time + 1) / 2; //总利息 计算
                js_Rzlx = Math.round(js_Rzlx_00 * 10000) / 10000;
                //js_Rzlx = Math.round(js_total) + Math.round(yll) + Math.round(js_time);
                js_Rbxhj = js_Rzlx + Math.round(js_total); //本息合计 计算

                jq('.js_total').text(js_total);  //贷款总额
                jq('.js_time').text(js_time);  //还款时间
                jq('#js_Lmy').text(js_Lmy);  //每月还款
                jq('#js_Lzlx').text(js_Lzlx); //总支付利息  每月等额还款
                jq('#js_Lbxhj').text(js_Lbxhj); //本息合计  每月等额还款
                //逐月递减还款
                jq('#js_Rsy').text(js_Rsy); //首月还款
                jq('#js_Rdj').text(js_Rdj); //每月递减 计算
                jq('#js_Rzlx').text(js_Rzlx); //总利息
                jq('#js_Rbxhj').text(js_Rbxhj); //本息合计
            }
        }
        if (jq('#jsstyle_1').attr('checked') == 'checked') {
            if (gjj_pmdj == true && gjj_mj == true && gjj_nll == true) {
                //按贷款额度计算
                //alert(r6);
                var js_fjzj = Math.round(r4 * r5 * 10000) / 10000; //定义并计算房价总价 = 平米价格 * 平米
                var bx = 0;
                if (r6 == 1) {
                    bx = 0.7; //alert('1111');
                } else if (r6 == 2) {
                    bx = 0.4; //alert('2222');
                }
                var dz = Math.round(js_fjzj * bx * 10000) / 10000; //定义贷款总额 = 房价总价 - 首付
                js_total = Math.round(dz * 10000) / 10000;  //计算-贷款总额
                js_time = Math.round(r2 * 10000) / 10000;  //计算-还款时间
                //计算-还款月数
                var yll = Math.round(((r3 * 0.01) / 12) * 10000) / 10000;  //月利率 = 年利率/12*（贷款月份数）
                var t = Math.pow((1 + yll), js_time);
                js_Lmy = Math.round((js_total * yll * (t / (t - 1))) * 10000) / 10000; //计算-每月还款
                //js_Lmy = Math.round((js_total * yll * (t / (t - 1)))*10000)/10000
                var js_Lzlx_00 = (js_time * js_Lmy) - js_total;  //计算-总支付利息  每月等额还款
                js_Lzlx = Math.round(js_Lzlx_00 * 10000) / 10000;
                //js_Lzlx = (js_time * js_Lmy) - js_total;  //计算-总支付利息  每月等额还款
                js_Lbxhj = js_Lzlx + Math.round(js_total);  //计算-本息合计  每月等额还款
                //等本计算
                var js_mbj = js_total / js_time;  //每月本金 计算
                var js_Rsy_00 = js_mbj + (js_total * yll); //首月还款 计算
                js_Rsy = Math.round(js_Rsy_00 * 10000) / 10000;
                var js_Rdj_00 = (js_mbj + (js_total - js_mbj) * yll) - (js_mbj + (js_total - js_mbj * 2) * yll); //每月递减 计算
                js_Rdj = Math.round(js_Rdj_00 * 10000) / 10000;
                var js_Rzlx_00 = js_total * yll * (js_time + 1) / 2; //总利息 计算
                js_Rzlx = Math.round(js_Rzlx_00 * 10000) / 10000;
                //js_Rzlx = Math.round(js_total) + Math.round(yll) + Math.round(js_time);
                js_Rbxhj = js_Rzlx + Math.round(js_total); //本息合计 计算

                jq('.js_total').text(js_total);  //贷款总额
                jq('.js_time').text(js_time);  //还款时间
                jq('#js_Lmy').text(js_Lmy);  //每月还款
                jq('#js_Lzlx').text(js_Lzlx); //总支付利息  每月等额还款
                jq('#js_Lbxhj').text(js_Lbxhj); //本息合计  每月等额还款
                //逐月递减还款
                jq('#js_Rsy').text(js_Rsy); //首月还款
                jq('#js_Rdj').text(js_Rdj); //每月递减 计算
                jq('#js_Rzlx').text(js_Rzlx); //总利息
                jq('#js_Rbxhj').text(js_Rbxhj); //本息合计
            }
        }
    });
}

//公积金贷款计算器 ----------over------------//


//组合贷款计算器 ----------start------------//
//贷款期限
function zhdkyz1() {
    var ctime = jq('#zhdk_pop_list') //贷款期限下拉按钮
    var clist = jq('#zhdk_list_options'); //贷款期限下拉框
    //贷款期限 按钮下拉函数
    ctime.click(function() {
        var js_yon = clist.hasClass('none');
        if (js_yon == true) {
            clist.removeClass('none');
        } else {
            clist.addClass('none'); //把所有此类下拉框隐藏
        }
    });
}
//贷款期限--下拉点击选择
function zhdkmonth(zhdkbb) {
    var bthis = jq(zhdkbb);
    //var cd_val = bthis.html(); //提取当前点击的val
    var js_reval = bthis.attr('reval'); //提取当前点击reval的相关值
    var jsshow = jq('#zhdk_dktime'); //当前最顶部显示的列表项
    var jsli = jq('#zhdk_list_options li a');
    var jsul = jq('#zhdk_list_options');
    var jsshow_reval = jsshow.attr('reval');
    jsshow_reval = js_reval;
    zhdk_b2 = jsshow_reval;
    //bb = cd_reval;
    jsshow.text(bthis.text());
    jsli.removeClass('mo');
    bthis.addClass('mo');
    jsul.addClass('none');
    //alert(cdshow_reval);
    return zhdk_b2;
}
//公积金贷款金额
function zhdkyz2(zhdk_a) {
    var cipt = jq('#zhdk_gjjprice') //金额 ipt
    var zhdk_a = cipt.val();
    //验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义金额 val
        var Tprice_val = jq.trim(Cprice_val); //定义定义金额 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            zhdk_gjjprice = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                zhdk_gjjprice = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                zhdk_gjjprice = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return zhdk_a;
}
//公积金贷款利率
function zhdkyz3(zhdk_b) {
    var cipt = jq('#zhdk_gjjll') //利率 ipt
    var zhdk_b = cipt.val();
    //验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义利率 val
        var Tprice_val = jq.trim(Cprice_val); //定义利率 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            zhdk_gjjll = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                zhdk_gjjll = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                zhdk_gjjll = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return zhdk_b;
}
//商业贷款金额
function zhdkyz4(zhdk_c) {
    var cipt = jq('#zhdk_sdprice') //金额 ipt
    var zhdk_c = cipt.val();
    //验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义金额 val
        var Tprice_val = jq.trim(Cprice_val); //定义金额 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            zhdk_sdprice = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                zhdk_sdprice = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                zhdk_sdprice = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return zhdk_c;
}
//商业贷款利率
function zhdkyz5(zhdk_d) {
    var cipt = jq('#zhdk_sdll') //利率 ipt
    var zhdk_d = cipt.val();
    //验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义利率 val
        var Tprice_val = jq.trim(Cprice_val); //定义利率 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            zhdk_sdll = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                zhdk_sdll = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                zhdk_sdll = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return zhdk_d;
}
//组合贷款计算公式
function zhdkjs(zhdk_a, zhdk_b, zhdk_c, zhdk_d) {
    var zhdktn = jq('#zhdk_jsbtn');  //计算 btn
    zhdktn.click(function() {
        var js_total = jq('.js_total'); //贷款总额 
        var js_time = jq('.js_time'); //还款月数
        var js_Lmy = jq('#js_Lmy'); //每月还款  每月等额还款
        var js_Rsy = jq('#js_Rsy'); //首月还款  逐月递减还款
        var js_Rdj = jq('#js_Rdj'); //每月递减  逐月递减还款
        var js_Lzlx = jq('#js_Lzlx'); //总支付利息  每月等额还款
        var js_Rzlx = jq('#js_Rzlx'); //总支付利息  逐月递减还款
        var js_Lbxhj = jq('#js_Lbxhj'); //本息合计  每月等额还款
        var js_Rbxhj = jq('#js_Rbxhj'); //本息合计  逐月递减还款

        var r0 = zhdk_b2; //贷款期限  将字符转成数字格式-默认值
        var r1 = zhdkyz2(zhdk_a); //公积金贷款金额  将字符转成数字格式-输入值
        var r2 = zhdkyz3(zhdk_b); //公积金贷款利率  将字符转成数字格式-默认值
        var r3 = zhdkyz4(zhdk_c); //商业贷款金额  将字符转成数字格式
        var r4 = zhdkyz5(zhdk_d); //商业贷款利率  将字符转成数字格式-输入值

        if (zhdk_gjjprice == true && zhdk_gjjll == true && zhdk_sdprice == true && zhdk_sdll == true) {
            //////num-1 start

            //按贷款额度计算
            var js_total_01 = Math.round(r1 * 10000) / 10000;  //计算-公积金贷款金额
            js_time = Math.round(r0 * 10000) / 10000;  //计算-还款时间
            //计算-还款月数
            var yll_01 = Math.round(((r2 * 0.01) / 12) * 10000) / 10000;  //月利率 = 年利率/12*（贷款月份数）
            var t_01 = Math.pow((1 + yll_01), js_time);
            var js_Lmy_01 = Math.round((js_total_01 * yll_01 * (t_01 / (t_01 - 1))) * 10000) / 10000; //计算-每月还款
            var js_Lzlx_00_01 = (js_time * js_Lmy_01) - js_total_01;  //计算-总支付利息  每月等额还款
            var js_Lzlx_01 = Math.round(js_Lzlx_00_01 * 10000) / 10000;
            var js_Lbxhj_01 = js_Lzlx_01 + Math.round(js_total_01);  //计算-本息合计  每月等额还款
            //等本计算
            var js_mbj_01 = js_total_01 / js_time;  //每月本金 计算
            var js_Rsy_00_01 = js_mbj_01 + (js_total_01 * yll_01); //首月还款 计算
            var js_Rsy_01 = Math.round(js_Rsy_00_01 * 10000) / 10000;
            var js_Rdj_00_01 = (js_mbj_01 + (js_total_01 - js_mbj_01) * yll_01) - (js_mbj_01 + (js_total_01 - js_mbj_01 * 2) * yll_01); //每月递减 计算
            var js_Rdj_01 = Math.round(js_Rdj_00_01 * 10000) / 10000;
            var js_Rzlx_00_01 = js_total_01 * yll_01 * (js_time + 1) / 2; //总利息 计算
            var js_Rzlx_01 = Math.round(js_Rzlx_00_01 * 10000) / 10000;
            var js_Rbxhj_01 = js_Rzlx_01 + Math.round(js_total_01); //本息合计 计算

            /////num-1 over

            //////num-2 start

            //按贷款额度计算
            var js_total_02 = Math.round(r3 * 10000) / 10000;  //计算-公积金贷款金额
            //js_time = Math.round(r0 * 10000) / 10000;  //计算-还款时间
            //计算-还款月数
            var yll_02 = Math.round(((r4 * 0.01) / 12) * 10000) / 10000;  //月利率 = 年利率/12*（贷款月份数）
            var t_02 = Math.pow((1 + yll_02), js_time);
            var js_Lmy_02 = Math.round((js_total_01 * yll_02 * (t_02 / (t_02 - 1))) * 10000) / 10000; //计算-每月还款
            var js_Lzlx_00_02 = (js_time * js_Lmy_02) - js_total_02;  //计算-总支付利息  每月等额还款
            var js_Lzlx_02 = Math.round(js_Lzlx_00_02 * 10000) / 10000;
            var js_Lbxhj_02 = js_Lzlx_02 + Math.round(js_total_02);  //计算-本息合计  每月等额还款
            //等本计算
            var js_mbj_02 = js_total_02 / js_time;  //每月本金 计算
            var js_Rsy_00_02 = js_mbj_02 + (js_total_02 * yll_02); //首月还款 计算
            var js_Rsy_02 = Math.round(js_Rsy_00_02 * 10000) / 10000;
            var js_Rdj_00_02 = (js_mbj_02 + (js_total_02 - js_mbj_02) * yll_02) - (js_mbj_02 + (js_total_02 - js_mbj_02 * 2) * yll_02); //每月递减 计算
            var js_Rdj_02 = Math.round(js_Rdj_00_02 * 10000) / 10000;
            var js_Rzlx_00_02 = js_total_02 * yll_02 * (js_time + 1) / 2; //总利息 计算
            var js_Rzlx_02 = Math.round(js_Rzlx_00_02 * 10000) / 10000;
            var js_Rbxhj_02 = js_Rzlx_02 + Math.round(js_total_02); //本息合计 计算

            /////num-2 over

            js_total = js_total_01 + js_total_02;  //贷款总额
            js_Lmy = js_Lmy_01 + js_Lmy_02;  //每月还款
            js_Lzlx = js_Lzlx_01 + js_Lzlx_02; //总支付利息  每月等额还款
            js_Lbxhj = js_Lbxhj_01 + js_Lbxhj_02; //本息合计  每月等额还款
            js_Rsy = js_Rsy_01 + js_Rsy_02; //首月还款
            js_Rdj = (js_Rdj_01 + js_Rdj_02) * 10000 / 10000; //每月递减
            js_Rzlx = js_Rzlx_01 + js_Rzlx_02; //总利息
            js_Rbxhj = js_Rbxhj_01 + js_Rbxhj_02; //本息合计

            jq('.js_total').text(js_total);  //贷款总额
            jq('.js_time').text(js_time);  //还款时间
            jq('#js_Lmy').text(js_Lmy);  //每月还款
            jq('#js_Lzlx').text(js_Lzlx); //总支付利息  每月等额还款
            jq('#js_Lbxhj').text(js_Lbxhj); //本息合计  每月等额还款
            //逐月递减还款
            jq('#js_Rsy').text(js_Rsy); //首月还款
            jq('#js_Rdj').text(js_Rdj); //每月递减 计算
            jq('#js_Rzlx').text(js_Rzlx); //总利息
            jq('#js_Rbxhj').text(js_Rbxhj); //本息合计
        }
    });
}
//组合贷款计算器 ----------over------------//



//二手房贷款计算器 ----------start------------//
//房屋建筑面积
function esfdyz1(esfd_a) {
    var cipt = jq('#efd_mj') //房屋建筑面积 ipt
    var esfd_a = cipt.val();
    //房屋建筑面积验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义房屋建筑面积 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            efd_mj = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                efd_mj = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                efd_mj = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return esfd_a;
}

//房屋建筑面积
function esfdyz2(esfd_b) {
    var cipt = jq('#efd_zj') //房屋建筑面积 ipt
    var esfd_b = cipt.val();
    //房屋建筑面积验证
    cipt.blur(function() {
        var Cprice_val = cipt.val(); //定义房屋建筑面积 val
        var Tprice_val = jq.trim(Cprice_val); //定义购车价格 去空格后的val
        //var error_ts = jq('.js_price_error'); //定义提示错误 
        var error_ts = jq(this).parent().parent().children('.js_error'); //定义提示错误 
        //以下为验证输入的值
        if (Tprice_val == "") {
            error_ts.removeClass('none');
            error_ts.text('不能为空');
            efd_zj = false;
        } else if (Tprice_val.length > 0) {
            if (!isNaN(Tprice_val)) {
                error_ts.addClass('none');
                cipt.addClass('success_ts');
                efd_zj = true;
            } else {
                error_ts.removeClass('none');
                error_ts.text('少年，这不是一个数字');
                cipt.addClass('error_ts');
                efd_zj = false;
            }
        }
    });
    cipt.focus(function() { //获取焦点后 对字的颜色进行改变
        cipt.removeClass('success_ts');
        cipt.removeClass('error_ts');
    });
    return esfd_b;
}

function esfdjs(esfd_a, esfd_b) {
    var efdbtn = jq('#efd_jsbtn');  //计算 btn
    efdbtn.click(function() {
        var js_hj01 = jq('#js_hj01'); //合计
        var js_qs01 = jq('#js_qs01'); //契税
        var js_yys01 = jq('#js_yys01'); //营业税
        var js_cjs01 = jq('#js_cjs01'); //城建税
        var js_jyfjs01 = jq('#js_jyfjs01'); //教育附加费
        var js_gs01 = jq('#js_gs01'); //个人所得税
        var js_yhs01 = jq('#js_yhs01'); //印花税

        var r1 = esfdyz1(esfd_a); //房屋建筑面积  将字符转成数字格式-输入值
        var r2 = esfdyz2(esfd_b); //房屋总价  将字符转成数字格式-输入值
        
        //计算开始
        if (efd_mj == true && efd_zj == true) {
            if (jq('#jsfwxz_0').attr('checked') == 'checked') {
                if (r1 < 90) {
                    js_qs01 = Math.round(r2 * 0.01 * 10000 * 10000) / 10000;
                }
                else {
                    js_qs01 = Math.round(r2 * 0.015 * 10000 * 10000) / 10000;
                } //契税 --计算1
            } else if (jq('#jsfwxz_1').attr('checked') == 'checked') {
                js_qs01 = Math.round(r2 * 0.03 * 10000 * 10000) / 10000; //契税 --计算2
            }
            if (jq('#jshas5_0').attr('checked') == 'checked') {
                js_yys01 = 0;
                js_cjs01 = 0;
                js_jyfjs01 = 0;
                js_gs01 = 0;
                //js_gs01 = 0;
                js_yhs01 = 0;
            }
            if (jq('#jshas5_1').attr('checked') == 'checked') {
                js_yys01 = Math.round(r2 * 0.0555 * 10000 * 10000) / 10000; //营业税 --计算
                js_cjs01 = Math.round(js_yys01 * 0.07 * 100000) / 100000; //城建税 --计算
                js_jyfjs01 = Math.round(js_yys01 * 0.03 * 100000) / 100000; //教育附加费 --计算
                js_gs01 = Math.round(r2 * 0.01 * 10000 * 10000) / 10000; //个人所得税 --计算
                js_yhs01 = 0; //印花税 --计算
            }
            js_hj01 = js_qs01 + js_yys01 + js_cjs01 + js_jyfjs01 + js_gs01; //合计 --计算
            //输出计算结果
            jq('#js_hj01').text(js_hj01);  //合计
            jq('#js_qs01').text(js_qs01);  //契税
            jq('#js_yys01').text(js_yys01);  //营业税
            jq('#js_cjs01').text(js_cjs01);  //城建税
            jq('#js_jyfjs01').text(js_jyfjs01);  //教育附加费
            jq('#js_gs01').text(js_gs01);  //个人所得税
            jq('#js_yhs01').text(js_yhs01);  //印花税
        }

    });
}
//二手房贷款计算器 ----------over------------//