<?php

/**
 *		[Discuz!] (C)2001-2099 Comsenz Inc.
 *		This is NOT a freeware, use is subject to license terms
 *
 *		$Id: Security.php 33923 2013-09-03 02:59:43Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Cloud::loadFile('Service_Server_Restful');

class Cloud_Service_Server_Security extends Cloud_Service_Server_Restful {

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance() {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function onSecuritySetEvilPost($data) {

		$results = array();
		foreach ($data as $evilPost) {

			$results[] = $this->_handleEvilPost($evilPost['tid'], $evilPost['pid'], $evilPost['evilType'], $evilPost['evilLevel']);
		}

		return $results;
	}

	public function onSecuritySetEvilUser($data, $days = 1) {
		$results = array();

		foreach ($data as $evilUser) {
			$results[] = $this->_handleEvilUser($evilUser['uid'], $evilUser['evilType'], $evilUser['evilLevel'], $days);
		}
		return $results;
	}

	/**
	 * handleEvilPost
	 *
	 * @param integer $tid 主题ID
	 * @param integer $pid 回复ID
	 * @param integer $evilType 恶意类型
	 * @param integer $evilLevel 恶意级别
	 */
	protected function _handleEvilPost($tid, $pid, $evilType, $evilLevel = 1) {

		// 语言势
		include_once DISCUZ_ROOT.'./source/language/lang_admincp_cloud.php';

		$securityService = Cloud::loadClass('Service_Security');
		$securityService->writeLog($pid, 'pid');

		$evilPost = C::t('#security#security_evilpost')->fetch($pid);

		if (count($evilPost)) {
			return true;
		} else {
			require_once libfile('function/delete');
			require_once libfile('function/forum');
			require_once libfile('function/post');

			$data = array('pid' => $pid, 'tid' => $tid, 'evilcount' => 1, 'eviltype' => $evilType, 'createtime' => TIMESTAMP);
			$post = get_post_by_pid($pid);

			if (is_array($post) && count($post) > 0) {
				// tid ?pid 不配对的情况
				if ($tid != $post['tid']) {
					return false;
				}

				$thread = get_thread_by_tid($tid);

				if ($post['first']) {
					$data['type'] = 1;
					// 判断主题过滤条件
					if ($this->_checkThreadIgnore($tid)) {
						return false;
					}
					// 恶意主题处理
					// 先插入恶意表，再删除帖子，为了能够进行删帖上房
					// DB::insert('security_evilpost', $data, 0, 1);
					C::t('#security#security_evilpost')->insert($data, false, true);
					$this->_updateEvilCount('thread');
					// DB::query("UPDATE ".DB::table('forum_thread')." SET displayorder='-1', digest='0', moderated='1' WHERE tid = '".$tid."'");
					//C::t('forum_thread')->update($tid, array('displayorder' => -1, 'digest' => 0, 'moderated' => 1), TRUE);
					deletethread(array($tid), true, true, true);
					// updatepost(array('invisible' => '-1'), "tid = '".$tid."'");
					//loadcache('posttableids');
					//$posttableids = $_G['cache']['posttableids'] ? $_G['cache']['posttableids'] : array('0');
					//foreach($posttableids as $id) {
					//	C::t('forum_post')->update_by_tid($id, $tidsarr, array('invisible' => '-1'));
					//}
					// 主题操作日志
					updatemodlog($tid, 'DEL', 0, 1, $extend_lang['security_modreason']);
				} else {
					$data['type'] = 0;
					// 判断回复过滤条件
					if ($this->_checkPostIgnore($pid, $post)) {
						return false;
					}
					// DB::insert('security_evilpost', $data, 0, 1);
					C::t('#security#security_evilpost')->insert($data, false, true);
					$this->_updateEvilCount('post');
					// 恶意帖子处理

					deletepost(array($pid), 'pid', true, false, true);
				}
				//note 将UID记录到恶意用户表
				if(!empty($post['authorid'])) {
					$data = array('uid' => $post['authorid'], 'createtime' => TIMESTAMP);
					C::t('#security#security_eviluser')->insert($data, false, true);
				}
			} else {
				$data['operateresult'] = 2;
				// DB::insert('security_evilpost', $data, 0, 1);
				C::t('#security#security_evilpost')->insert($data, false, true);
			}
			if($evilLevel >= 5) {
				$user = C::t('common_member')->fetch($post['authorid'], 0, 1);
				$this->_handleBandUser($user, 1);
			}
		}

		return true;
	}
	
	/**
	 * _handleBandUser
	 *
	 * @param integer $user 用户
	 * @param integer $days 禁言天数
	 * @return mixed
	 */
	protected function _handleBandUser($user, $days = 1) {
		$uid = $user['uid'];
		if($this->_checkUserIgnore($uid)) {
			return false;
		}
		require_once libfile('function/forum');
		$setarr = array('groupid' => 4);
		if($days) {
			$days = !empty($days) ? TIMESTAMP + $days * 86400 : 0;
			$days = $days > TIMESTAMP ? $days : 0;
			if($days) {
				$user['groupterms']['main'] = array('time' => $days, 'adminid' => $user['adminid'], 'groupid' => $user['groupid']);
				$user['groupterms']['ext'][4] = $days;
				C::t('common_member_field_forum')->update($uid, array('groupterms' => serialize($user['groupterms'])));
				$setarr['groupexpiry'] = groupexpiry($user['groupterms']);
			} else {
				$setarr['groupexpiry'] = 0;
			}
		}

		require_once libfile('function/misc');
		return C::t('common_member')->update($uid, $setarr);
	}

	/**
	 * _handleEvilUser
	 *
	 * @param integer $uid 用户ID
	 * @param integer $evilType 恶意类型
	 * @param integer $evilLevel 恶意级别
	 * @param integer $days 屏蔽天数
	 * @return mixed
	 */
	protected function _handleEvilUser($uid, $evilType, $evilLevel = 1, $days = 1) {
		global $_G;

		// 语言势
		include_once DISCUZ_ROOT.'./source/language/lang_admincp_cloud.php';

		// debug时写日志
		$securityService = Cloud::loadClass('Service_Security');
		$securityService->writeLog($uid, 'uid');
		
		// 用户是管理员繿
		if($this->_checkUserIgnore($uid)) {
			return false;
		}
		$user = C::t('common_member')->fetch($uid, 0, 1);
		
		if(is_array($user)) {				
			$update = $this->_handleBandUser($user, $days);
			if ($update) {
				// 禁止用户的操作潍
				$_G['member']['username'] = 'SYSTEM';
				savebanlog($user['username'], $user['groupid'], 4, 0, $extend_lang['security_modreason']);
			}
		} 

		$evilUser = C::t('#security#security_eviluser')->fetch($uid);

		if (count($evilUser)) {
			return true;
		} else {
			// 如果不是第一次返回该恶意用户，增加统謿
			$data = array('uid' => $uid, 'evilcount' => 1, 'eviltype' => $evilType, 'createtime' => TIMESTAMP);			
			
			// DB::insert('security_eviluser', $data, 0, 1);
			C::t('#security#security_eviluser')->insert($data, false, true);
			$this->_updateEvilCount('member');			
		}

		return true;
	}

	/*
	 * @ _checkThreadIgnore 判断主题是否隿|?过滤
	 * @param $post
	 * @returns
	 */
	protected function _checkThreadIgnore($tid) {

		// 隿|?桿μ?的字欿
		if (!intval($tid)) {
			return true;
		}
		require_once libfile('function/forum');
		$checkFiled = array('highlight', 'displayorder', 'digest');
		$thread = get_thread_by_tid($tid);
		$checkResult = false;
		// 先过滤用憿
		$checkResult = $this->_checkBoardIgnore($thread['fid']);
		$checkResult = $checkResult ? true : $this->_checkUserIgnore($thread['authorid']);
		foreach ($checkFiled as $field) {
			if ($thread[$field] > 0) {
				$checkResult = true;
			};
		}

		return $checkResult;
	}

	/*
	 * @brief _updateEvilCount 更新恶意统计擿
	 * @param $type thread post member
	 * @returns
	 */
	protected function _updateEvilCount($type) {

		if (empty($type)) {
			return false;
		}

		$settingKey = 'cloud_security_stats_' . $type;
		$count = intval(C::t('common_setting')->fetch($settingKey));
		C::t('common_setting')->update($settingKey, $count + 1);

	}

	/*
	 * @brief _checkPostIgnore 判断回复的过滤条乿
	 * @param $pid
	 * @returns
	 */
	protected function _checkPostIgnore($pid, $post) {
		if (!intval($pid)) {
			return true;
		}
		$checkResult = false;
		$checkResult = $this->_checkBoardIgnore($post['fid']);
		$checkResult = $checkResult ? true : $this->_checkUserIgnore($post['authorid']);

		// $postStick = DB::result_first("SELECT count(*) FROM " . DB::table('forum_poststick') . " WHERE pid = '$pid'");
		$postStick = C::t('forum_poststick')->count_by_pid($pid);
		// $checkResult = $postStick ? true : false;
		if ($checkResult || $postStick) {
			$checkResult = true;
		}

		return $checkResult;
	}

	/*
	 * @brief _checkBoardIgnore 判断版块的过滤条乿
	 * @param $pid
	 * @returns
	 */
	protected function _checkBoardIgnore($fid) {
		global $_G;
		$checkResult = false;

		$whiteList = $_G['setting']['security_forums_white_list'];
		$whiteList = is_array($whiteList) ? $whiteList : array();
		if (in_array($fid, $whiteList)) {
			$checkResult = true;
		}
		return $checkResult;
	}

	/*
	 * @brief _checkUserIgnore 判断用户是否在忽略范围内
	 * @param $uid
	 * @returns
	 */
	protected function _checkUserIgnore($uid) {
		global $_G;
		if (!intval($uid)) {
			return true;
		}
		$whiteList = $_G['setting']['security_usergroups_white_list'];
		$whiteList = is_array($whiteList) ? $whiteList : array();

		$memberInfo = C::t('common_member')->fetch($uid, 0, 1);
		$checkResult = false;
		// 纯设置，无默认白名单
		if (in_array($memberInfo['groupid'], $whiteList)) {
			$checkResult = true;
		}

		return $checkResult;
	}
}
