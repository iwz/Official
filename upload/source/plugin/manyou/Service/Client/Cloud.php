<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: Cloud.php 33755 2013-08-10 03:21:02Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Cloud::loadFile('Service_Client_Restful');

class Cloud_Service_Client_Cloud extends Cloud_Service_Client_Restful {

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance($debug = false) {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self($debug);
		}

		return self::$_instance;
	}

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct($debug = false) {

		return parent::__construct($debug);
	}

	public function appOpen() {
		return $this->_callMethod('site.appOpen', array('sId' => $this->_sId, 'appIdentifier' => 'search'));
	}
	
	public function appOpenWithRegister($appIdentifier, $extra = array()) {
		$this->reloadId();
		return $this->_callMethod('site.appOpen', array(
			'sId' => $this->_sId,
			'appIdentifier' => $appIdentifier,
			'sName' => $this->siteName,
			'sSiteKey' => $this->uniqueId,
			'sUrl' => $this->siteUrl,
			'sCharset' => $this->charset,
			'sTimeZone' => $this->timeZone,
			'sUCenterUrl' => $this->UCenterUrl,
			'sLanguage' => $this->language,
			'sProductType' => $this->productType,
			'sProductVersion' => $this->productVersion,
			'sTimestamp' => $this->_ts,
			'sApiVersion' => $this->apiVersion,
			'sSiteUid' => $this->siteUid,
			'sProductRelease' => $this->productRelease,
			'extra' => $extra,
		), false, true);
	}
	
	public function appOpenFormView($appIdentifier, $submitUrl, $fromUrl) {
		return $this->_callMethod('site.getAppOpenFormView', array(
			'sId' => $this->_sId,
			'sCharset' => $this->charset,
			'appIdentifier' => $appIdentifier,
			'submitUrl' => $submitUrl,
			'fromUrl' => $fromUrl,
			'type' => 'open',
		));
	}
	
	public function appClose($appIdentifier) {
		return $this->_callMethod('site.appClose', array(
			'sId' => $this->_sId,
			'appIdentifier' => $appIdentifier,
			'sSiteUid' => $this->siteUid,
		), false, true);
	}
	
	public function appCloseReasonsView($appIdentifier, $submitUrl, $fromUrl) {
		return $this->_callMethod('site.getAppCloseReasonsView', array(
			'sId' => $this->_sId,
			'appIdentifier' => $appIdentifier,
			'submitUrl' => $submitUrl,
			'fromUrl' => $fromUrl,
		));
	}
	
	private function reloadId() {
		global $_G;
		loadcache('setting', 1);
		$this->_sId = !empty($_G['setting']['my_siteid']) ? $_G['setting']['my_siteid'] : '';
		$this->_sKey = !empty($_G['setting']['my_sitekey']) ? $_G['setting']['my_sitekey'] : '';
	}

	public function bindQQ($appIdentifier, $fromUrl, $extra = array()) {
		$this->reloadId();
		$utilService = Cloud::loadClass('Service_Util');
		$fromUrl .= $extra ? '&'.$utilService->httpBuildQuery(array('extra' => $extra), '', '&') : '';
		$params = array(
			's_id' => $this->_sId,
			'app_identifier' => $appIdentifier,
			's_site_uid' => $this->siteUid,
			'from_url' => $fromUrl,
			'ADTAG' => 'CP.CLOUD.BIND.INDEX',
		);
		ksort($params);
		$str = $utilService->httpBuildQuery($params, '', '&');
		$params['sig'] = md5(sprintf('%s|%s|%s', $str, $this->_sKey, $this->_ts));
		$params['ts'] = $this->_ts;		
		return 'http://cp.discuz.qq.com/addon_bind/index?'.$utilService->httpBuildQuery($params, '', '&');
	}

	/**
	 * Discuz! 调用平台接口，注册站点信息
	 *
	 * @param string $sName 站点名称
	 * @param string $sSiteKey 产品 Key (站点唯一 ID)
	 * @param string $sUrl 站点url 必须以 http://开头.
	 * @param string $sCharset 站点使用字符集 utf8 gbk big5
	 * @param string $sTimeZone 站点使用时区
	 * @param string $sUCenterUrl 站点 UCenter 地址
	 * @param string $sLanguage 站点使用语言
	 * @param string $sProductType (Discuz!, Discuz!X ...) 产品类型
	 * @param string $sProductVersion (7.2, 1.5 ...) 产品版本
	 * @param integer $sTimestamp 站点服务器时间
	 * @param string $sApiVersion 站点云平台客户端版本
	 * @param integer $sSiteUid 站长在站点上的Uid
	 *
	 * @throws Cloud_Site_Exception
	 *  + 1 发生了一个未知错误，请重新提交请求。
	 *  + 2 目前服务不可用。
	 *  + 3 IP 地址被禁止。
	 *  + 100 无效的参数
	 *  + 101 无效的 API Key。
	 *  + 102 无效的签名值
	 *
	 *  + 111 xxx参数格式不合法.
	 *  + 112 站点 Url 重复
	 *  + 114 站点产品 Key 重复
	 *
	 * @return array 返回站点 ID 和站点 Key
	 *  + sId 站点 ID
	 *  + sKey 站点通信 Key
	 *
	 * @source
	 * <code>
	 *
	 * $cloudClient = new Cloud_Client();
	 * $cloudInfo = $cloudClient->register($sName, $sSiteKey, $sUrl, $sCharset,
	 *                                     $sTimeZone, $sUCenterUrl, $sLanguage,
	 *                                     $sProductType, $sProductVersion
	 *                                     $sTimestamp, $sApiVersion, $sSiteUid);
	 * echo $cloudInfo['sId'];
	 * echo $cloudInfo['sKey'];
	 *
	 * </code>
	 *
	 */
	public function register() {

		return $this->_callMethod('site.register', array(
														 'sName' => $this->siteName,
														 'sSiteKey' => $this->uniqueId,
														 'sUrl' => $this->siteUrl,
														 'sCharset' => $this->charset,
														 'sTimeZone' => $this->timeZone,
														 'sUCenterUrl' => $this->UCenterUrl,
														 'sLanguage' => $this->language,
														 'sProductType' => $this->productType,
														 'sProductVersion' => $this->productVersion,
														 'sTimestamp' => $this->_ts,
														 'sApiVersion' => $this->apiVersion,
														 'sSiteUid' => $this->siteUid,
														 'sProductRelease' => $this->productRelease,
												   )
								  );
	}

	/**
	 * Discuz!调用平台接口同步站点信息
	 *
	 * @param string $sName 站点名称
	 * @param string $sSiteKey 产品 Key (站点唯一ID)
	 * @param string $sUrl 站点 Url 必须以 http:// 开头.
	 * @param string $sCharset 站点使用字符集 utf8 gbk big5
	 * @param string $sTimeZone 站点使用时区
	 * @param string $sUCenterUrl 站点 UCenter 地址
	 * @param string $sLanguage 站点使用语言
	 * @param string $sProductType (Discuz!, Discuz!X ...) 产品类型
	 * @param string $sProductVersion (7.2, 1.5 ...) 产品版本
	 * @param integer $sTimestamp 站点服务器时间
	 * @param string $sApiVersion 站点云平台客户端版本
	 * @param integer $sSiteUid 站长在站点上的Uid
	 *
	 * @throws Cloud_Site_Exception
	 *  + 1 发生了一个未知错误，请重新提交请求。
	 *  + 2 目前服务不可用。
	 *  + 3 IP 地址被禁止。
	 *  + 100 无效的参数
	 *  + 101 无效的 API Key。
	 *  + 102 无效的签名值
	 *
	 *  + 111 xxx参数格式不合法
	 *  + 121 当前站点不存在
	 *
	 * @return boolean true | false 更新成功|失败
	 *
	 * @source
	 * <code>
	 *
	 * $cloudClient = new Cloud_Client($_sId, $sKey);
	 * $cloudSync = $cloudClient->sync($sName, $sSiteKey, $sUrl, $sCharset,
	 *                                 $sTimeZone, $sUCenterUrl, $sLanguage,
	 *                                 $sProductType, $sProductVersion
	 *                                 $sTimestamp, $sApiVersion, $sSiteUid);
	 * if($cloudSync) {
	 *  // do something
	 * }
	 *
	 * </code>
	 */
	public function sync() {
		return $this->_callMethod('site.sync', array(
													 'sId' => $this->_sId,
													 'sName' => $this->siteName,
													 'sSiteKey' => $this->uniqueId,
													 'sUrl' => $this->siteUrl,
													 'sCharset' => $this->charset,
													 'sTimeZone' => $this->timeZone,
													 'sUCenterUrl' => $this->UCenterUrl,
													 'sLanguage' => $this->language,
													 'sProductType' => $this->productType,
													 'sProductVersion' => $this->productVersion,
													 'sTimestamp' => $this->_ts,
													 'sApiVersion' => $this->apiVersion,
													 'sSiteUid' => $this->siteUid,
													 'sProductRelease' => $this->productRelease
													 )
								  );
	}

	/**
	 * Discuz! 调用平台接口重新生成站点 ID 和站点 Key
	 *
	 * @throws Cloud_Site_Exception
	 *  + 1 发生了一个未知错误，请重新提交请求。
	 *  + 2 目前服务不可用。
	 *  + 3 IP 地址被禁止。
	 *  + 100 无效的参数
	 *  + 101 无效的 API Key。
	 *  + 102 无效的签名值
	 *
	 *  + 111 xxx参数格式不合法.
	 *  + 121 当前站点不存在
	 *
	 * @return array 返回站点 ID 和站点通信 Key
	 *  + sId 站点 ID
	 *  + sKey 站点通信 Key
	 *
	 * @source
	 * <code>
	 *
	 * $cloudClient = new Cloud_Client($_sId, $sKey);
	 * $cloudReset = $cloudClient->resetKey();
	 * echo $cloudReset['sId'];
	 * echo $cloudReset['sKey'];
	 *
	 * </code>
	 */
	public function resetKey() {

		return $this->_callMethod('site.resetKey', array('sId' => $this->_sId));
	}

	public function resume() {

		// 修复工具使用 UTF-8 编码
		return $this->_callMethod('site.resume', array(
																			   'sUrl' => $this->siteUrl,
																			   'sCharset' => 'UTF-8',
																			   'sProductType' => $this->productType,
																			   'sProductVersion' => $this->productVersion
																			   )
												 );
	}

	public function registerCloud($cloudApiIp = '') {

		try {

			$returnData = $this->register();

		} catch (Cloud_Service_Client_RestfulException $e) {

			// 第一次出异常，有IP就重试一次，没有IP提示DNS出错
			if ($e->getCode() == 1 && $cloudApiIp) {

				$this->setCloudApiIp($cloudApiIp);

				try {

					$returnData = $this->register();
					// 成功则更新数据库中的API地址
					C::t('common_setting')->update('cloud_api_ip', $cloudApiIp);

				} catch (Cloud_Service_Client_RestfulException $e) {

					// 第二次失败向上抛异常
					throw new Cloud_Service_Client_RestfulException($e);
				}
			} else {
					// 错误号不是1也直接往上抛异常
					throw new Cloud_Service_Client_RestfulException($e);
			}
		}

		$sId = intval($returnData['sId']);
		$sKey = trim($returnData['sKey']);

		if ($sId && $sKey) {
			C::t('common_setting')->update_batch(array('my_siteid' => $sId, 'my_sitekey' =>$sKey ,'cloud_status' => '2'));
			updatecache('setting');
		} else {
			// 云平台返回结果不正确
			throw new Cloud_Service_Client_RestfulException('Error Response.', 2);
		}

		return true;
	}


	public function upgradeManyou($cloudApiIp = '') {

		try {

			$returnData = $this->sync();

		} catch (Cloud_Service_Client_RestfulException $e) {

			// 第一次出异常，有IP就重试一次，没有IP提示DNS出错
			if ($e->getCode() == 1 && $cloudApiIp) {

				$this->setCloudApiIp($cloudApiIp);

				try {

					$returnData = $this->sync();
					// 成功则更新数据库中的API地址
					C::t('common_setting')->update('cloud_api_ip', $cloudApiIp);

				} catch (Cloud_Service_Client_RestfulException $e) {

					// 第二次失败向上抛异常
					throw new Cloud_Service_Client_RestfulException($e);
				}
			}

		}

		return true;
	}

}