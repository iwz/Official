<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: Restful.php 33750 2013-08-09 09:56:01Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Cloud::loadFile('Service_Client_RestfulException');

/**
 * 云平台接口客户端类，Discuz! 通过此类中方法来调用平台接口
 */
abstract class Cloud_Service_Client_Restful {

	protected $_cloudApiIp = '';

	protected $_sId = 0;

	protected $_sKey = '';

	protected $_url = 'http://api.discuz.qq.com/site.php';

	protected $_format = 'PHP';

	protected $_ts = 0;

	protected $_debug = false;

	protected $_batchParams = array();

	public $errorCode = 0;

	public $errorMessage = '';

	public $my_status = false;
	public $cloud_status = false;

	public $siteName = '';
	public $uniqueId = '';
	public $siteUrl = '';
	public $charset = '';
	public $timeZone = 0;
	public $UCenterUrl = '';
	public $language = '';
	public $productType = '';
	public $productVersion = '';
	public $productRelease = '';
	public $apiVersion = '';
	public $siteUid = 0;

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance($debug = false) {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self($debug);
		}

		return self::$_instance;
	}

	/**
	 * Discuz! 云平台客户端构造函数
	 */
	public function __construct($debug = false) {

		$this->_debug = $debug;

		$this->_initSiteEnv();
	}

	/**
	 * 初始化站点变量
	 */
	protected function _initSiteEnv() {

		global $_G;

		require_once DISCUZ_ROOT.'./source/discuz_version.php';

		$this->my_status = !empty($_G['setting']['my_app_status']) ? $_G['setting']['my_app_status'] : '';
		$this->cloud_status = !empty($_G['setting']['cloud_status']) ? $_G['setting']['cloud_status'] : '';

		$this->_sId = !empty($_G['setting']['my_siteid']) ? $_G['setting']['my_siteid'] : '';
		$this->_sKey = !empty($_G['setting']['my_sitekey']) ? $_G['setting']['my_sitekey'] : '';
		$this->_ts = TIMESTAMP;

		$this->siteName = !empty($_G['setting']['bbname']) ? $_G['setting']['bbname'] : '';

		$this->uniqueId = $_G['setting']['siteuniqueid'];
		$this->siteUrl = $_G['siteurl'];
		$this->charset = CHARSET;
		$this->timeZone = !empty($_G['setting']['timeoffset']) ? $_G['setting']['timeoffset'] : '';
		$this->UCenterUrl = !empty($_G['setting']['ucenterurl']) ? $_G['setting']['ucenterurl'] : '';
		$this->language = $_G['config']['output']['language'] ? $_G['config']['output']['language'] : 'zh_CN';
		$this->productType = 'DISCUZX';
		$this->productVersion = defined('DISCUZ_VERSION') ? DISCUZ_VERSION : '';
		$this->productRelease = defined('DISCUZ_RELEASE') ? DISCUZ_RELEASE : '';

		$utilService = Cloud::loadClass('Service_Util');
		$this->apiVersion = $utilService->getApiVersion();

		$this->siteUid = $_G['uid'];

		if ($_G['setting']['cloud_api_ip']) {
			$this->setCloudApiIp($_G['setting']['cloud_api_ip']);
		}

	}

	/**
	 * _callMethod
	 * 请求方法
	 *
	 * @ignore
	 * @param  string $method
	 * @param  array $args
	 * @param  boolean 是否为批量请求
	 * @param  boolean 直接返回请求结果
	 * @return void
	 */
	protected function _callMethod($method, $args, $isBatch = false, $return = false) {
		// $log = array($method, $args);
		// file_put_contents(DISCUZ_ROOT . './data/manyou.log', var_export($log, true), 8);
		$this->errorCode = 0;
		$this->errorMessage = '';
		$url = $this->_url;
		$avgDomain = explode('.', $method);
		switch ($avgDomain[0]) {
			case 'site':
				$url = 'http://api.discuz.qq.com/site_cloud.php';
				break;
			case 'qqgroup':
				$url = 'http://api.discuz.qq.com/site_qqgroup.php';
				break;
			case 'connect':
				$url = 'http://api.discuz.qq.com/site_connect.php';
				break;
			case 'security':
				$url = 'http://api.discuz.qq.com/site_security.php';
				break;
			default:
				$url = $this->_url;
		}
		$params = array();
		$params['sId'] = $this->_sId;
		$params['method'] = $method;
		$params['format'] = strtoupper($this->_format);

		$params['sig'] = $this->_generateSig($params, $method, $args);
		$params['ts'] = $this->_ts;

		$postData = $this->_createPostData($params, $args);

		if ($isBatch) {
			// 批量接口并不实时返回数据
			$this->_batchParams[] = $postData;

			return true;
		} else {

			$utilService = Cloud::loadClass('Service_Util');
			$postString = $utilService->httpBuildQuery($postData, '', '&');

			$result = $this->_postRequest($url, $postString);
			if ($this->_debug) {
				$this->_message('receive data ' . dhtmlspecialchars($result) . "\n\n");
			}

			if(!$return) {
				return $this->_parseResponse($result, false, $return);
			} else {
				$response = @dunserialize($result);
				if(!is_array($response)) {
					return $result;
				} else {
					return $response;
				}
			}
		}
	}

	/**
	 * _parseResponse
	 * 解析云平台回包响应值
	 *
	 * @ignore
	 * @param  string $result 云平台的响应包内容
	 * @param  boolean 是否为批量请求
	 * @return mixed 对应方法的返回值
	 */
	protected function _parseResponse($response, $isBatch = false) {

		if (!$response) {
			// 未知异常
			$this->_unknowErrorMessage();
		}

		$response = @dunserialize($response);
		if (!is_array($response)) {
			// 未知异常
			$this->_unknowErrorMessage();
		}
		if ($response['errCode']) {
			// 接口返回错误异常
			$this->errorCode = $response['errCode'];
			$this->errorMessage = $response['errMessage'];
			throw new Cloud_Service_Client_RestfulException($response['errMessage'], $response['errCode']);
		}
		if (!isset($response['result']) && !isset($response['batchResult'])) {
			// 未知异常
			$this->_unknowErrorMessage();
		}

		if ($isBatch) {
			return $response['batchResult'];
		} else {
			return $response['result'];
		}

	}

	/**
	 * runBatchMethod
	 * 执行批量方法请求
	 *
	 * @ignore
	 * @return void
	 */
	public function runBatchMethod() {

		if (!$this->_batchParams) {
			return false;
		}

		$postData = array('batchParams' => $this->_batchParams);
		$utilService = Cloud::loadClass('Service_Util');
		$postString = $utilService->httpBuildQuery($postData, '', '&');

		$result = $this->_postRequest($this->_url, $postString);
		if ($this->_debug) {
			$this->_message('receive data ' . dhtmlspecialchars($result) . "\n\n");
		}

		return $this->_parseResponse($result, true);
	}

	/**
	 * _unknowErrorMessage
	 * 未知错误
	 *
	 * @return void
	 */
	protected function _unknowErrorMessage() {
		$this->errorCode = 1;
		$this->errorMessage = 'An unknown error occurred. May be DNS Error. ';

		throw new Cloud_Service_Client_RestfulException($this->errorMessage, $this->errorCode);
	}

	/**
	 * _generateSig
	 * sig生成方法
	 *
	 * @ignore
	 * @param  mixed $params
	 * @param  mixed $method
	 * @param  mixed $args
	 * @return void
	 */
	protected function _generateSig($params, $method, $args) {

		$postData = $this->_createPostData($params, $args);

		$utilService = Cloud::loadClass('Service_Util');
		$postString = $utilService->httpBuildQuery($postData, '', '&');

		if ($this->_debug) {
			$this->_message('sig string: ' . $postString . '|' . $this->_sKey . '|' . $this->_ts . "\n\n");
		}

		return md5(sprintf('%s|%s|%s', $postString, $this->_sKey, $this->_ts));
	}

	/**
	 * _createPostData
	 * 生成post请求数组
	 *
	 * @ignore
	 * @param  mixed $params
	 * @param  mixed $args
	 * @return void
	 */
	protected function _createPostData($params, $args) {

		// 对 params 和 args 分别排序后，args拼接在params后
		ksort($params);
		ksort($args);

		$params['args'] = $args;

		return $params;
	}

	/**
	 * _postRequest
	 *
	 * @ignore
	 * @param  string $url
	 * @param  string $data
	 * @return result
	 */
	protected function _postRequest($url, $data, $ip = '') {
		if ($this->_debug) {
			$this->_message('post params: ' . $data. "\n\n");
		}

		$ip = $this->_cloudApiIp;

		$result = $this->_fsockopen($url, 0, $data, '', false, $ip, 5);
		return $result;
	}

	/**
	 * _fsockopen
	 * @ignore
	 * @params string $url URL
	 * @params integer $limit 返回数据长度限制
	 * @params string $post POST内容
	 * @params string $cookie cookie内容
	 * @params string $ip HOST对应的IP地址
	 * @params integer $timeout 超时时间
	 * @params boolean $block 模块方式
	 *
	 */
	function _fsockopen($url, $limit = 0, $post = '', $cookie = '', $bysocket = false, $ip = '', $timeout = 15, $block = true) {
		return dfsockopen($url, $limit, $post, $cookie, $bysocket, $ip, $timeout, $block);
	}

	/**
	 * _message
	 *
	 * @ignore
	 * @param  mixed $msg
	 * @return void
	 */
	protected function _message($msg) {
		echo $msg;
	}

	/**
	 * setCloudApiIp
	 *
	 * 设置云平台接口IP
	 */
	public function setCloudApiIp($ip) {

		$this->_cloudApiIp = $ip;

		return true;
	}

	/**
	 * 获取用户openId
	 * @param Integer $uid: 用户uid
	 * @return string 用户的openId
	 */
	protected function getUserOpenId($uid) {
		$openId = '';
		try {
			$connectInfo = C::t('#qqconnect#common_member_connect')->fetch($uid);
			if($connectInfo) {
				$openId = $connectInfo['conopenid'];
			}
		} catch (Exception $e) {}
		return $openId;
	}
	/**
	 * 根据UID获取用户手机串码
	 * @param Integer $uid: 用户uid
	 * @return array 手机串码列表
	 */
	protected function getUserDeviceToken($uids) {
		$uids = (array)$uids;
		$deviceToken = array();
		try {
			$query = DB::query('SELECT * FROM '.DB::table('common_devicetoken').' WHERE uid IN('.dimplode($uids).')', array(), true);
			while($value = DB::fetch($query)) {
				$deviceToken[$value['uid']][] = $value['token'];
			}
		} catch (Exception $e) {}
		return $deviceToken;
	}

}
