<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_cleanup_daily.php 33675 2013-08-01 02:09:09Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
//note 更新推荐群组缓存
require_once libfile('function/cache');
updatecache('forumrecommend');

C::t('common_task')->update_available();

//DB::query("UPDATE ".DB::table('common_advertisement')." SET available='0' WHERE endtime>'0' AND endtime<='$_G[timestamp]'", 'UNBUFFERED');
if(C::t('common_advertisement')->close_endtime()) {
	updatecache(array('setting', 'advs'));
}
//清理7天前的记录
C::t('forum_threaddisablepos')->truncate();
//DB::query("TRUNCATE ".DB::table('common_searchindex'));
C::t('common_searchindex')->truncate();
//DB::query("DELETE FROM ".DB::table('forum_threadmod')." WHERE tid>0 AND dateline<'$_G[timestamp]'-31536000", 'UNBUFFERED');
C::t('forum_threadmod')->delete_by_dateline($_G['timestamp']-31536000);
//DB::query("DELETE FROM ".DB::table('forum_forumrecommend')." WHERE expiration>0 AND expiration<'$_G[timestamp]'", 'UNBUFFERED');
C::t('forum_forumrecommend')->delete_old();
//DB::query("DELETE FROM ".DB::table('home_visitor')." WHERE uid>0 AND dateline<'$_G[timestamp]'-7776000", 'UNBUFFERED');
//删除三个月之前的记录
C::t('home_visitor')->delete_by_dateline($_G['timestamp']-7776000);
//DB::query("DELETE FROM ".DB::table('home_notification')." WHERE uid>0 AND new=0 AND dateline<'$_G[timestamp]'-172800", 'UNBUFFERED');
//删除昨天的postcache记录
C::t('forum_postcache')->delete_by_dateline(TIMESTAMP-86400);
//清除15天前的新主题记录
C::t('forum_newthread')->delete_by_dateline(TIMESTAMP-1296000);
//重置验证码表
C::t('common_seccheck')->truncate();

if($settingnew['heatthread']['type'] == 2 && $settingnew['heatthread']['period']) {
	$partakeperoid = 86400 * $settingnew['heatthread']['period'];
	//DB::query("DELETE FROM ".DB::table('forum_threadpartake')." WHERE dateline<'$_G[timestamp]'-$partakeperoid", 'UNBUFFERED');
	C::t('forum_threadpartake')->delete($_G[timestamp]-$partakeperoid);
}

//DB::query("UPDATE ".DB::table('common_member_count')." SET todayattachs='0',todayattachsize='0'");
C::t('common_member_count')->clear_today_data();

//DB::query("UPDATE ".DB::table('forum_trade')." SET closed='1' WHERE expiration>0 AND expiration<'$_G[timestamp]'", 'UNBUFFERED');
C::t('forum_trade')->update_closed($_G['timestamp']);
//DB::query("DELETE FROM ".DB::table('forum_tradelog')." WHERE buyerid>0 AND status=0 AND lastupdate<'$_G[timestamp]'-432000", 'UNBUFFERED');
C::t('forum_tradelog')->clear_failure(7);
C::t('forum_tradelog')->expiration_payed(7);
C::t('forum_tradelog')->expiration_finished(7);
//note DB::query("UPDATE ".DB::table('forum_tradelog')." SET status='7' WHERE buyerid>'0' AND status='5' AND lastupdate<'$_G[timestamp]'-604800 AND transport='3' AND offline='1'");
//note DB::query("UPDATE ".DB::table('forum_tradelog')." SET status='7' WHERE buyerid>'0' AND status='5' AND lastupdate<'$_G[timestamp]'-2592000 AND transport<>'3' AND offline='1'");

if($_G['setting']['cachethreadon']) {
	removedir($_G['setting']['cachethreaddir'], TRUE);
}
removedir($_G['setting']['attachdir'].'image', TRUE);
@touch($_G['setting']['attachdir'].'image/index.htm');

C::t('forum_attachment_unused')->clear();

C::t('forum_polloption_image')->clear();

//note 到期恢复禁言用户
$uids = $members = array();
/*$query = DB::query("SELECT uid, groupid, credits FROM ".DB::table('common_member')." WHERE groupid IN ('4', '5') AND groupexpiry>'0' AND groupexpiry<'$_G[timestamp]'");
while($row = DB::fetch($query)) {
	$uids[] = $row['uid'];
	$members[$row['uid']] = $row;
}
 *
 */
$members = C::t('common_member')->fetch_all_ban_by_groupexpiry(TIMESTAMP);
if(($uids = array_keys($members))) {
	$setarr = array();
	//$query = DB::query("SELECT uid, groupterms FROM ".DB::table('common_member_field_forum')." WHERE uid IN (".dimplode($uids).")");
	//while($member = DB::fetch($query)) {
	foreach(C::t('common_member_field_forum')->fetch_all($uids) as $uid => $member) {
		//$sql = 'uid=uid';
		$member['groupterms'] = dunserialize($member['groupterms']);
		$member['groupid'] = $members[$uid]['groupid'];
		$member['credits'] = $members[$uid]['credits'];

		if(!empty($member['groupterms']['main']['groupid'])) {
			$groupidnew = $member['groupterms']['main']['groupid'];
			$adminidnew = $member['groupterms']['main']['adminid'];
			unset($member['groupterms']['main']);
			unset($member['groupterms']['ext'][$member['groupid']]);
			//$sql .= ', groupexpiry=\''.groupexpiry($member['groupterms']).'\'';
			$setarr['groupexpiry'] = groupexpiry($member['groupterms']);
		} else {
			//$query = DB::query("SELECT groupid FROM ".DB::table('common_usergroup')." WHERE type='member' AND creditshigher<='$member[credits]' AND creditslower>'$member[credits]'");
			//$groupidnew = DB::result($query, 0);
			$query = C::t('common_usergroup')->fetch_by_credits($member['credits'], 'member');
			$groupidnew = $query['groupid'];
			$adminidnew = 0;
		}
		//$sql .= ", adminid='$adminidnew', groupid='$groupidnew'";
		$setarr['adminid'] = $adminidnew;
		$setarr['groupid'] = $groupidnew;
		//DB::query("UPDATE ".DB::table('common_member')." SET $sql WHERE uid='$member[uid]'");
		C::t('common_member')->update($uid, $setarr);
		//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET groupterms='".($member['groupterms'] ? addslashes(serialize($member['groupterms'])) : '')."' WHERE uid='$member[uid]'");
		C::t('common_member_field_forum')->update($uid, array('groupterms' => ($member['groupterms'] ? serialize($member['groupterms']) : '')));
	}
}

// 广告提醒
if(!empty($_G['setting']['advexpiration']['allow'])) {
	$endtimenotice = mktime(0, 0, 0, date('m', TIMESTAMP), date('d', TIMESTAMP), date('Y', TIMESTAMP)) + $_G['setting']['advexpiration']['day'] * 86400;
	$advs = array();
//	$query = DB::query("SELECT advid, title FROM ".DB::table('common_advertisement')." WHERE endtime='$endtimenotice'");
//	while($adv = DB::fetch($query)) {
	foreach(C::t('common_advertisement')->fetch_all_endtime($endtimenotice) as $adv) {
		$advs[] = '<a href="admin.php?action=adv&operation=edit&advid='.$adv['advid'].'" target="_blank">'.$adv['title'].'</a>';
	}
	if($advs) {
		$users = explode("\n", $_G['setting']['advexpiration']['users']);
		$users = array_map('trim', $users);
		if($users) {
			//$query = DB::query("SELECT username, uid, email FROM ".DB::table("common_member")." WHERE username IN (".dimplode($users).")");
			//while($member = DB::fetch($query)) {
			foreach(C::t('common_member')->fetch_all_by_username($users) as $member) {
				$noticelang = array('day' => $_G['setting']['advexpiration']['day'], 'advs' => implode("<br />", $advs), 'from_id' => 0, 'from_idtype' => 'advexpire');
				if(in_array('notice', $_G['setting']['advexpiration']['method'])) {
					notification_add($member['uid'], 'system', 'system_adv_expiration', $noticelang, 1);
				}
				if(in_array('mail', $_G['setting']['advexpiration']['method'])) {
					if(!sendmail("$member[username] <$member[email]>", lang('email', 'adv_expiration_subject', $noticelang), lang('email', 'adv_expiration_message', $noticelang))) {
						runlog('sendmail', "$member[email] sendmail failed.");
					}
				}
			}
		}
	}
}


//noteX 清理过期充值卡密
//$count = DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_card')." WHERE status = '1' AND cleardateline <= '{$_G['timestamp']}'");
$count = C::t('common_card')->count_by_where("status = '1' AND cleardateline <= '{$_G['timestamp']}'");
if($count) {
	//DB::query("UPDATE ".DB::table('common_card')." SET status = 9 WHERE status = '1' AND cleardateline <= '{$_G['timestamp']}'");
	C::t('common_card')->update_to_overdue($_G['timestamp']);
	$card_info = serialize(array('num' => $count));
//	DB::query("INSERT INTO ".DB::table('common_card_log')." (info, dateline, operation)VALUES('{$card_info}', '{$_G['timestamp']}', 9)");
	$cardlog = array(
		'info' => $card_info,
		'dateline' => $_G['timestamp'],
		'operation' => 9
	);
	C::t('common_card_log')->insert($cardlog);
}

//note 删除过期的用户操作记录
//DB::delete('common_member_action_log', 'dateline < '.($_G['timestamp'] - 86400));
C::t('common_member_action_log')->delete_by_dateline($_G['timestamp'] - 86400);

//note 删除超过7天的淘帖邀请
C::t('forum_collectioninvite')->delete_by_dateline($_G['timestamp'] - 86400*7);

//note 清除注册验证码规则状态
loadcache('seccodedata', true);
$_G['cache']['seccodedata']['register']['show'] = 0;
savecache('seccodedata', $_G['cache']['seccodedata']);

/**
* 删除非空目录
* @param $path 目录
*/
function removedir($dirname, $keepdir = FALSE) {
	$dirname = str_replace(array( "\n", "\r", '..'), array('', '', ''), $dirname);

	if(!is_dir($dirname)) {
		return FALSE;
	}
	$handle = opendir($dirname);
	while(($file = readdir($handle)) !== FALSE) {
		if($file != '.' && $file != '..') {
			$dir = $dirname . DIRECTORY_SEPARATOR . $file;
			is_dir($dir) ? removedir($dir) : unlink($dir);
		}
	}
	closedir($handle);
	return !$keepdir ? (@rmdir($dirname) ? TRUE : FALSE) : TRUE;
}

?>