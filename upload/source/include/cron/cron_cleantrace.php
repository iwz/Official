<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_cleantrace.php 24958 2011-10-19 02:54:32Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//清理脚印和最新访客
$maxday = 90;//保留90天的
$deltime = $_G['timestamp'] - $maxday*3600*24;

//清理脚印
//DB::query("DELETE FROM ".DB::table('home_clickuser')." WHERE dateline < '$deltime'");
C::t('home_clickuser')->delete_by_dateline($deltime);

//最新访客
//DB::query("DELETE FROM ".DB::table('home_visitor')." WHERE dateline < '$deltime'");
C::t('home_visitor')->delete_by_dateline($deltime);

?>