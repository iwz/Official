<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_cleanup_monthly.php 26922 2011-12-27 10:00:36Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//$myrecordtimes = TIMESTAMP - $_G['setting']['myrecorddays'] * 86400;

//note DB::query("DELETE FROM ".DB::table('common_invite')." WHERE dateline<'$_G[timestamp]'-2592000 AND status='4'", 'UNBUFFERED');
//note DB::query("TRUNCATE ".DB::table('forum_relatedthread')."");
C::t('common_mytask')->delete_exceed(2592000);

?>