<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: space_debate.php 28220 2012-02-24 07:52:50Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$minhot = $_G['setting']['feedhotmin']<1?3:$_G['setting']['feedhotmin'];
$page = empty($_GET['page'])?1:intval($_GET['page']);
if($page<1) $page=1;
$id = empty($_GET['id'])?0:intval($_GET['id']);
$opactives['debate'] = 'class="a"';

//默认显示
if(empty($_GET['view'])) $_GET['view'] = 'we';
$_GET['order'] = empty($_GET['order']) ? 'dateline' : $_GET['order'];
//分页
$perpage = 20;
$perpage = mob_perpage($perpage);
$start = ($page-1)*$perpage;
ckstart($start, $perpage);

$list = $userlist = array();
$count = $pricount = 0;

$gets = array(
	'mod' => 'space',
	'uid' => $space['uid'],
	'do' => 'debate',
	'view' => $_GET['view'],
	'order' => $_GET['order'],
	'type' => $_GET['type'],
	'fuid' => $_GET['fuid'],
	'searchkey' => $_GET['searchkey']
);
$theurl = 'home.php?'.url_implode($gets);
$multi = '';

// $wheresql = '1';
// $apply_sql = '';

$f_index = '';
$ordersql = 't.dateline DESC';
$need_count = true;
$join = $authorid = $replies = 0;
$displayorder = null;
$subject = '';

if($_GET['view'] == 'me') {

	if($_GET['type'] == 'reply') {
// 		$wheresql = "p.authorid = '$space[uid]' AND p.first='0' AND p.tid = t.tid";
// 		$posttable = getposttable();
// 		$apply_sql = ', '.DB::table($posttable).' p ';
		$authorid = $space['uid'];
		$join = true;
	} else {
// 		$wheresql = "t.authorid = '$space[uid]'";
		$authorid = $space['uid'];
	}
	$viewtype = in_array($_GET['type'], array('orig', 'reply')) ? $_GET['type'] : 'orig';
	$typeactives = array($viewtype => ' class="a"');

} else {

	space_merge($space, 'field_home');

	if($space['feedfriend']) {

		$fuid_actives = array();

		//查看指定好友的
		require_once libfile('function/friend');
		$fuid = intval($_GET['fuid']);
		if($fuid && friend_check($fuid, $space['uid'])) {
// 			$wheresql = "t.authorid='$fuid'";
			$authorid = $fuid;
			$fuid_actives = array($fuid=>' selected');
		} else {
// 			$wheresql = "t.authorid IN ($space[feedfriend])";
			$authorid = explode(',', $space['feedfriend']);
			$theurl = "home.php?mod=space&uid=$space[uid]&do=$do&view=we";
		}

		//好友列表
		//$query = DB::query("SELECT * FROM ".DB::table('home_friend')." WHERE uid='$space[uid]' ORDER BY num DESC LIMIT 0,100");
		$query = C::t('home_friend')->fetch_all_by_uid($space['uid'], 0, 100, true);
		//while ($value = DB::fetch($query)) {
		foreach($query as $value) {
			$userlist[] = $value;
		}
	} else {
		$need_count = false;
	}
}

$actives = array($_GET['view'] =>' class="a"');

if($need_count) {

// 	$wheresql .= " AND t.special='5'";
// 	$wheresql .= $_GET['view'] != 'me' ? " AND t.displayorder>='0'" : '';
	if($_GET['view'] != 'me') {
		$displayorder = 0;
	}
	//搜索
	if($searchkey = stripsearchkey($_GET['searchkey'])) {
// 		$wheresql .= " AND t.subject LIKE '%$searchkey%'";
		$subject = $searchkey;
		$searchkey = dhtmlspecialchars($searchkey);
	}

// 		$count = DB::result(DB::query("SELECT COUNT(*) FROM ".DB::table('forum_thread')." t $apply_sql WHERE $wheresql"),0);
	$count = C::t('forum_thread')->count_by_special(5, $authorid, $replies, $displayorder, $subject, $join);
	if($count) {
// 			$field = $apply_sql ? ', p.message' : '';
// 			$query = DB::query("SELECT t.* $field FROM ".DB::table('forum_thread')." t $apply_sql
// 				WHERE $wheresql
// 				ORDER BY $ordersql LIMIT $start,$perpage");

		$dids = $special = $multitable = $tids = array();
		require_once libfile('function/post');
		//while($value = DB::fetch($query)) {
		foreach(C::t('forum_thread')->fetch_all_by_special(5, $authorid, $replies, $displayorder, $subject, $join, $start, $perpage) as $value) {
			$value['dateline'] = dgmdate($value['dateline']);
			if($_GET['view'] == 'me' && $_GET['type'] == 'reply' && $page == 1 && count($special) < 2) {
				$value['message'] = messagecutstr($value['message'], 200);
				$special[$value['tid']] = $value;
			} else {
				if($page == 1 && count($special) < 2) {
					$tids[$value['posttableid']][$value['tid']] = $value['tid'];
					$special[$value['tid']] = $value;
				} else {
					$list[$value['tid']] = $value;
				}
			}
			$dids[$value['tid']] = $value['tid'];
		}
		if($tids) {
			foreach($tids as $postid => $tid) {
				//$posttable = getposttable();
				//$query = DB::query("SELECT tid, message FROM ".DB::table($posttable)." WHERE tid IN(".dimplode($tid).")");
				foreach(C::t('forum_post')->fetch_all_by_tid(0, $tid) as $value) {
					$special[$value['tid']]['message'] = messagecutstr($value['message'], 200);
				}
			}
		}
		if($dids) {
//				$query = DB::query("SELECT * FROM ".DB::table('forum_debate')." WHERE tid IN(".dimplode($dids).")");
//				while($value = DB::fetch($query)) {
			foreach(C::t('forum_debate')->fetch_all($dids) as $value) {
				//计算百分比
				$value['negavotesheight'] = $value['affirmvotesheight'] = '8px';
				if($value['affirmvotes'] || $value['negavotes']) {
					$allvotes = $value['affirmvotes'] + $value['negavotes'];
					$value['negavotesheight'] = round($value['negavotes']/$allvotes * 100, 2).'%';
					$value['affirmvotesheight'] = round($value['affirmvotes']/$allvotes * 100, 2).'%';
				}
				if($list[$value['tid']]) {
					$list[$value['tid']] = array_merge($value, $list[$value['tid']]);
				} elseif($special[$value['tid']]) {
					$special[$value['tid']] = array_merge($value, $special[$value['tid']]);
				}
			}
		}

		//分页
		$multi = multi($count, $perpage, $page, $theurl);

	}

}


if($_G['uid']) {
	if($_GET['view'] == 'all') {
		$navtitle = lang('core', 'title_view_all').lang('core', 'title_debate');
	} elseif($_GET['view'] == 'me') {
		$navtitle = lang('core', 'title_my_debate');
	} else {
		$navtitle = lang('core', 'title_friend_debate');
	}
} else {
	if($_GET['order'] == 'hot') {
		$navtitle = lang('core', 'title_top_debate');
	} else {
		$navtitle = lang('core', 'title_newest_debate');
	}
}

include_once template("diy:home/space_debate");

?>