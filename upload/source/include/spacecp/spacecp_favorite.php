<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: spacecp_favorite.php 34278 2013-12-03 09:46:45Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
$_GET['type'] = in_array($_GET['type'], array("thread", "forum", "group", "blog", "album", "article", "all")) ? $_GET['type'] : 'all';
if($_GET['op'] == 'delete') {

	if($_GET['checkall']) {
		if($_GET['favorite']) {
			//DB::query('DELETE FROM '.DB::table('home_favorite')." WHERE uid='$_G[uid]' AND favid IN (".dimplode($_GET['favorite']).")");
			$deletecounter = array();
			$data = C::t('home_favorite')->fetch_all($_GET['favorite']);
			foreach($data as $dataone) {
				switch($dataone['idtype']) {
					case 'fid':
						$deletecounter['fids'][] = $dataone['id'];
						break;
					default:
						break;
				}
			}
			if($deletecounter['fids']) {
				C::t('forum_forum')->update_forum_counter($deletecounter['fids'], 0, 0, 0, 0, -1);
			}
			C::t('home_favorite')->delete($_GET['favorite'], false, $_G['uid']);
			if($_G['setting']['cloud_status']) {
				$favoriteService = Cloud::loadClass('Service_Client_Favorite');
				$favoriteService->remove($_G['uid'], $_GET['favorite'], TIMESTAMP);
			}
		}
		showmessage('favorite_delete_succeed', 'home.php?mod=space&uid='.$_G['uid'].'&do=favorite&view=me&type='.$_GET['type'].'&quickforward=1');
	} else {
		$favid = intval($_GET['favid']);
		//$thevalue = DB::fetch_first('SELECT * FROM '.DB::table('home_favorite')." WHERE favid='$favid'");
		$thevalue = C::t('home_favorite')->fetch($favid);
		if(empty($thevalue) || $thevalue['uid'] != $_G['uid']) {
			showmessage('favorite_does_not_exist');
		}

		if(submitcheck('deletesubmit')) {
			//DB::query('DELETE FROM '.DB::table('home_favorite')." WHERE favid='$favid'");
			switch($thevalue['idtype']) {
				case 'fid':
					C::t('forum_forum')->update_forum_counter($thevalue['id'], 0, 0, 0, 0, -1);
					break;
				default:
					break;
			}
			C::t('home_favorite')->delete($favid);
			if($_G['setting']['cloud_status']) {
				$favoriteService = Cloud::loadClass('Service_Client_Favorite');
				$favoriteService->remove($_G['uid'], $favid);
			}
			showmessage('do_success', 'home.php?mod=space&uid='.$_G['uid'].'&do=favorite&view=me&type='.$_GET['type'].'&quickforward=1', array('favid' => $favid, 'id' => $thevalue['id']), array('showdialog'=>1, 'showmsg' => true, 'closetime' => true, 'locationtime' => 3));
		}
	}

} else {

//	if(!checkperm('allowfavorite')) {
//		showmessage('no_privilege');
//	}

	//新用户见习
	cknewuser();

	$type = empty($_GET['type']) ? '' : $_GET['type'];
	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	$spaceuid = empty($_GET['spaceuid']) ? 0 : intval($_GET['spaceuid']);
	$idtype = $title = $icon = '';
	switch($type) {
		case 'thread':
			$idtype = 'tid';
//			$title = DB::result_first('SELECT subject FROM '.DB::table('forum_thread')." WHERE tid='$id'");
			$thread = C::t('forum_thread')->fetch($id);
			$title = $thread['subject'];
			$icon = '<img src="static/image/feed/thread.gif" alt="thread" class="vm" /> ';
			break;
		case 'forum':
			$idtype = 'fid';
			//$title = DB::result_first('SELECT `name` FROM '.DB::table('forum_forum')." WHERE fid='$id' AND status !='3'");
			$foruminfo = C::t('forum_forum')->fetch($id);
			loadcache('forums');
			$forum = $_G['cache']['forums'][$id];
			if(!$forum['viewperm'] || ($forum['viewperm'] && forumperm($forum['viewperm'])) || strstr($forum['users'], "\t$_G[uid]\t")) {
				$title = $foruminfo['status'] != 3 ? $foruminfo['name'] : '';
				$icon = '<img src="static/image/feed/discuz.gif" alt="forum" class="vm" /> ';
			}
			break;
		case 'blog':
			$idtype = 'blogid';
			//$title = DB::result_first('SELECT subject FROM '.DB::table('home_blog')." WHERE blogid='$id' AND uid='$spaceuid'");
			$bloginfo = C::t('home_blog')->fetch($id);
			$title = ($bloginfo['uid'] == $spaceuid) ? $bloginfo['subject'] : '';
			$icon = '<img src="static/image/feed/blog.gif" alt="blog" class="vm" /> ';
			break;
		case 'group':
			$idtype = 'gid';
			//$title = DB::result_first('SELECT `name` FROM '.DB::table('forum_forum')." WHERE fid='$id' AND status ='3'");
			$foruminfo = C::t('forum_forum')->fetch($id);
			$title = $foruminfo['status'] == 3 ? $foruminfo['name'] : '';
			$icon = '<img src="static/image/feed/group.gif" alt="group" class="vm" /> ';
			break;
		case 'album':
			$idtype = 'albumid';
			//$title = DB::result_first('SELECT albumname FROM '.DB::table('home_album')." WHERE albumid='$id' AND uid='$spaceuid'");
			$result = C::t('home_album')->fetch($id, $spaceuid);
			$title = $result['albumname'];
			$icon = '<img src="static/image/feed/album.gif" alt="album" class="vm" /> ';
			break;
		case 'space':
			$idtype = 'uid';
			//$title = DB::result_first('SELECT username FROM '.DB::table('common_member')." WHERE uid='$id'");
			$_member = getuserbyuid($id);
			$title = $_member['username'];
			$unset($_member);
			$icon = '<img src="static/image/feed/profile.gif" alt="space" class="vm" /> ';
			break;
		case 'article':
			$idtype = 'aid';
			//$title = DB::result_first('SELECT title FROM '.DB::table('portal_article_title')." WHERE aid='$id'");
			$article = C::t('portal_article_title')->fetch($id);
			$title = $article['title'];
			$icon = '<img src="static/image/feed/article.gif" alt="article" class="vm" /> ';
			break;
	}
	if(empty($idtype) || empty($title)) {
		showmessage('favorite_cannot_favorite');
	}

	// 是否已经收藏过
	//if(DB::result_first('SELECT * FROM '.DB::table('home_favorite')." WHERE uid='$_G[uid]' AND idtype='$idtype' AND id='$id'")) {
	$fav = C::t('home_favorite')->fetch_by_id_idtype($id, $idtype, $_G['uid']);
	if($fav) {
		showmessage('favorite_repeat');
	}
	$description = $extrajs = '';
	$description_show = nl2br($description);

	//$fav_count = DB::result_first("SELECT COUNT(*) FROM ".DB::table('home_favorite')." WHERE id='$id' AND idtype='$idtype'");
	$fav_count = C::t('home_favorite')->count_by_id_idtype($id, $idtype);
	if(submitcheck('favoritesubmit') || ($type == 'forum' || $type == 'group' || $type == 'thread') && $_GET['formhash'] == FORMHASH) {
		$arr = array(
			'uid' => intval($_G['uid']),
			'idtype' => $idtype,
			'id' => $id,
			'spaceuid' => $spaceuid,
			'title' => getstr($title, 255),
			'description' => getstr($_POST['description'], '', 0, 0, 1),
			'dateline' => TIMESTAMP
		);
		//DB::insert('home_favorite', $arr);
		$favid = C::t('home_favorite')->insert($arr, true);
		//上报消息中心
		if($_G['setting']['cloud_status']) {
			$favoriteService = Cloud::loadClass('Service_Client_Favorite');
			$favoriteService->add($arr['uid'], $favid, $arr['id'], $arr['idtype'], $arr['title'], $arr['description'], TIMESTAMP);
		}
		switch($type) {
			case 'thread':
//				DB::query("UPDATE ".DB::table('forum_thread')." SET favtimes=favtimes+1 WHERE tid='$id'");
				C::t('forum_thread')->increase($id, array('favtimes'=>1));
				//if($_G['setting']['heatthread']['type'] == 2) { //note 按参与人次
				require_once libfile('function/forum');
				update_threadpartake($id);
				//}
				break;
			case 'forum':
				//DB::query("UPDATE ".DB::table('forum_forum')." SET favtimes=favtimes+1 WHERE fid='$id'");
				C::t('forum_forum')->update_forum_counter($id, 0, 0, 0, 0, 1);
				$extrajs = '<script type="text/javascript">$("number_favorite_num").innerHTML = parseInt($("number_favorite_num").innerHTML)+1;$("number_favorite").style.display="";</script>';
				dsetcookie('nofavfid', '', -1);
				break;
			case 'blog':
				//DB::query("UPDATE ".DB::table('home_blog')." SET favtimes=favtimes+1 WHERE blogid='$id' AND uid='$spaceuid'");
				C::t('home_blog')->increase($id, $spaceuid, array('favtimes' => 1));
				break;
			case 'group':
				//DB::query("UPDATE ".DB::table('forum_forum')." SET favtimes=favtimes+1 WHERE fid='$id' AND status='3'");
				C::t('forum_forum')->update_forum_counter($id, 0, 0, 0, 0, 1);
				break;
			case 'album':
				//DB::query("UPDATE ".DB::table('home_album')." SET favtimes=favtimes+1 WHERE albumid='$id' AND uid='$spaceuid'");
				C::t('home_album')->update_num_by_albumid($id, 1, 'favtimes', $spaceuid);
				break;
			case 'space':
				//DB::query("UPDATE ".DB::table('common_member_status')." SET favtimes=favtimes+1 WHERE uid='$id'");
				C::t('common_member_status')->increase($id, array('favtimes' => 1));
				break;
			case 'article':
				//DB::query("UPDATE ".DB::table('portal_article_count')." SET favtimes=favtimes+1 WHERE aid='$id'");
				C::t('portal_article_count')->increase($id, array('favtimes' => 1));
				break;
		}
		showmessage('favorite_do_success', dreferer(), array('id' => $id, 'favid' => $favid), array('showdialog' => true, 'closetime' => true, 'extrajs' => $extrajs));
	}
}

include template('home/spacecp_favorite');


?>
