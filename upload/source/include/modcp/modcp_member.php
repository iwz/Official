<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: modcp_member.php 33701 2013-08-06 05:04:36Z nemohou $
 */

if(!defined('IN_DISCUZ') || !defined('IN_MODCP')) {
	exit('Access Denied');
}

/**
 * 版主前台编辑会员资料，仅限于 来自，签名，自我介绍，店铺介绍
 */
if($op == 'edit') {

	$_GET['uid'] = isset($_GET['uid']) ? intval($_GET['uid']) : '';
	$_GET['username'] = isset($_GET['username']) ? trim($_GET['username']) : '';

	$member = loadmember($_GET['uid'], $_GET['username'], $error);
	$usernameenc = $member ? rawurlencode($member['username']) : '';

	if($member && submitcheck('editsubmit') && !$error) {

		//$sql = 'uid=uid';
		if($_G['group']['allowedituser']) {

			if(!empty($_GET['clearavatar'])) {
				loaducenter();
				uc_user_deleteavatar($member['uid']);
			}

			require_once libfile('function/discuzcode');

			if($_GET['bionew']) {
				$biohtmlnew = nl2br(dhtmlspecialchars($_GET['bionew']));
			} else {
				$biohtmlnew = '';
			}

			if($_GET['signaturenew']) {
				$signaturenew = censor($_GET['signaturenew']);
				$sightmlnew = discuzcode($signaturenew, 1, 0, 0, 0, $member['allowsigbbcode'], $member['allowsigimgcode'], 0, 0, 1);
			} else {
				$sightmlnew = $signaturenew = '';
			}

			!empty($_GET['locationnew']) && $locationnew = dhtmlspecialchars($_GET['locationnew']);

			//$sql .= ', sigstatus=\''.($signaturenew ? 1 : 0).'\'';
			//noteX 是否保留location?
			//DB::query("UPDATE ".DB::table('common_member_profile')." SET bio='$biohtmlnew' WHERE uid='$member[uid]'");
			//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET sightml='$sightmlnew' WHERE uid='$member[uid]'");
			C::t('common_member_profile')->update($member['uid'], array('bio' => $biohtmlnew));
			C::t('common_member_field_forum')->update($member['uid'], array('sightml' => $sightmlnew));
		}
		//noteX 待议
		//DB::query("UPDATE ".DB::table('common_member')." SET $sql WHERE uid='$member[uid]'");
		acpmsg('members_edit_succeed', "$cpscript?mod=modcp&action=$_GET[action]&op=$op");
		/*showmessage('members_edit_succeed');*/

	} elseif($member) {

		require_once libfile('function/editor');
		$bio = explode("\t\t\t", $member['bio']);
		$member['bio'] = html2bbcode($bio[0]);
		$member['biotrade'] = !empty($bio[1]) ? html2bbcode($bio[1]) : '';
		$member['signature'] = html2bbcode($member['sightml']);
		$username = !empty($_GET['username']) ? $member['username'] : '';

	}

/**
 * 版主禁止会员管理
 */
} elseif($op == 'ban' && ($_G['group']['allowbanuser'] || $_G['group']['allowbanvisituser'])) {

	$_GET['uid'] = isset($_GET['uid']) ? intval($_GET['uid']) : '';
	$_GET['username'] = isset($_GET['username']) ? trim($_GET['username']) : '';
	$member = loadmember($_GET['uid'], $_GET['username'], $error);
	$usernameenc = $member ? rawurlencode($member['username']) : '';

	//note 违规记录
	include_once libfile('function/member');
	$clist = crime('getactionlist', $member['uid']);

	if(($member['type'] == 'system' && in_array($member['groupid'], array(1, 2, 3, 6, 7, 8))) || $member['type'] == 'special') {
		acpmsg('modcp_member_ban_illegal');
		/*showmessage('modcp_member_ban_illegal');*/
	}

	if($member && submitcheck('bansubmit') && !$error) {
		$setarr = array();
		//$sql = 'uid=uid';
		$reason = dhtmlspecialchars(trim($_GET['reason']));
		if(!$reason && ($_G['group']['reasonpm'] == 1 || $_G['group']['reasonpm'] == 3)) {
			acpmsg('admin_reason_invalid');
		}

		if($_GET['bannew'] == 4 || $_GET['bannew'] == 5) {
			if($_GET['bannew'] == 4 && !$_G['group']['allowbanuser'] || $_GET['bannew'] == 5 && !$_G['group']['allowbanvisituser']) {
				acpmsg('admin_nopermission');
			}
			$groupidnew = $_GET['bannew'];
			$banexpirynew = !empty($_GET['banexpirynew']) ? TIMESTAMP + $_GET['banexpirynew'] * 86400 : 0;
			$banexpirynew = $banexpirynew > TIMESTAMP ? $banexpirynew : 0;
			if($banexpirynew) {
				$member['groupterms'] = $member['groupterms'] && is_array($member['groupterms']) ? $member['groupterms'] : array();
				$member['groupterms']['main'] = array('time' => $banexpirynew, 'adminid' => $member['adminid'], 'groupid' => $member['groupid']);
				$member['groupterms']['ext'][$groupidnew] = $banexpirynew;
				//$sql .= ', groupexpiry=\''.groupexpiry($member['groupterms']).'\'';
				$setarr['groupexpiry'] = groupexpiry($member['groupterms']);
			} else {
				//$sql .= ', groupexpiry=0';
				$setarr['groupexpiry'] = 0;
			}
			if(!$member['adminid']) {
				$member_status = C::t('common_member_status')->fetch($member['uid']);
				if($member_status) {
					captcha::report($member_status['lastip']);
				}
			}
			$adminidnew = -1;
			//DB::delete('forum_postcomment', "authorid='$member[uid]' AND rpid>'0'");
			C::t('forum_postcomment')->delete_by_authorid($member['uid'], false, true);
		} elseif($member['groupid'] == 4 || $member['groupid'] == 5) {
			if(!empty($member['groupterms']['main']['groupid'])) {
				$groupidnew = $member['groupterms']['main']['groupid'];
				$adminidnew = $member['groupterms']['main']['adminid'];
				unset($member['groupterms']['main']);
				unset($member['groupterms']['ext'][$member['groupid']]);
				//$sql .= ', groupexpiry=\''.groupexpiry($member['groupterms']).'\'';
				$setarr['groupexpiry'] = groupexpiry($member['groupterms']);
			} else {
				//$query = DB::query("SELECT groupid FROM ".DB::table('common_usergroup')." WHERE type='member' AND creditshigher<='$member[credits]' AND creditslower>'$member[credits]'");
				//$groupidnew = DB::result($query, 0);
				$usergroup = C::t('common_usergroup')->fetch_by_credits($member['credits']);
				$groupidnew = $usergroup['groupid'];
				$adminidnew = 0;
			}
		} else {
			$groupidnew = $member['groupid'];
			$adminidnew = $member['adminid'];
		}

		//$sql .= ", adminid='$adminidnew', groupid='$groupidnew'";
		$setarr['adminid'] = $adminidnew;
		$setarr['groupid'] = $groupidnew;
		//DB::query("UPDATE ".DB::table('common_member')." SET $sql WHERE uid='$member[uid]'");
		C::t('common_member')->update($member['uid'], $setarr);

		if(DB::affected_rows()) {
			savebanlog($member['username'], $member['groupid'], $groupidnew, $banexpirynew, $reason);
		}

		//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET groupterms='".($member['groupterms'] ? addslashes(serialize($member['groupterms'])) : '')."' WHERE uid='$member[uid]'");
		C::t('common_member_field_forum')->update($member['uid'], array('groupterms' => serialize($member['groupterms'])));
		//note 发送通知
		if($_GET['bannew'] == 4) {
			$notearr = array(
				'user' => "<a href=\"home.php?mod=space&uid=$_G[uid]\">$_G[username]</a>",
				'day' => $_GET['banexpirynew'],
				'reason' => $reason,
				'from_id' => 0,
				'from_idtype' => 'banspeak'
			);
			notification_add($member['uid'], 'system', 'member_ban_speak', $notearr, 1);
		}
		if($_GET['bannew'] == 5) {
			$notearr = array(
				'user' => "<a href=\"home.php?mod=space&uid=$_G[uid]\">$_G[username]</a>",
				'day' => $_GET['banexpirynew'],
				'reason' => $reason,
				'from_id' => 0,
				'from_idtype' => 'banvisit'
			);
			notification_add($member['uid'], 'system', 'member_ban_visit', $notearr, 1);
		}

		//note 违规记录
		if($_GET['bannew'] == 4 || $_GET['bannew'] == 5) {
			crime('recordaction', $member['uid'], ($_GET['bannew'] == 4 ? 'crime_banspeak' : 'crime_banvisit'), $reason);
		}

		acpmsg('modcp_member_ban_succeed', "$cpscript?mod=modcp&action=$_GET[action]&op=$op");
		/*showmessage('modcp_member_ban_succeed');*/

	}

} elseif($op == 'ipban' && $_G['group']['allowbanip']) {

	require_once libfile('function/misc');
	$iptoban = getgpc('ip') ? dhtmlspecialchars(explode('.', getgpc('ip'))) : array('','','','');
	$updatecheck = $addcheck = $deletecheck = $adderror = 0;

	if(submitcheck('ipbansubmit')) {
		$_GET['delete'] = isset($_GET['delete']) ? $_GET['delete'] : '';
//		if($ids = dimplode($_GET['delete'])) {
//			DB::query("DELETE FROM ".DB::table('common_banned')." WHERE id IN ($ids) AND ('$_G[adminid]'='1' OR admin='$_G[username]')");
		if($_GET['delete']) {
			$deletecheck = C::t('common_banned')->delete_by_id($_GET['delete'], $_G['adminid'], $_G['username']);
//			$deletecheck = DB::affected_rows();
		}
		if($_GET['ip1new'] != '' && $_GET['ip2new'] != '' && $_GET['ip3new'] != '' && $_GET['ip4new'] != '') {
			$addcheck = ipbanadd($_GET['ip1new'], $_GET['ip2new'], $_GET['ip3new'], $_GET['ip4new'], $_GET['validitynew'], $adderror);
			if(!$addcheck) {
				$iptoban = array($_GET['ip1new'], $_GET['ip2new'], $_GET['ip3new'], $_GET['ip4new']);
			}
		}

		if(!empty($_GET['expirationnew']) && is_array($_GET['expirationnew'])) {
			foreach($_GET['expirationnew'] as $id => $expiration) {
				if($expiration == intval($expiration)) {
					$expiration = $expiration > 1 ? (TIMESTAMP + $expiration * 86400) : TIMESTAMP + 86400;
//					DB::query("UPDATE ".DB::table('common_banned')." SET expiration='$expiration' WHERE id='$id' AND ('$_G[adminid]'='1' OR admin='$_G[username]')");
					$updatecheck = C::t('common_banned')->update_expiration_by_id($id, $expiration, $_G['adminid'], $_G['username']);
//					empty($updatecheck) && $updatecheck = DB::affected_rows();
				}
			}
		}

		if($deletecheck || $addcheck || $updatecheck) {
			require_once(libfile('function/cache'));
			updatecache('ipbanned');
		}

	}

	$iplist = array();
//	$query = DB::query("SELECT * FROM ".DB::table('common_banned')." ORDER BY dateline");
//	while($banned = DB::fetch($query)) {
	foreach(C::t('common_banned')->fetch_all_order_dateline() as $banned) {
		for($i = 1; $i <= 4; $i++) {
			if($banned["ip$i"] == -1) {
				$banned["ip$i"] = '*';
			}
		}
		$banned['disabled'] = $_G['adminid'] != 1 && $banned['admin'] != $_G['member']['username'] ? 'disabled' : '';
		$banned['dateline'] = dgmdate($banned['dateline'], 'd');
		$banned['expiration'] = dgmdate($banned['expiration'], 'd');
		$banned['theip'] = "$banned[ip1].$banned[ip2].$banned[ip3].$banned[ip4]";
		$banned['location'] = convertip($banned['theip']);
		$iplist[$banned['id']] = $banned;
	}

} else {
	showmessage('undefined_action');
}

function loadmember(&$uid, &$username, &$error) {
	global $_G;

	$uid = !empty($_GET['uid']) && is_numeric($_GET['uid']) && $_GET['uid'] > 0 ? $_GET['uid'] : '';
	$username = isset($_GET['username']) && $_GET['username'] != '' ? dhtmlspecialchars(trim($_GET['username'])) : '';

	$member = array();

	if($uid || $username != '') {

		/*$query = DB::query("SELECT m.uid, m.username, m.groupid, m.adminid, mf.groupterms, mp.bio, mf.sightml, u.type AS grouptype, uf.allowsigbbcode, uf.allowsigimgcode, m.credits FROM ".DB::table('common_member')." m
			LEFT JOIN ".DB::table('common_member_field_forum')." mf ON mf.uid=m.uid
			LEFT JOIN ".DB::table('common_usergroup')." u ON u.groupid=m.groupid
			LEFT JOIN ".DB::table('common_member_profile')." mp ON mp.uid=m.uid
			LEFT JOIN ".DB::table('common_usergroup_field')." uf ON uf.groupid=m.groupid
			WHERE ".($uid ? "m.uid='$uid'" : "m.username='$username'"));*/
		$member = $uid ? getuserbyuid($uid) : C::t('common_member')->fetch_by_username($username);
		if($member) {
			$uid = $member['uid'];
			$member = array_merge($member, C::t('common_member_field_forum')->fetch($uid), C::t('common_member_profile')->fetch($uid),
					C::t('common_usergroup')->fetch($member['groupid']), C::t('common_usergroup_field')->fetch($member['groupid']));
		}
		if(!$member) {
			$error = 2;
		} elseif(($member['grouptype'] == 'system' && in_array($member['groupid'], array(1, 2, 3, 6, 7, 8))) || in_array($member['adminid'], array(1,2,3))) {
			$error = 3;
		} else {
			$member['groupterms'] = dunserialize($member['groupterms']);
			$member['banexpiry'] = !empty($member['groupterms']['main']['time']) && ($member['groupid'] == 4 || $member['groupid'] == 5) ? dgmdate($member['groupterms']['main']['time'], 'Y-n-j') : '';
			$error = 0;
		}

	} else {
		$error = 1;
	}
	return $member;
}

function ipbanadd($ip1new, $ip2new, $ip3new, $ip4new, $validitynew, &$error) {
	global $_G;

	if($ip1new != '' && $ip2new != '' && $ip3new != '' && $ip4new != '') {
		$own = 0;
		$ip = explode('.', $_G['clientip']);
		for($i = 1; $i <= 4; $i++) {

			if(!is_numeric(${'ip'.$i.'new'}) || ${'ip'.$i.'new'} < 0) {
				if($_G['adminid'] != 1) {
					$error = 1;
					return FALSE;
				}
				${'ip'.$i.'new'} = -1;
				$own++;
			} elseif(${'ip'.$i.'new'} == $ip[$i - 1]) {
				$own++;
			}
			${'ip'.$i.'new'} = intval(${'ip'.$i.'new'}) > 255 ? 255 : intval(${'ip'.$i.'new'});
		}

		if($own == 4) {
			$error = 2;
			return FALSE;
		}

		$query = DB::query("SELECT * FROM ".DB::table('common_banned')." WHERE (ip1='$ip1new' OR ip1='-1') AND (ip2='$ip2new' OR ip2='-1') AND (ip3='$ip3new' OR ip3='-1') AND (ip4='$ip4new' OR ip4='-1')");
//		if($banned = DB::fetch($query)) {
		if($banned = C::t('common_banned')->fetch_by_ip($ip1new, $ip2new, $ip3new, $ip4new)) {
			$error = 3;
			return FALSE;
		}

		$expiration = $validitynew > 1 ? (TIMESTAMP + $validitynew * 86400) : TIMESTAMP + 86400;

		//DB::query("UPDATE ".DB::table('common_session')." SET groupid='6' WHERE ('$ip1new'='-1' OR ip1='$ip1new') AND ('$ip2new'='-1' OR ip2='$ip2new') AND ('$ip3new'='-1' OR ip3='$ip3new') AND ('$ip4new'='-1' OR ip4='$ip4new')");
		C::app()->session->update_by_ipban($ip1new, $ip2new, $ip3new, $ip4new);
//		DB::query("INSERT INTO ".DB::table('common_banned')." (ip1, ip2, ip3, ip4, admin, dateline, expiration)
//				VALUES ('$ip1new', '$ip2new', '$ip3new', '$ip4new', '$_G[username]', '$_G[timestamp]', '$expiration')");
		$data = array(
			'ip1' => $ip1new,
			'ip2' => $ip2new,
			'ip3' => $ip3new,
			'ip4' => $ip4new,
			'admin' => $_G['username'],
			'dateline' => $_G['timestamp'],
			'expiration' => $expiration
		);
		C::t('common_banned')->insert($data);
		captcha::report($ip1new.'.'.$ip2new.'.'.$ip3new.'.'.$ip4new);

		return TRUE;

	}

	return FALSE;

}

?>