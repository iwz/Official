/*
	[Discuz!] (C)2001-2099 Comsenz Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: tree.js 23838 2011-08-11 06:51:58Z monkey $
*/

//note private 预置图标，考虑到书写的方便，树形菜单外观基本一样，故写成外部变量。
var icon = new Object();
icon.root		= IMGDIR + '/tree_root.gif';
icon.folder		= IMGDIR + '/tree_folder.gif';
icon.folderOpen		= IMGDIR + '/tree_folderopen.gif';
icon.file		= IMGDIR + '/tree_file.gif';
icon.empty		= IMGDIR + '/tree_empty.gif';
icon.line		= IMGDIR + '/tree_line.gif';
icon.lineMiddle		= IMGDIR + '/tree_linemiddle.gif';
icon.lineBottom		= IMGDIR + '/tree_linebottom.gif';
icon.plus		= IMGDIR + '/tree_plus.gif';
icon.plusMiddle		= IMGDIR + '/tree_plusmiddle.gif';
icon.plusBottom		= IMGDIR + '/tree_plusbottom.gif';
icon.minus		= IMGDIR + '/tree_minus.gif';
icon.minusMiddle	= IMGDIR + '/tree_minusmiddle.gif';
icon.minusBottom	= IMGDIR + '/tree_minusbottom.gif';

//note 树结点
function treeNode(id, pid, name, url, target, open) {
	var obj = new Object();
	obj.id = id;
	obj.pid = pid;
	obj.name = name;
	obj.url = url;
	obj.target = target;
	obj.open = open;
	obj._isOpen = open;
	obj._lastChildId = 0;
	obj._pid = 0;
	return obj;
}

//note 树
function dzTree(treeName) {
	//note private 结点集合
	this.nodes = new Array();
	this.openIds = getcookie('leftmenu_openids');
	this.pushNodes = new Array();
	//note public 添加结点
	this.addNode = function(id, pid, name, url, target, open) {
		var theNode = new treeNode(id, pid, name, url, target, open);
		this.pushNodes.push(id);
		//note 搜索该结点的孩子父亲，并且更新自已和父亲的状态，是否有孩子，是否为最后一个。
		//note 更新父结点的最后一个孩子的id
		if(!this.nodes[pid]) {
			this.nodes[pid] = new Array();
		}
		this.nodes[pid]._lastChildId = id;

		for(k in this.nodes) {
			//note 初始化开启状态 cookie 纪录
			if(this.openIds && this.openIds.indexOf('_' + theNode.id) != -1) {
				theNode._isOpen = true;
			}
			//note 看是否有没有孩子。如果有，更新该结点最后一个孩子的id
			if(this.nodes[k].pid == id) {
				theNode._lastChildId = this.nodes[k].id;
			}
		}
		this.nodes[id] = theNode;
	};

	//note public
	this.show = function() {
		//note 我们将第一个传入的结点当作根结点，并且从根结点开始连接。pid=0
		var s = '<div class="tree">';
		s += this.createTree(this.nodes[0]);
		s += '</div>';
		document.write(s);
	};

	//note private 创建树，参数为某节点，将该节点下的所有孩子全部连接成一个字符串
	this.createTree = function(node, padding) {
		//note 初始化参数
		padding = padding ? padding : '';
		//note 第一级菜单不进行缩进
		if(node.id == 0){
			var icon1 = '';
		} else {
			var icon1 = '<img src="' + this.getIcon1(node) + '" onclick="' + treeName + '.switchDisplay(\'' + node.id + '\')" id="icon1_' + node.id + '" style="cursor: pointer;">';
		}
		var icon2 = '<img src="' + this.getIcon2(node) + '" onclick="' + treeName + '.switchDisplay(\'' + node.id + '\')" id="icon2_' + node.id + '" style="cursor: pointer;">';
		var s = '<div class="node" id="node_' + node.id + '">' + padding + icon1 + icon2 + this.getName(node) + '</div>';//note + ' ' + node.id + ' ' + node.pid + ' ' + node._lastChildId
		s += '<div class="nodes" id="nodes_' + node.id + '" style="display:' + (node._isOpen ? '' : 'none') + '">';
		for(k in this.pushNodes) {
			var id = this.pushNodes[k];
			var theNode = this.nodes[id];
			if(theNode.pid == node.id) {
				//note 第一级菜单不进行缩进
				if(node.id == 0){
					var thePadding = '';
				} else {
					var thePadding = padding + (node.id == this.nodes[node.pid]._lastChildId  ? '<img src="' + icon.empty + '">' : '<img src="' + icon.line + '">');
				}
				if(!theNode._lastChildId) {
					var icon1 = '<img src="' + this.getIcon1(theNode) + '"' + ' id="icon1_' + theNode.id + '">';
					var icon2 = '<img src="' + this.getIcon2(theNode) + '" id="icon2_' + theNode.id + '">';
					//note 当前结点
					s += '<div class="node" id="node_' + theNode.id + '">' + thePadding + icon1 + icon2 + this.getName(theNode) + '</div>';
				} else {
					//note DEBUG此处 node.id == 0 时向下传递永远为空
					s += this.createTree(theNode, thePadding);
				}
			}
		}
		s += '</div>';
		return s;
	};

	//note private createTree()
	this.getIcon1 = function(theNode) {
		var parentNode = this.nodes[theNode.pid];
		var src = '';
		//note 如果为目录
		if(theNode._lastChildId) {
			//note 如果目录为打开
			if(theNode._isOpen) {
				//note 判断位置
				if(theNode.id == 0) {
					return icon.minus;
				}
				if(theNode.id == parentNode._lastChildId) {
					src = icon.minusBottom;
				} else {
					src = icon.minusMiddle;
				}
			} else {
				if(theNode.id == 0) {
					return icon.plus;
				}
				//note 判断位置
				if(theNode.id == parentNode._lastChildId) {
					src = icon.plusBottom;
				} else {
					src = icon.plusMiddle;
				}
			}
		//note 如果为文件
		} else {
			if(theNode.id == parentNode._lastChildId) {
				src = icon.lineBottom;
			} else {
				src = icon.lineMiddle;
			}
		}
		return src;
	};

	//note private createTree()
	this.getIcon2 = function(theNode) {
		var src = '';
		if(theNode.id == 0 ) {
			return icon.root;
		}
		if(theNode._lastChildId) {
			if(theNode._isOpen) {
				src = icon.folderOpen;
			} else {
				src = icon.folder;
			}
		} else {
			src = icon.file;
		}
		return src;
	};

	//note pulic，结点 innerHTML
	this.getName = function(theNode) {
		if(theNode.url) {
			return '<a href="'+theNode.url+'" target="' + theNode.target + '"> '+theNode.name+'</a>';
		} else {
			return theNode.name;
		}
	};

	//note pulic，点击结点时，激发此事件。此方法外为树初始化完以后。所以处理比较特殊。变量范围一定要注意。
	this.switchDisplay = function(nodeId) {
		//note 这里的处理有些特殊。主要调用类本身。
		eval('var theTree = ' + treeName);
		var theNode = theTree.nodes[nodeId];
		//note 打开菜单
		if($('nodes_' + nodeId).style.display == 'none') {
			//note保存到cookie
			theTree.openIds = updatestring(theTree.openIds, nodeId);
			setcookie('leftmenu_openids', theTree.openIds, 8640000000);
			theNode._isOpen = true;
			$('nodes_' + nodeId).style.display = '';
			$('icon1_' + nodeId).src = theTree.getIcon1(theNode);
			$('icon2_' + nodeId).src = theTree.getIcon2(theNode);
		//note 关闭菜单
		} else {
			//note 清理cookie
			theTree.openIds = updatestring(theTree.openIds, nodeId, true);
			setcookie('leftmenu_openids', theTree.openIds, 8640000000);
			theNode._isOpen = false;
			$('nodes_' + nodeId).style.display = 'none';
			$('icon1_' + nodeId).src =  theTree.getIcon1(theNode);
			$('icon2_' + nodeId).src = theTree.getIcon2(theNode);

		}
	};
}